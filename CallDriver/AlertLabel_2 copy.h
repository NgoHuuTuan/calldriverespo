//
//  AlertLabel.h
//  EasyBus
//
//  Created by Manh Tien on 10/20/15.
//  Copyright © 2015 Manh Tien. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertLabel : UILabel
+ (instancetype)initAlertWithContent: (NSString *)text;
+ (void)alertWithContent:(NSString *)text inView:(UIView *)parentView;
- (void)dismissAlertLabel;
@end
