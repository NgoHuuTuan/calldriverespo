//
//  CDObject.m
//  CallDriver
//
//  Created by Ngo Huu Tuan on 7/5/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import "CDObject.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIkit.h>

@implementation CDObject

static CDObject *_CDObject;


+ (CDObject *)sharedInstance{
	dispatch_once_t *dispatchOnce_t;
	dispatch_once(dispatchOnce_t, ^{
		_CDObject = [[CDObject alloc] init];
	});
	return _CDObject;
}

- (void)initObjects{
	_mainQueue = [NSOperationQueue new];
}

#pragma mark - Network Activity Indicator

- (void)startNetworkActivityIndicator{
	self.networkCount++;
	if (self.networkCount > 0) {
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	} else {
		self.networkCount = 0;
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	}
}

- (void)stopNetworkActivityIndicator{
	self.networkCount--;
	if (self.networkCount > 0) {
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	} else {
		self.networkCount = 0;
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	}
}

@end
