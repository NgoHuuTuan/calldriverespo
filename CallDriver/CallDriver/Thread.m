//
//  Thread.m
//  CT
//
//  Created by EuiHwa Park on 14. 1. 9..
//  Copyright (c) 2014년 EuiHwa Park. All rights reserved.
//

#import "Thread.h"

@implementation Thread

+ (void)addThreadQueue:(NSOperationQueue *)queue target:(id)target selector:(SEL)sel object:(id)arg {
    NSInvocationOperation *oper = [[NSInvocationOperation alloc] initWithTarget:target selector:sel object:arg];
    [queue addOperation:oper];
}

@end
