//
//  CountdownTimer.h
//  CallDriver
//
//  Created by Ngo Huu Tuan on 3/29/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol CountdownTimerDelegate <NSObject>

@required
- (void)selectedTimer:(NSString *)timer;
@end

@interface CountdownTimer : UIView

@property (weak, nonatomic) id <CountdownTimerDelegate> delegate;

- (void)showTimer;

@end
