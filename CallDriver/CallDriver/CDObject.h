//
//  CDObject.h
//  CallDriver
//
//  Created by Ngo Huu Tuan on 7/5/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface CDObject : NSObject


#define _obj [CDObject sharedIntance]

@property (nonatomic, strong) NSOperationQueue *mainQueue;


@property (readwrite, assign) NSInteger networkCount;


// GPS
@property (nonatomic, strong) CLLocationManager *locationManager;


+ (CDObject *)sharedInstance;
- (void)startNetworkActivityIndicator;
- (void)stopNetworkActivityIndicator;

@end
