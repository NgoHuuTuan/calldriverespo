//
//  NSDictionary+RemoveNull.h
//  CT
//
//  Created by 박의화 on 2015. 3. 22..
//  Copyright (c) 2015년 의화. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (RemoveNull)

- (NSDictionary *)removeNull;

@end
