//
//  LoginVC.m
//  CallDriver
//
//  Created by Manh Tien on 4/6/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import "LoginVC.h"
#import "Define.h"
#import "Reachability.h"
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import <Bolts/Bolts.h>
#import "ViewController.h"
#import "AppDelegate.h"
#import "DriverViewController.h"
#import "MBProgressHUD.h"


@interface LoginVC () <UITextFieldDelegate,UIAlertViewDelegate> {
    
    UIImageView *_imviLogo;
    UITextField *_txtFiUsername;
    UITextField *_txtFiPassword;
    UIButton *_btnLogin;
    UIButton *_btnCreateAcount;
    
    UIScrollView *_scrollView;
    UIImageView *_imviBackgound;
    
    UILabel *_lblLineUser;
    UILabel *_lblLinePass;
}

@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self constructUIElements];
    [self addUIElements];
    
    [_scrollView addConstraints:[self constraints]];
    
    
    // Add tap gesture recognizer
    UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHandle:)];
    [self.view addGestureRecognizer:tapGes];
    
    //

    [self checkAutoLogin];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    NSLog(@"view will appear");
    
    // Register notification when the keyboard will be show, hide
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    NSLog(@"view will disappear");
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)constructUIElements{
    
    
    _imviLogo = [[UIImageView alloc] init];
    _txtFiUsername = [[UITextField alloc] init];
    _txtFiPassword = [[UITextField alloc] init];
    _btnLogin = [[UIButton alloc]  init];
    _btnCreateAcount = [[UIButton alloc] init];
    _scrollView = [[UIScrollView alloc] init];
    _imviBackgound = [[UIImageView alloc] init];
    _lblLinePass = [[UILabel alloc] init];
    _lblLineUser = [[UILabel alloc] init];
    
    
    _imviLogo.translatesAutoresizingMaskIntoConstraints = NO;
    _txtFiUsername.translatesAutoresizingMaskIntoConstraints = NO;
    _txtFiPassword.translatesAutoresizingMaskIntoConstraints = NO;
    _btnLogin.translatesAutoresizingMaskIntoConstraints = NO;
    _btnCreateAcount.translatesAutoresizingMaskIntoConstraints = NO;
    _scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    _lblLinePass.translatesAutoresizingMaskIntoConstraints = NO;
    _lblLineUser.translatesAutoresizingMaskIntoConstraints = NO;
    
    
    _imviLogo.image = [UIImage imageNamed:@"Logo"];
    _imviBackgound.image = [UIImage imageNamed:@"GB"];

    _btnLogin.layer.cornerRadius = 5;
    _btnLogin.clipsToBounds = YES;
    [_btnLogin addTarget:self action:@selector(btnLoginClicked:) forControlEvents:UIControlEventTouchUpInside];
    [_btnLogin setBackgroundColor:RGB(255,79,113)];
    [_btnLogin setTitle:@"Login" forState:UIControlStateNormal];
    
    [_btnCreateAcount setTitle:@"Create an account" forState:UIControlStateNormal];
    [_btnCreateAcount setTitleColor:[UIColor lightTextColor] forState:UIControlStateNormal];
    [_btnCreateAcount setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    _btnCreateAcount.titleLabel.font = [UIFont systemFontOfSize:14];
    [_btnCreateAcount addTarget:self action:@selector(createAccount:) forControlEvents:UIControlEventTouchUpInside];
    
    
    _scrollView.scrollEnabled = YES;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.bounces = NO;
    _scrollView.alwaysBounceVertical = NO;
    _scrollView.delaysContentTouches = NO;
    _scrollView.backgroundColor = [UIColor clearColor];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"GB"]];
    
    _txtFiUsername.delegate = self;
    _txtFiUsername.borderStyle = UITextBorderStyleNone;
    _txtFiUsername.keyboardType = UIKeyboardTypeEmailAddress;
    _txtFiUsername.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtFiUsername.textAlignment = NSTextAlignmentCenter;
    _txtFiUsername.textColor = [UIColor whiteColor];
    _txtFiUsername.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    
    _txtFiPassword.delegate = self;
    _txtFiPassword.borderStyle = UITextBorderStyleNone;
    _txtFiPassword.keyboardType = UIKeyboardTypeDefault;
    _txtFiPassword.secureTextEntry = YES;
    _txtFiPassword.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtFiPassword.textAlignment = NSTextAlignmentCenter;
    _txtFiPassword.textColor = [UIColor whiteColor];
    _txtFiPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    
    
    _lblLineUser.backgroundColor = [UIColor lightGrayColor];
    _lblLinePass.backgroundColor = [UIColor lightGrayColor];
    
}

- (void)addUIElements{
    
    //	[self.view addSubview:_imviBackgound];
    [self.view addSubview:_scrollView];
    
    [_scrollView addSubview:_imviLogo];
    [_scrollView addSubview:_txtFiUsername];
    [_scrollView addSubview:_txtFiPassword];
    [_scrollView addSubview:_btnLogin];
    [_scrollView addSubview:_btnCreateAcount];
    [_scrollView addSubview:_lblLineUser];
    [_scrollView addSubview:_lblLinePass];
    
}

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


- (NSArray *)constraints{
    
    NSMutableArray *constraintArray = [[NSMutableArray alloc] init];
    
    // Create _txtFiIUsername, _txtFiPassword, _btnLogin, _btnCreateAcount's constraints
    CGFloat imviLogo_Width, imviLogo_Height;
    CGFloat txtFi_Height;
    CGFloat btn_Height;
    CGFloat horizontalMargin = 40.0f;
    NSNumber *txtFiWidth =  @(ScreenWidth - horizontalMargin*2);
    
    if(IS_IPHONE_4){
        imviLogo_Width = 120.0f;
        imviLogo_Height = 100.0f;
        txtFi_Height = 30.0f;
        btn_Height = 40.0f;
    }else if(IS_IPHONE_5){
        imviLogo_Width = 150.0f;
        imviLogo_Height = 110.0f;
        txtFi_Height = 40.0f;
        btn_Height = 40.0f;
    }else {
        imviLogo_Width = 170.0f;
        imviLogo_Height = 150.0f;
        txtFi_Height = 40.0f;
        btn_Height = 40.0f;
    }
    
    
    
    //Create _scrolView's constraints
    NSArray *scrollViewCons_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[_scrollView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_scrollView)];
    NSArray *scrollViewCons_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[_scrollView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_scrollView)];
    
    [self.view addConstraints:scrollViewCons_V];
    [self.view addConstraints:scrollViewCons_H];
    
    
    // Create _imviLogo's constraints
    NSArray *imviLogoConstraintArr;
    NSLayoutConstraint *logoConstraint_Width = [NSLayoutConstraint constraintWithItem:_imviLogo attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:imviLogo_Width];
    NSLayoutConstraint *logoConstraint_Heigh = [NSLayoutConstraint constraintWithItem:_imviLogo attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:imviLogo_Height];
    NSLayoutConstraint *logoConstraint_Y = [NSLayoutConstraint constraintWithItem:_imviLogo attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_scrollView attribute:NSLayoutAttributeTop multiplier:1 constant:50];
    NSLayoutConstraint *logoConstraintCenterX = [NSLayoutConstraint constraintWithItem:_imviLogo attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:_scrollView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0];
    
    imviLogoConstraintArr = [NSArray arrayWithObjects:logoConstraint_Width, logoConstraint_Heigh, logoConstraintCenterX,logoConstraint_Y, nil];
    
    
    
    
    NSDictionary *metrics = @{
                              @"txtFiHeigh":@(txtFi_Height),
                              @"horizontalMargin":@(horizontalMargin),
                              @"verticalSpace":@20,
                              @"verticalSmallSpace":@10,
                              @"btnHeighSmall":@30,
                              @"btnHeigh":@(btn_Height),
                              @"txtFiWidth":txtFiWidth
                              };
    
    NSDictionary *views = @{
                            @"logo":_imviLogo,
                            @"username":_txtFiUsername,
                            @"password":_txtFiPassword,
                            @"login":_btnLogin,
                            @"createAcc":_btnCreateAcount,
                            @"lineUser":_lblLineUser,
                            @"linePass":_lblLinePass
                            };
    
    
    NSArray *spaceLogoAndUsername = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[logo]-70-[username]"
                                                                            options:0
                                                                            metrics:metrics
                                                                              views:views];
    
    NSArray *usernameCons_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-horizontalMargin-[username(txtFiWidth)]-horizontalMargin-|"
                                                                      options:0
                                                                      metrics:metrics
                                                                        views:views];
    
    
    NSString *visualFormat = @"V:[username(txtFiHeigh)]-[lineUser(1)]-verticalSmallSpace-[password(txtFiHeigh)]-[linePass(1)]-verticalSpace-[login(50)]-verticalSmallSpace-[createAcc(btnHeighSmall)]-20@500-|";
    NSArray *username_password_login_createAcc_ConsArr = [NSLayoutConstraint constraintsWithVisualFormat:visualFormat
                                                                                                 options:NSLayoutFormatAlignAllLeading
                                                                                                 metrics:metrics
                                                                                                   views:views];
    
    NSArray *passwordCons_H = [NSLayoutConstraint constraintsWithVisualFormat:@"[password(username)]"
                                                                      options:0
                                                                      metrics:metrics
                                                                        views:views];
    NSArray *loginCons_H = [NSLayoutConstraint constraintsWithVisualFormat:@"[login(username)]"
                                                                   options:0
                                                                   metrics:metrics
                                                                     views:views];
    NSArray *createAccCons_H = [NSLayoutConstraint constraintsWithVisualFormat:@"[createAcc(username)]"
                                                                       options:0
                                                                       metrics:metrics
                                                                         views:views];

    NSArray *lineUserCons_H = [NSLayoutConstraint constraintsWithVisualFormat:@"[lineUser(username)]"
                                                                      options:0
                                                                      metrics:metrics
                                                                        views:views];
    NSArray *linePassCons_H = [NSLayoutConstraint constraintsWithVisualFormat:@"[linePass(username)]"
                                                                      options:0
                                                                      metrics:metrics
                                                                        views:views];
    
    [constraintArray addObjectsFromArray:imviLogoConstraintArr];
    [constraintArray addObjectsFromArray:usernameCons_H];
    [constraintArray addObjectsFromArray:spaceLogoAndUsername];
    [constraintArray addObjectsFromArray:username_password_login_createAcc_ConsArr];
    [constraintArray addObjectsFromArray:passwordCons_H];
    [constraintArray addObjectsFromArray:loginCons_H];
    [constraintArray addObjectsFromArray:createAccCons_H];
    [constraintArray addObjectsFromArray:lineUserCons_H];
    [constraintArray addObjectsFromArray:linePassCons_H];
 
    return constraintArray;
}



#pragma Define selector for UITapGestureReconizer

- (void)tapHandle:(UITapGestureRecognizer *)tap{
    tap.cancelsTouchesInView = YES;
    [self.view endEditing:YES];
}

#pragma mark - Login

- (IBAction)btnLoginClicked:(id)sender{
    NSString *strPassword = _txtFiPassword.text;
    NSString *strUserName = _txtFiUsername.text;
	

    __weak LoginVC *weakSelf = self;
    MBProgressHUD * HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.label.text = @"Loading...";
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    HUD.backgroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
//    HUD.dimBackground = YES;
    
    [PFUser logInWithUsernameInBackground:strUserName password:strPassword block:^(PFUser * _Nullable user, NSError * _Nullable error) {
        if (!error) {
            NSLog(@"Login success");
            NSLog(@"%@",[PFUser currentUser]);
            [[PFUser currentUser] setValue:[PFInstallation currentInstallation].deviceToken forKey:@"token"];
            [[PFUser currentUser] saveInBackground];
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
                [weakSelf pushWhenLoginSucces];
            });
        }else{
            NSLog(@"Login error: %@",error.description);
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
            });
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Login Fail" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Oke" otherButtonTitles: nil];
            [alertView show];
        }
    }];
	
//	AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
//	ViewController *vc = [[ViewController alloc] init];
//	UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
//	[appDelegate.window setRootViewController:nav];

}

-  (void)checkAutoLogin{
    if ([PFUser currentUser]) {
        [self pushWhenLoginSucces];
    }
}

#pragma mark - CreateAccount

- (IBAction)createAccount:(id)sender{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please contact with Mr.Tien to create a new account" delegate:nil cancelButtonTitle:@"Oke" otherButtonTitles: nil];
    [alertView show];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self.view endEditing:YES];
    
    return YES;
}

#pragma mark - Handle keyboard will show

- (void)keyboardWillHide:(NSNotification *)notification{
    NSTimeInterval duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsZero;
        [_scrollView setContentInset:edgeInsets];
        [_scrollView setScrollIndicatorInsets:edgeInsets];
    }];
}

- (void)keyboardWillShow:(NSNotification*)notification
{
    CGSize kbSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    NSTimeInterval duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
        [_scrollView setContentInset:edgeInsets];
        [_scrollView setScrollIndicatorInsets:edgeInsets];
    }];

}

#pragma mark - Check internet connection

- (BOOL)checkInternetConnection
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    if (networkStatus != NotReachable) {
        // Connected. Do some Internet stuff
        
        return YES;
    } else {
        // Not connected
        UIAlertView *alertView  = [[UIAlertView alloc] initWithTitle:@"" message:@"Please connect to Internet" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alertView show];
        
        return NO;
    }
}


- (void)pushWhenLoginSucces{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    int permission = [[[PFUser currentUser] valueForKey:@"level"] intValue];
	
    if (permission == AccessPermissionCEO) {
        ViewController *vc = [[ViewController alloc] init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        [appDelegate.window setRootViewController:nav];
    }else if(permission == AccessPermissionDriver){
        DriverViewController *vc = [[DriverViewController alloc] init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        [appDelegate.window setRootViewController:nav];
    }
}




@end
