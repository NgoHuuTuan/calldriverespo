//
//  ServerConnection.h
//  CT
//
//  Created by EuiHwa Park on 13. 11. 19..
//  Copyright (c) 2013년 EuiHwa Park. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServerConnection : NSObject

// URL에 대한 직접 결과값
+ (NSString *)connectionResultStringToURL:(NSString *)url connectionName:(NSString *)connectionName;
+ (NSString *)connectionStringPostURL:(NSString *)url body:(NSString *)body connectionName:(NSString *)connectionName utf8:(BOOL)utf8;
// Get
+ (NSDictionary *)connectionDictionaryGetURL:(NSString *)url connectionName:(NSString *)connectionName;
+ (NSArray *)connectionArrayGetURL:(NSString *)url connectionName:(NSString *)connectionName;
// Post
+ (NSDictionary *)connectionDictionaryPostURL:(NSString *)url body:(NSString *)body connectionName:(NSString *)connectionName utf8:(BOOL)utf8;
+ (NSArray *)connectionArrayPostURL:(NSString *)url body:(NSString *)body connectionName:(NSString *)connectionName;
// Put
+ (NSDictionary *)connectionDictionaryPutURL:(NSString *)url body:(NSString *)body connectionName:(NSString *)connectionName;
+ (NSArray *)connectionArrayPutURL:(NSString *)url body:(NSString *)body connectionName:(NSString *)connectionName;


@end
