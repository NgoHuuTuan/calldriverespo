//
//  AppDelegate.h
//  CallDriver
//
//  Created by Ngo Huu Tuan on 3/26/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

