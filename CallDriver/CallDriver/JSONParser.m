//
//  JSONParser.m
//  DefaultJSON
//
//  Created by 박의화 on 11. 5. 30..
//  Copyright 2011 sunchon. All rights reserved.
//

#import "JSONParser.h"


@implementation JSONParser

- (id)initWithJSON {
	
	if ((self = [super init])) {
		
		jsonParser = [[SBJsonParser alloc] init];
		jsonWriter = [[SBJsonWriter alloc] init];
		
	}
	
	return self;
}

// 쓰레드 처리 해야 합니다.
- (NSMutableDictionary *)dictionaryWithURL:(NSString *)urlString {
    
    NSString *escapedUrl = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
	NSString *str = [NSString stringWithContentsOfURL:[NSURL URLWithString:escapedUrl] encoding:NSUTF8StringEncoding error:nil];
    
	NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
	    
	[dictionary setDictionary:[jsonParser objectWithString:str]];
	
	return dictionary;
	
}

// 쓰레드 처리 해야 합니다.
- (NSMutableArray *)arrayWithURL:(NSString *)urlString {
    
    NSString *escapedUrl = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	NSString *str = [NSString stringWithContentsOfURL:[NSURL URLWithString:escapedUrl] encoding:NSUTF8StringEncoding error:nil];
	
	NSMutableArray *array = [[NSMutableArray alloc] init];
	
	[array setArray:[jsonParser objectWithString:str]];
	
	return array;
	
}

- (NSMutableDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    
	NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
	
	[dictionary setDictionary:[jsonParser objectWithString:jsonString]];
	
	return dictionary;
	
}

- (NSMutableArray *)arrayWithJsonString:(NSString *)jsonString {
    
	NSMutableArray *array = [[NSMutableArray alloc] init];
	
	[array setArray:[jsonParser objectWithString:jsonString]];
	
	return array;
	
}

@end
