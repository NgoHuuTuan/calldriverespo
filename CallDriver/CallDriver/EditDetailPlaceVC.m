//
//  EditDetailPlaceVC.m
//  CallDriver
//
//  Created by Manh Tien on 4/5/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import "EditDetailPlaceVC.h"
#import "TextFieldCell.h"
#import "Define.h"
#import "Constants.h"	
#import "FavouritePlace.h"


@interface EditDetailPlaceVC ()<UITableViewDataSource,UITableViewDelegate>
@property (strong,nonatomic) UITableView *tableView;
@end

@implementation EditDetailPlaceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	self.title = @"Edit favourite place";
	
	_tableView = [[UITableView alloc] init];
	_tableView.translatesAutoresizingMaskIntoConstraints = NO;
	_tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	_tableView.dataSource = self;
	_tableView.delegate = self;
    _tableView.alwaysBounceVertical = NO;
	[self.view addSubview:_tableView];
	
	_tableView.delaysContentTouches = NO;
	// Autolayout
	NSDictionary *views = @{
							@"tableView":_tableView,
							@"topLayoutGuide":self.topLayoutGuide
							};
	NSArray *tbv_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[tableView]-0-|" options:0 metrics:nil views:views];
	NSArray *tbv_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[topLayoutGuide]-10-[tableView]-0-|" options:0 metrics:nil views:views];
	
	[self.view addConstraints:tbv_H];
	[self.view addConstraints:tbv_V];
	
	self.automaticallyAdjustsScrollViewInsets = NO;
	self.view.backgroundColor = [UIColor whiteColor];
	
	// Enable click button in table view
	for (UIView *currentView in _tableView.subviews)
	{
		if([currentView isKindOfClass:[UIScrollView class]])
		{
			((UIScrollView *)currentView).delaysContentTouches = NO;
			break;
		}
	}
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

#pragma mark - UITableViewDelegate and Datasource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return (IS_IPHONE_4)?50.0f:60.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0: // Place Type
        {
            UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"placetypecell"];
			cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
			switch (self.place.placeType) {
                case FavouritePlaceHome:
                {
                    cell.imageView.image = [UIImage imageNamed:@"homeicon"];
                    cell.textLabel.text = @"Home";
                    break;
                }
                case FavouritePlaceWork:
                {
                    cell.imageView.image = [UIImage imageNamed:@"workicon"];
                    cell.textLabel.text = @"Work";
                    break;
                }
                case FavouritePlaceOthers:
                {
                    cell.imageView.image = [UIImage imageNamed:@"workicon"];
                    cell.textLabel.text = @"Others";
                    break;
                }
                default:
                    break;
            }
            return cell;
        }
            
        case 1:{    // Title
			TextFieldCell *textFieldCell = [[TextFieldCell alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, (IS_IPHONE_4)?40.0f:50.0f)];
			textFieldCell.textField.text = self.place.strPlaceTitle;
            [textFieldCell.textField becomeFirstResponder];
			return textFieldCell;
        }
        case 2:{    // Location
			UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"locationcell"];
            cell.textLabel.text = self.place.strPlaceName;
			cell.textLabel.numberOfLines = 2;
            return cell;
        }
		case 3:{
			
			UITableViewCell *cell = [[UITableViewCell alloc] init];
			UIButton *tbnSend = [[UIButton alloc] initWithFrame:CGRectMake(50.0f, 10.0f, ScreenWidth - 100, (IS_IPHONE_4)?40.0f:50.0f)];
			[tbnSend setTitle:@"Save" forState:UIControlStateNormal];
			[tbnSend setBackgroundImage:[Constants imageWithColor:kColorPink] forState:UIControlStateNormal];
			[tbnSend addTarget:self action:@selector(save:) forControlEvents:UIControlEventTouchUpInside];
			tbnSend.layer.cornerRadius = 10.0f;
			tbnSend.layer.masksToBounds = YES;
			[cell.contentView addSubview:tbnSend];
			 return cell;
		}
			break;
        default:
            break;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	[self.view endEditing:YES];
	
	if(indexPath.row == 0){
		 UITableViewCell *placeTypeCell = [_tableView  cellForRowAtIndexPath:indexPath];
		
		
		UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Choose place type"
																	   message:nil
																preferredStyle:UIAlertControllerStyleActionSheet];
		
		UIAlertAction* homeAction = [UIAlertAction actionWithTitle:@"Home" style:UIAlertActionStyleDefault
														   handler:^(UIAlertAction * action) {
															   placeTypeCell.imageView.image = [UIImage imageNamed:@"homeicon"];
															   placeTypeCell.textLabel.text = @"Home";
															   _place.placeType = FavouritePlaceHome;
														   }];
		UIAlertAction* workAction = [UIAlertAction actionWithTitle:@"Work" style:UIAlertActionStyleDefault
														   handler:^(UIAlertAction * action) {
															   placeTypeCell.imageView.image = [UIImage imageNamed:@"workicon"];
															   placeTypeCell.textLabel.text = @"Work";
															   _place.placeType = FavouritePlaceWork;
														   }];
		UIAlertAction* othersAction = [UIAlertAction actionWithTitle:@"Others" style:UIAlertActionStyleDefault
														  handler:^(UIAlertAction * action) {
															  placeTypeCell.imageView.image = [UIImage imageNamed:@"homeicon"];
															  placeTypeCell.textLabel.text = @"Others";
															  _place.placeType = FavouritePlaceOthers;
														  }];
		UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
														  handler:^(UIAlertAction * action) {}];
		
		[alert addAction:homeAction];
		[alert addAction:workAction];
		[alert addAction:othersAction];
		[alert addAction:cancelAction];
		[self presentViewController:alert animated:YES completion:nil];
	}else if (indexPath.row == 3){
	}
}


- (IBAction)save:(id)sender{
	TextFieldCell *textFieldCell = (TextFieldCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
	
	if([[textFieldCell.textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]){
		UIAlertView *titleEmptyAlert = [[UIAlertView alloc] initWithTitle:@"Notification"
																  message:@"Please enter the title"
																 delegate:nil
														cancelButtonTitle:nil
														otherButtonTitles:@"Ok", nil];
		[titleEmptyAlert show];
	}else{
		_place.strPlaceTitle = textFieldCell.textField.text;
//		_place.wrapPlaceType = [NSString stringWithFormat:@"%ld", (long)_place.placeType];
		if(_editingMode == CreateNewPlace){\
			[FavouritePlace addNewFavouritePlaces:_place];
		}else{
			// Repair selected place (remove and add again)
			[FavouritePlace removeFavouritePlaces:_place];
			[FavouritePlace addNewFavouritePlaces:_place];
		}
		[self.navigationController popViewControllerAnimated:YES];
	}
	
	[self.delegate didSaveNewPlace];
}


@end


