//
//  CountdownTimer.m
//  CallDriver
//
//  Created by Ngo Huu Tuan on 3/29/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import "CountdownTimer.h"
#import "Constants.h"

@interface CountdownTimer () <UIPickerViewDelegate, UIPickerViewDataSource>

@property (strong, nonatomic) UIPickerView *pickerView;
@property (strong, nonatomic) UIButton *btnOk;
@property (strong, nonatomic) NSArray *arrDuration;
@property (assign, nonatomic) NSInteger selectedTimer;

@end


@implementation CountdownTimer

- (instancetype)initWithFrame:(CGRect)frame{
	self = [super initWithFrame:frame];
	
	if(self){
		
		self.backgroundColor = [UIColor colorWithRed:20/255 green:20/255 blue:20/255 alpha:0.7];
		
		UIView *anView = [[UIView alloc] init];
		anView.backgroundColor = [UIColor whiteColor];
		anView.layer.cornerRadius = kCornerRadius;
		anView.layer.shadowColor = [UIColor grayColor].CGColor;
		anView.layer.shadowOpacity = 0.5;
		anView.translatesAutoresizingMaskIntoConstraints = NO;
		[self addSubview:anView];
		
		_arrDuration = @[@"5 min", @"7 min", @"10 min", @"13 min", @"15 min", @"20 min", @"25 min", @"27 min", @"30 min", @"35 min", @"40 min", @"45 min", @"50 min", @"55 min", @"1h 10min", @"1h 15min",@"1h 20min",@"1h 25min", @"1h 30min"];
		
		_pickerView = [[UIPickerView alloc] init];
		_pickerView.delegate = self;
		_pickerView.dataSource = self;
		_pickerView.translatesAutoresizingMaskIntoConstraints = NO;
		
		
		_btnOk = [[UIButton alloc] init];
		[_btnOk addTarget:self action:@selector(chooseThatDate:) forControlEvents:UIControlEventTouchUpInside];
		[_btnOk setTitle:@"OK" forState:UIControlStateNormal];
		[_btnOk setBackgroundImage:[Constants imageWithColor:kColorPink] forState:UIControlStateNormal];
		_btnOk.layer.cornerRadius = 10.0f;
		_btnOk.layer.masksToBounds = YES;
		_btnOk.translatesAutoresizingMaskIntoConstraints = NO;
		[self addSubview:_btnOk];
		
		self.alpha = 0.0f;
		
		[self addSubview:anView];
		[anView addSubview:_pickerView];
		[anView addSubview:_btnOk];
		
		// Create timePicker constraints
		NSLayoutConstraint *timePickerCons_Leading = [NSLayoutConstraint constraintWithItem:_pickerView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1 constant:40];
		NSLayoutConstraint *timePickerCons_Trailing = [NSLayoutConstraint constraintWithItem:_pickerView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1 constant:-40];
		NSLayoutConstraint *timePickerCons_Height = [NSLayoutConstraint constraintWithItem:_pickerView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:200];
		NSLayoutConstraint *timePickerCons_CenterY = [NSLayoutConstraint constraintWithItem:_pickerView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
		[self addConstraint:timePickerCons_Leading];
		[self addConstraint:timePickerCons_Trailing];
		[self addConstraint:timePickerCons_Height];
		[self addConstraint:timePickerCons_CenterY];
		
		
		// Create _btnOk constraints
		NSLayoutConstraint *btnOkCons_Leading = [NSLayoutConstraint constraintWithItem:_btnOk attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1 constant:60];
		NSLayoutConstraint *btnOkCons_Trailing = [NSLayoutConstraint constraintWithItem:_btnOk attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1 constant:-60];
		NSLayoutConstraint *btnOkCons_Height = [NSLayoutConstraint constraintWithItem:_btnOk attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:50];
		NSLayoutConstraint *btnOkCons_Top = [NSLayoutConstraint constraintWithItem:_btnOk attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_pickerView attribute:NSLayoutAttributeBottom multiplier:1 constant:40];
		[self addConstraint:btnOkCons_Leading];
		[self addConstraint:btnOkCons_Trailing];
		[self addConstraint:btnOkCons_Height];
		[self addConstraint:btnOkCons_Top];
		
		
		// Create anview constraints
		NSLayoutConstraint *anViewCons_Leading = [NSLayoutConstraint constraintWithItem:anView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:_pickerView attribute:NSLayoutAttributeLeading multiplier:1 constant:-20];
		NSLayoutConstraint *anViewCons_Trailing = [NSLayoutConstraint constraintWithItem:anView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:_pickerView attribute:NSLayoutAttributeTrailing multiplier:1 constant:20];
		NSLayoutConstraint *anViewCons_Bottom = [NSLayoutConstraint constraintWithItem:anView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_btnOk attribute:NSLayoutAttributeBottom multiplier:1 constant:20];
		NSLayoutConstraint *anViewCons_Top = [NSLayoutConstraint constraintWithItem:anView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_pickerView attribute:NSLayoutAttributeTop multiplier:1 constant:-20];
		[self addConstraint:anViewCons_Leading];
		[self addConstraint:anViewCons_Trailing];
		[self addConstraint:anViewCons_Bottom];
		[self addConstraint:anViewCons_Top];
	}
	
	_selectedTimer = 0;
	
	return self;
}


- (IBAction)chooseThatDate:(id)sender{
	
	[self.delegate selectedTimer:(NSString *)_arrDuration[_selectedTimer]];
	
	[self hide];
}

- (void)showTimer{
	
	[WINDOW addSubview:self];
	[UIView animateWithDuration:0.5f animations:^{
		self.alpha = 1.0f;
	}];
}

- (void)hide{
	
	[UIView animateWithDuration:0.4f animations:^{
		self.alpha = 0.0f;
	} completion:^(BOOL finished) {
		[self removeFromSuperview];
	}];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	
	return (NSString *)_arrDuration[row];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
	return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
	
	return _arrDuration.count;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
	
	_selectedTimer = row;
	
}

@end
