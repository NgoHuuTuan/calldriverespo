//
//  CallingInformation.m
//  CleanBasket
//
//  Created by Manh Tien on 3/26/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import "CallingInformation.h"

@implementation CallingInformation

+ (instancetype)initWithDictionaryData:(NSDictionary *)dic{
    CallingInformation *call = [[CallingInformation alloc] init];
    call.strUserPhone = [dic valueForKey:@"userphone"];
    call.strDatePickup = [dic valueForKey:@"timereturn"];
    call.strDateCalling = [dic valueForKey:@"timego"];
    
    float latitude = [[dic valueForKey:@"latitude"] floatValue];
    float longtitude = [[dic valueForKey:@"longtitude"] floatValue];
    call.pickupLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longtitude];
    
    call.callingID = [[dic valueForKey:@"id"] intValue];
    call.callingStatus = [[dic valueForKey:@"status"] intValue];
    return call;
}
@end
