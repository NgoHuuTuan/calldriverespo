//
//  AlertLabel.m
//  EasyBus
//
//  Created by Manh Tien on 10/20/15.
//  Copyright © 2015 Manh Tien. All rights reserved.
//

#import "AlertLabel.h"
#import "Define.h"

@implementation AlertLabel

+ (instancetype)initAlertWithContent: (NSString *)text{
    AlertLabel *label = [[AlertLabel alloc] init];
//    UIFont *myFont = [UIFont systemFontOfSize:15.0f];
//    CGSize sizeOfLabel = [text sizeWithAttributes:@{NSFontAttributeName: myFont}];
//    CGRect newFrame = label.frame;
//    newFrame.size.width = sizeOfLabel.width;
//    newFrame.size.height = sizeOfLabel.height;
//    label.frame = newFrame;
    label.text = text;
    //label.font = myFont;
    label.textColor = [UIColor whiteColor];
    label.backgroundColor = [UIColor grayColor];
    label.layer.borderWidth = 0.0;
    label.layer.cornerRadius = 5.0;
    label.layer.zPosition = 100;
    [label sizeToFit];
    [label setNumberOfLines:1];
    label.alpha = 0.9;
    return label;
}

+ (void)alertWithContent:(NSString *)text inView:(UIView *)parentView{
	HideNetworkActivityIndicator();
	
	AlertLabel *alert = [AlertLabel initAlertWithContent:text];
	[alert setTranslatesAutoresizingMaskIntoConstraints:NO];
	[parentView addSubview:alert];
	[parentView addConstraint:[NSLayoutConstraint constraintWithItem:alert
														  attribute:NSLayoutAttributeCenterX
														  relatedBy:NSLayoutRelationEqual
															 toItem:parentView
														  attribute:NSLayoutAttributeCenterX
														 multiplier:1.0
														   constant:0.0]];
	[parentView addConstraint:[NSLayoutConstraint constraintWithItem:parentView
														  attribute:NSLayoutAttributeBottom
														  relatedBy:NSLayoutRelationEqual
															 toItem:alert
														  attribute:NSLayoutAttributeBottom
														 multiplier:1.0
														   constant:50.0]];
	[parentView addConstraint:[NSLayoutConstraint constraintWithItem:alert
													  attribute:NSLayoutAttributeHeight
													  relatedBy:NSLayoutRelationEqual
														 toItem:nil
													  attribute:NSLayoutAttributeNotAnAttribute
													 multiplier:1.0
													   constant:21]];
	[alert dismissAlertLabel];
}

- (void)dismissPerform{
    [UIView animateWithDuration:3.0 animations:^{
        self.alpha = 0.1;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}


- (void)dismissAlertLabel{
    [self performSelector:@selector(dismissPerform) withObject:nil afterDelay:0.5];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
