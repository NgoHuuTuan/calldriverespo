
//
//  Define.h
//  CT
//
//  Created by pehsys on 2015. 4. 27..
//  Copyright (c) 2015년 pehsys. All rights reserved.
//

#ifndef CT_Define_h
#define CT_Define_h

#ifdef DEBUG
#define APP_NUMBER 0 // 개발
#else
#define APP_NUMBER 1 // 배포
#endif

#define SERVER_CONNECTION_RESULT_ON         YES

#define APP_VERSION                         @"1.0.0"

#define THREAD_COUNT                        20
#define AMOUNT                              30
#define MAGNIFICATION                       2.0
#define MORE                                aa(@"더 보기..")
#define LOADING                             @"Loading.."
#define NO_COUPON                           aa(@"등록된 쿠폰이 없습니다.")
#define NOT_EXIST_RESULT                    aa(@"등록된 업소가 없습니다.")
#define DEVICE_LOCATION_OFF                 @"위치정보를 사용하려면 설정에서 위치 서비스를 켜십시오.\n(Device-설정-개인정보보호-위치 서비스)"
#define SERVER_ERROR                        @"SERVER ERROR"
#define REALTIME_DATEFORMAT                 @"yyyy-MM-dd a hh:mm:ss"
#define MONGO_NOT_EXIST_ITEM                @"[  ]"
#define SYSTEM_VERSION                      [[[UIDevice currentDevice] systemVersion] floatValue]
#define PAGESIZE                            1000
#define MORE_PAGE_REST_COUNT                20

#define aa(string) (NSLocalizedString(string, nil).length == 0)?(string):(NSLocalizedString(string, nil))

#define ENCODING(string) (__bridge NSString *)CFURLCreateStringByAddingPercentEscapes(NULL,(__bridge CFStringRef)string,NULL,(CFStringRef)@"!*'();:@&=+$,/?%%#[]", kCFStringEncodingUTF8)
#define DECODING(string) [string stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]

#ifdef DEBUG
#define NSLog( s, ... ) NSLog( @"[%@(%d)] %@", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
#define NSLog( s, ... )
#endif

#define ALERT(Y) {UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:Y delegate:nil cancelButtonTitle:nil otherButtonTitles:aa(@"확인"), nil];[alert show];}
#define ALERT_ERROR(Y) {UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:Y delegate:nil cancelButtonTitle:nil otherButtonTitles:aa(@"확인"), nil];[alert show];}

#define AddNotification(observer, sel, notiName) [[NSNotificationCenter defaultCenter] addObserver:observer selector:@selector(sel) name:notiName object:nil]
#define PostNotification(notiName, obj) [[NSNotificationCenter defaultCenter] postNotificationName:notiName object:obj]

#define IS_4INCH ([[UIScreen mainScreen] applicationFrame].size.height == 548.0 || [[UIScreen mainScreen] applicationFrame].size.height == 568.0)?(1):(0)
#define IS_IOS7 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)?(1):(0)
#define IS_KOREAN ([aa(@"확인") isEqualToString:@"확인"])?(1):(0)

#define IS_IPHONE_6_PLUS    ([[UIScreen mainScreen] bounds].size.height == 736.0)?(1):(0)
#define IS_IPHONE_6         ([[UIScreen mainScreen] bounds].size.height == 667.0)?(1):(0)
#define IS_IPHONE_5         ([[UIScreen mainScreen] bounds].size.height == 568.0)?(1):(0)
#define IS_IPHONE_4         ([[UIScreen mainScreen] bounds].size.height == 480.0)?(1):(0)

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define IS_PORTRAIT(toInterfaceOrientation) (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight)?(0):(1)
#define IS_LANDSCAPE(toInterfaceOrientation) (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight)?(1):(0)

#define IS_IOS_7 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)?(1):(0)
#define IS_IOS_8 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)?(1):(0)

#define IS_KOR ([aa(@"확인") isEqualToString:@"확인"])?(1):(0)
#define IS_ENG ([aa(@"확인") isEqualToString:@"OK"])?(1):(0)
#define IS_CHN ([aa(@"확인") isEqualToString:@"确认"])?(1):(0)

#define app                                 ((AppDelegate *)[[UIApplication sharedApplication] delegate])
#define WINDOW                              [[[UIApplication sharedApplication] delegate] window]
#define defaults                            [NSUserDefaults standardUserDefaults]
#define SharedApplication                   [UIApplication sharedApplication]
#define NotificationCenter                  [NSNotificationCenter defaultCenter]
#define Bundle                              [NSBundle mainBundle]
#define MainScreen                          [UIScreen mainScreen]
#define fileManager                         [NSFileManager defaultManager]
#define ShowNetworkActivityIndicator()      [UIApplication sharedApplication].networkActivityIndicatorVisible = YES
#define HideNetworkActivityIndicator()      [UIApplication sharedApplication].networkActivityIndicatorVisible = NO
#define NetworkActivityIndicatorVisible(x)  [UIApplication sharedApplication].networkActivityIndicatorVisible = x
#define NavBar                              self.navigationController.navigationBar
#define TabBar                              self.tabBarController.tabBar
#define NavBarHeight                        self.navigationController.navigationBar.bounds.size.height
#define TabBarHeight                        self.tabBarController.tabBar.bounds.size.height
#define ScreenWidth                         [[UIScreen mainScreen] bounds].size.width
#define ScreenHeight                        [[UIScreen mainScreen] bounds].size.height
#define TouchHeightDefault                  44
#define TouchHeightSmall                    32
#define ViewWidth(v)                        v.frame.size.width
#define ViewHeight(v)                       v.frame.size.height
#define ViewX(v)                            v.frame.origin.x
#define ViewY(v)                            v.frame.origin.y
#define SelfViewWidth                       self.view.bounds.size.width
#define SelfViewHeight                      self.view.bounds.size.height
#define RectX(f)                            f.origin.x
#define RectY(f)                            f.origin.y
#define RectWidth(f)                        f.size.width
#define RectHeight(f)                       f.size.height
#define RectSetWidth(f, w)                  CGRectMake(RectX(f), RectY(f), w, RectHeight(f))
#define RectSetHeight(f, h)                 CGRectMake(RectX(f), RectY(f), RectWidth(f), h)
#define RectSetX(f, x)                      CGRectMake(x, RectY(f), RectWidth(f), RectHeight(f))
#define RectSetY(f, y)                      CGRectMake(RectX(f), y, RectWidth(f), RectHeight(f))
#define RectSetSize(f, w, h)                CGRectMake(RectX(f), RectY(f), w, h)
#define RectSetOrigin(f, x, y)              CGRectMake(x, y, RectWidth(f), RectHeight(f))
#define DATE_COMPONENTS                     NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit
#define TIME_COMPONENTS                     NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit
#define FlushPool(p)                        [p drain]; p = [[NSAutoreleasePool alloc] init]
#define RGB(r, g, b)                        [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]
#define RGBA(r, g, b, a)                    [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

#endif
