//
//  NSDictionary+IsTureForKey.m
//  CB
//
//  Created by pehsys on 2015. 5. 11..
//  Copyright (c) 2015년 의화. All rights reserved.
//

#import "NSDictionary+IsTureForKey.h"

@implementation NSDictionary (IsTrueForKey)

- (BOOL)isTrueForKey:(NSString *)key {
    id object = [self objectForKey:key];
    if ([object isKindOfClass:[NSString class]]) {
        return [object isEqualToString:@"true"];
    } else if ([object isKindOfClass:[NSNumber class]]) {
        return [object boolValue];
    } else {
        return NO;
    }
}

@end
