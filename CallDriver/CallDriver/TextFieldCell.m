//
//  TextFieldCell.m
//  CallDriver
//
//  Created by Manh Tien on 4/5/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import "TextFieldCell.h"

@implementation TextFieldCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _textField = [[UITextField alloc] init];
		_textField.placeholder = @"Title";
		_textField.layer.masksToBounds = YES;
		_textField.layer.cornerRadius = 10.0f;
		_textField.borderStyle = UITextBorderStyleRoundedRect;
//		_textField.backgroundColor = [UIColor lightGrayColor];
        _textField.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:_textField];
        
        // Autolayout
        NSArray *constraint_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[_textField]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_textField)];
        NSArray *constraint_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[_textField]-20-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_textField)];
        
        [self.contentView addConstraints:constraint_H];
        [self.contentView addConstraints:constraint_V];
        
        [self updateConstraintsIfNeeded];
        [self layoutIfNeeded];
    }
    
    return self;
}

@end
