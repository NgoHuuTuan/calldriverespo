//
//  Thread.h
//  CT
//
//  Created by EuiHwa Park on 14. 1. 9..
//  Copyright (c) 2014년 EuiHwa Park. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Thread : NSObject

+ (void)addThreadQueue:(NSOperationQueue *)queue target:(id)target selector:(SEL)sel object:(id)arg;

@end
