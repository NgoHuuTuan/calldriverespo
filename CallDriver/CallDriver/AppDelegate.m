//
//  AppDelegate.m
//  CallDriver
//
//  Created by Ngo Huu Tuan on 3/26/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import "AppDelegate.h"
#import <GoogleMaps/GoogleMaps.h>
#import "ViewController.h"
#import "DriverViewController.h"
#import "Constants.h"
#import "EditDetailPlaceVC.h"

#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import <Bolts/Bolts.h>
#import "LoginVC.h"
#import "MainVC.h"


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
        [Parse enableLocalDatastore];

    
	    [Parse initializeWithConfiguration:[ParseClientConfiguration configurationWithBlock:^(id<ParseMutableClientConfiguration> configuration) {
	        configuration.applicationId = PARSE_APP_ID;
	        configuration.clientKey = PARSE_CLIENT_KEY;
	        configuration.server = PARSE_SERVER;
	    }]];
    
	
	

//
//	    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound);
//	    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes categories:nil];
//	
//	    [application registerUserNotificationSettings:settings];
//	    [application registerForRemoteNotifications];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeNewsstandContentAvailability| UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }


    
	[GMSServices provideAPIKey:kAPIkey];
//	 [GMSServices provideAPIKey:@"AIzaSyBai-zLOUBu9tgVoZYKT-_8JS18FWyW7WE"];
	
//	EditDetailPlaceVC *editVC = [[EditDetailPlaceVC alloc] init];
//	
//	ViewController *callerVC = [[ViewController alloc] init];
//	DriverViewController *driveVC = [[DriverViewController alloc] init];
//	
//	
//    UINavigationController *navDrive = [[UINavigationController alloc] initWithRootViewController:callerVC];
//	
//	UINavigationController *mainNC = [[UINavigationController alloc] initWithRootViewController:callerVC];
//	callerVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"CEO" image:[UIImage imageNamed:@"Phone"] tag:0];
//	navDrive.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Drive" image:[UIImage imageNamed:@"Driver"] tag:1];
//	
//	UITabBarController *mainTabBar = [[UITabBarController alloc] init];
//	mainTabBar.viewControllers = @[callerVC, navDrive];
//    LoginVC *loginVC = [[LoginVC alloc] init];
	
	MainVC *mainVC = [[MainVC alloc] initWithNibName:@"MainVC" bundle:nil];
	UINavigationController *mainNC = [[UINavigationController alloc] initWithRootViewController:mainVC];
	self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
	[self.window makeKeyAndVisible];
	[self.window setRootViewController:mainNC];
	
    // open app from a notification
    NSDictionary *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    
    if (notification)
    {
        //your alert should be here
        NSLog(@"Notification : %@",notification);
    }
	
	return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}




- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    //
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    currentInstallation.channels =  @[ @"global" ];
    [currentInstallation saveInBackground];
    //NSLog(@"Data :%@",[[NSString alloc] initWithData:deviceToken encoding:NSUTF8StringEncoding]);
    //NSLog(@"Device Token : %@",[NSString stringWithUTF8String:[deviceToken bytes]]);
    
    NSString* token = [[[[deviceToken description]
                               stringByReplacingOccurrencesOfString: @"<" withString: @""]
                              stringByReplacingOccurrencesOfString: @">" withString: @""]
                             stringByReplacingOccurrencesOfString: @" " withString: @""] ;
    NSLog(@"Device_Token     -----> %@\n",token);
    
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    NSLog(@"Fail register remote push notification : %@",error.description);
}

//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
//    NSLog(@"Push notifcation : %@",userInfo);
//    UIViewController *vc = (ViewController *)self.window.rootViewController;
//    int permission = [[[PFUser currentUser] valueForKey:@"level"] intValue];
//    if (permission == AccessPermissionCEO) {
//        
//    }else if (permission == AccessPermissionDriver){
//        if ([vc isKindOfClass:[DriverViewController class]]) {
//            DriverViewController *driverVC = (DriverViewController *)vc;
//
//        }
//    }
//    
//
//}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    NSLog(@"abc");
    NSLog(@"userinfo : %@",userInfo.description);
//    completionHandler(UIBackgroundFetchResultNewData);
    
    NSLog(@"Call fetch iCloud");
    
    UIViewController *vc = ((UINavigationController*)self.window.rootViewController).visibleViewController;
    int permission = [[[PFUser currentUser] valueForKey:@"level"] intValue];
    if (permission == AccessPermissionCEO) {
        
    }else if (permission == AccessPermissionDriver){
        if ([vc isKindOfClass:[DriverViewController class]]) {
            DriverViewController *driverVC = (DriverViewController *)vc;
           
            [driverVC fetchNewDataWitCompletionHandler:completionHandler andUserInfor:userInfo];
        }
    }

    // Start asynchronous NSOperation, or some other check
    
    // Ideally the NSOperation would notify when it has completed, but just for
    // illustrative purposes, call the completion block after 20 seconds.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 20 * NSEC_PER_SEC),
                   dispatch_get_main_queue(), ^{
                       // Check result of your operation and call completion block with the result
                       completionHandler(UIBackgroundFetchResultNewData);
                   });
}





    

    



@end
