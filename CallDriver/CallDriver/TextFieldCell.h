//
//  TextFieldCell.h
//  CallDriver
//
//  Created by Manh Tien on 4/5/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextFieldCell : UITableViewCell
@property (strong,nonatomic) UITextField *textField;
@end
