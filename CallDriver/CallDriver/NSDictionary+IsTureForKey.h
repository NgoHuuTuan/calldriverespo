//
//  NSDictionary+IsTureForKey.h
//  CB
//
//  Created by pehsys on 2015. 5. 11..
//  Copyright (c) 2015년 의화. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (IsTrueForKey)

- (BOOL)isTrueForKey:(NSString *)key;

@end
