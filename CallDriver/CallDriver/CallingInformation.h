//
//  CallingInformation.h
//  CleanBasket
//
//  Created by Manh Tien on 3/26/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "FavouritePlace.h"
#import <Parse/Parse.h>

@interface CallingInformation : NSObject
@property (strong,nonatomic) NSString *strUserPhone;
@property (strong,nonatomic) NSString *strDateCalling;
@property (strong,nonatomic) NSString *strDatePickup;
@property (strong,nonatomic) CLLocation *pickupLocation;
@property (assign) long callingID;
@property (assign) int callingStatus;

+ (instancetype)initWithDictionaryData:(NSDictionary *)dic;
@end
