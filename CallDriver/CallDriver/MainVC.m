//
//  MainVC.m
//  CallDriver
//
//  Created by Ngo Huu Tuan on 7/4/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import "MainVC.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Constants.h"
#import <AddressBook/ABAddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "DriverViewController.h"
#import "CallingInformation.h"
#import "CountdownTimer.h"
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import "DestinationWindowMarker.h"
#import "FavouritePlace.h"
#import "AlertLabel.h"
#import "EditDetailPlaceVC.h"
#import "EditFavouritePlaceViewController.h"
#import "TextFieldsController.h"
#import "TransparentView.h"

typedef NS_ENUM(NSInteger, kMapViewStatus){
	CallDriverFunction = 0,
	ShowingRouteFunction,
	ShowingSuggestion,
};

static kMapViewStatus mapViewStatus = CallDriverFunction;
static BOOL isShowingRoute = NO;
static BOOL selectedSuggestionPlace = NO;

@interface MainVC () <GMSMapViewDelegate, UITableViewDelegate, UITableViewDataSource, CountdownTimerDelegate, MFMessageComposeViewControllerDelegate, ABPeoplePickerNavigationControllerDelegate, EditDetailPlaceProtocol,TxtFiControllerDelegate, DestinationWindowMarkerDelegate>


@property (weak, nonatomic) IBOutlet UIImageView *imviPinCenter;
@property (weak, nonatomic) IBOutlet UITableView *tbvMain;


@property (copy, nonatomic) NSString *driverPhoneNumber;
@property (copy, nonatomic) NSString *driverName;
@property (copy, nonatomic) NSString *timer;


@property (strong, nonatomic) GMSMapView  *mapView;
@property (strong, nonatomic) GMSPolyline *polyline;
@property (strong, nonatomic) GMSMarker   *startPointMarker;
@property (strong, nonatomic) GMSMarker   *endPointMarker;
@property (strong, nonatomic) UIButton    *btnShowRoute;


@property (strong, nonatomic) DestinationWindowMarker *viWindowMarker;


@property (strong, nonatomic) TextFieldsController *txtFiController;




@property (nonatomic, strong) ABPeoplePickerNavigationController *addressBookController;



@end

@implementation MainVC


#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	
	// Segment configure
	UISegmentedControl *featureSegment = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Call driver",@"Direction", nil]];
	[featureSegment addTarget:self action:@selector(changeSegment:) forControlEvents:UIControlEventValueChanged];
	featureSegment.selectedSegmentIndex = 0;
	self.navigationItem.titleView = featureSegment;
	
	// Right bar button
	CGRect btnFrame = CGRectMake(0, 0, 30, 30);
	UIButton *rightBarBtn = [[UIButton alloc] initWithFrame:btnFrame];
	[rightBarBtn setImage:[UIImage imageNamed:@"EditIcon"] forState:UIControlStateNormal];
	[rightBarBtn addTarget:self action:@selector(clickRightBarBtn:) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftBarItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarBtn];
	self.navigationItem.rightBarButtonItem =  leftBarItem;
	
	// Mapview
	_mapView = [[GMSMapView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
	_mapView.translatesAutoresizingMaskIntoConstraints = NO;
	_mapView.settings.compassButton = YES;
	_mapView.settings.myLocationButton = YES;
	_mapView.myLocationEnabled = YES;
	_mapView.delegate = self;
	[_mapView addObserver:self forKeyPath:@"myLocation" options:0 context:nil];
//	_mapView.padding = UIEdgeInsetsMake(50.0f*4, 0.0, 50.0f*4, 0.0);

	[self.view addSubview:_mapView];
	[self.view sendSubviewToBack:_mapView];
	
	// Table view
	_tbvMain.delaysContentTouches = NO;
	_tbvMain.alwaysBounceVertical = NO;
	_tbvMain.separatorStyle = UITableViewCellSeparatorStyleNone;

	// Enable click button in table view
	for (UIView *currentView in _tbvMain.subviews)
	{
		if([currentView isKindOfClass:[UIScrollView class]])
		{
			((UIScrollView *)currentView).delaysContentTouches = NO;
			break;
		}
	}
	
	// Text field configure
	_txtFiController = [[TextFieldsController alloc] initWithNibName:@"TextFieldsController" bundle:nil];
	_txtFiController.delegate = self;
	[self.view addSubview:_txtFiController.view];
	[self.view bringSubviewToFront:_txtFiController.view];
	
	[_txtFiController didMoveToParentViewController:self];
	[self addChildViewController:_txtFiController];
	
	
	// Button show route
	_btnShowRoute = [[UIButton alloc] initWithFrame:CGRectMake(ScreenWidth - 55 - 10, ScreenHeight - 55 -20 - 55, 55, 55)];
	[_btnShowRoute setImage:[UIImage imageNamed:@"ShowRoute"] forState:UIControlStateNormal];
	[_btnShowRoute addTarget:self  action:@selector(showRoute:) forControlEvents:UIControlEventTouchUpInside];
//	_btnShowRoute.alpha = 0.0f;
	_btnShowRoute.backgroundColor = [UIColor whiteColor];
	_btnShowRoute.layer.masksToBounds = YES;
	_btnShowRoute.layer.cornerRadius = 55/2.0;
	[self.mapView addSubview:_btnShowRoute];
	
	
	_viWindowMarker = [[DestinationWindowMarker alloc] initWithFrame:CGRectMake(ScreenWidth/2.0 - 120, ScreenHeight/2.0 - 40 - 40, 240, 40)];
	_viWindowMarker.delegate = self;
	_viWindowMarker.alpha = 0.0f;
//	[self.view addSubview:_viWindowMarker];
	[self.view insertSubview:_viWindowMarker belowSubview:_txtFiController.view];
	
	// Get previous call info
	_driverName = [[NSUserDefaults standardUserDefaults] stringForKey:@"DriverNamePrevious"];
	_driverPhoneNumber = [[NSUserDefaults standardUserDefaults] stringForKey:@"DriverPhoneNumberPrevious"];
	_timer = [[NSUserDefaults standardUserDefaults] stringForKey:@"PreviousTime"];
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
	// If self still register KVO event, let remove it
	@try{
		[_mapView removeObserver:self forKeyPath:@"myLocation"];
	}@catch(id anException){
		//do nothing
		NSLog(@"Exception: %@", anException);
	}
}

#pragma mark - NSKeyValueObserving

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
	if([keyPath isEqualToString:@"myLocation"]){
		CLLocation *myLocation = [object myLocation];
		
		GMSCameraUpdate *cameraUpdate = [GMSCameraUpdate setTarget:myLocation.coordinate zoom:12];
		[_mapView moveCamera:cameraUpdate];
		[_mapView removeObserver:self forKeyPath:@"myLocation"];
	}
}

#pragma mark - Custom methods

- (void)showTableInfo  {
	[UIView animateWithDuration:1.0f animations:^{
		_tbvMain.alpha = 1.0f;
	}];
}

- (void)hideTableInfo  {
	[UIView animateWithDuration:0.5f animations:^{
		_tbvMain.alpha = 0.0f;
	}];
}

- (void)showBtnRoute   {
	[UIView animateWithDuration:0.5f animations:^{
		_btnShowRoute.alpha = 1.0f;
	}];
}

- (void)hideBtnRoute   {
	[UIView animateWithDuration:0.5f animations:^{
		_tbvMain.alpha = 0.0f;
	}];
}

- (void)showWindowMaker{
	if(mapViewStatus == CallDriverFunction){
		[UIView animateWithDuration:1.0f animations:^{
			_viWindowMarker.alpha = 1.0f;
		}];
	}
}

- (void)hideWindowMaker{
	[UIView animateWithDuration:0.5f animations:^{
		_viWindowMarker.alpha = 0.0f;
	}];
}

- (void)createFlag     {
	_polyline.map = nil;
	if([self.txtFiController selectedStartTxtFi]){
		_startPointMarker.map = nil;
		_startPointMarker = [GMSMarker markerWithPosition:_txtFiController.startPlaceFounded.location.coordinate];
		_startPointMarker.icon = [UIImage imageNamed:@"startPin"];
		_startPointMarker.position = _txtFiController.startPlaceFounded.location.coordinate;
		_startPointMarker.map = _mapView;
	}else{
		_endPointMarker.map = nil;
		_endPointMarker = [GMSMarker markerWithPosition:_txtFiController.endPlaceFounded.location.coordinate];
		_endPointMarker.icon = [UIImage imageNamed:@"endPin"];
		_endPointMarker.position = _txtFiController.endPlaceFounded.location.coordinate;
		_endPointMarker.map = _mapView;
	}
	
	if(_txtFiController.startPlaceFounded != nil && _txtFiController.endPlaceFounded != nil){
		[self showBtnRoute];
	}
}

- (void)getLocation    {
	ShowNetworkActivityIndicator();
	
	CLLocationCoordinate2D newCoordinate = [_mapView.projection coordinateForPoint:_mapView.center];
	NSString *urlString = [NSString stringWithFormat:@"%@geocode/json?latlng=%f,%f&location_type=GEOMETRIC_CENTER&language=vi&key=%@",baseURL, newCoordinate.latitude, newCoordinate.longitude, kGGMapServerKey];
	NSURL *url = [NSURL URLWithString:urlString];
	NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
		
		if (!error) {
			
			NSError *error1;
			NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error1];
			NSString *status = [result objectForKey:@"status"];
			// Print adress to Pin
			if([status isEqualToString:@"OK"] && error == nil){
				// get string result from json
				NSDictionary *resultDic = [[result valueForKey:@"results"] firstObject];
				
				NSString *formatted_address = [resultDic objectForKey:@"formatted_address"];
				NSArray *addressArr = [formatted_address componentsSeparatedByString:@","];
				NSString *stringAddress;
				// Get address from string array
				if(addressArr.count > 2){
					stringAddress = [NSString stringWithFormat:@"%@, %@, %@", (NSString *)addressArr[0], (NSString *)addressArr[1], (NSString *)addressArr[2]];
				}else if(addressArr.count == 2){
					stringAddress = [NSString stringWithFormat:@"%@, %@", (NSString *)addressArr[0], (NSString *)addressArr[1]];
				}else if(addressArr.count == 1){
					stringAddress = [NSString stringWithFormat:@"%@@", (NSString *)addressArr[0]];
				}else{
					stringAddress = [NSString stringWithFormat:@"Not found address"];
				}
				FavouritePlace *newPlace = [FavouritePlace newPlaceWithName:stringAddress andCoordinate:newCoordinate];
				NSLog(@"Found new place: %@", stringAddress);
				dispatch_async(dispatch_get_main_queue(), ^{
					HideNetworkActivityIndicator();
					[self.txtFiController setNewPlaceForSelectedTxtFi:newPlace];
					
					if(mapViewStatus == CallDriverFunction){
						[self showTableInfo];
						[self showWindowMaker];
						// Create new start point marker
						_startPointMarker = [GMSMarker markerWithPosition:_txtFiController.startPlaceFounded.location.coordinate];
						_startPointMarker.icon = [UIImage imageNamed:@"startPin"];
						_startPointMarker.position = _txtFiController.startPlaceFounded.location.coordinate;
					}else if(mapViewStatus == ShowingRouteFunction){
						[self createFlag];
					}
					
				});
				
			}else{
				dispatch_async(dispatch_get_main_queue(), ^{
					[AlertLabel alertWithContent:@"Location is not available" inView:_txtFiController.view];
				});
			}
		}else{
			dispatch_async(dispatch_get_main_queue(), ^{
				[AlertLabel alertWithContent:@"Can not get location" inView:_txtFiController.view];
			});
		}
	}];
	
	[task resume];
}

- (void)drawPolylineOnMap: (GMSPolyline *)newPolyline{
	if (newPolyline != nil) {
		isShowingRoute = YES;
		_btnShowRoute.alpha = 0.0f;
		_imviPinCenter.alpha = 0.0f;
		
		// Delete old polyline
		self.polyline.map = nil;
		// Update region
		GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:[newPolyline.path coordinateAtIndex:0]  coordinate:[newPolyline.path coordinateAtIndex:newPolyline.path.count-1]];
		GMSCameraUpdate *boundCamera = [GMSCameraUpdate fitBounds:bounds withPadding:100.0];
		[self.mapView animateWithCameraUpdate:boundCamera];
		// Draw new direction
		self.polyline = newPolyline;
		self.polyline.map = self.mapView;
		self.polyline.strokeWidth = 3;
	}else{
		[self showAlertErrorConnection];
	}
}

- (void)getDirectionFromLocation:(CLLocationCoordinate2D)startLocation to:(CLLocationCoordinate2D)stopLocation{
	NSString *strRequest = [NSString stringWithFormat:@"%@directions/json?key=%@&language=vi&origin=%f,%f&destination=%f,%f",baseURL,kGGMapServerKey,startLocation.latitude,startLocation.longitude,stopLocation.latitude,stopLocation.longitude];
	NSURLSession *session = [NSURLSession sharedSession];
	
	__weak MainVC *weakSelf = self;
	[[session dataTaskWithURL:[NSURL URLWithString:strRequest] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
		if (!error) {
			NSError *parseError;
			NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
			if (!parseError) {
				NSDictionary *routes = [[result objectForKey:@"routes"] firstObject];
				NSDictionary *route = [routes objectForKey:@"overview_polyline"];
				if (route == nil) {
					dispatch_async(dispatch_get_main_queue(), ^{
						[weakSelf showAlertCantGetDirection];
					});
					return;
				}
				dispatch_async(dispatch_get_main_queue(), ^{
					NSString *overview_route = [route objectForKey:@"points"];
					GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
					GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
					[weakSelf drawPolylineOnMap:polyline];
				});
			}
		}else{
			[weakSelf showAlertErrorConnection];
		}
	}] resume];
}

#pragma mark - UITableViewDelegate & UITableViewDatasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 50.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
	return 0.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
	
	return 0.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

	UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"CellIdent"];
	
	switch (indexPath.row) {
		case 0:
		{
			cell.textLabel.text = @"Timer";
			cell.detailTextLabel.text = _timer?_timer:@"";
		}
			break;
			
		case 1:
		{
			cell.textLabel.text = @"Driver";
			cell.detailTextLabel.text = _driverName?_driverName:@"";
		}
			break;
		case 2:
		{
			cell.textLabel.text = @"Phone number";
			cell.detailTextLabel.text = _driverPhoneNumber?_driverPhoneNumber:@"";
		}
			break;
		case 3:
		{
			CGFloat tbnWidth = self.view.frame.size.width - 2*50.0f;
			CGFloat tbnHeight = 50.0f - 2*5.0f;
			
			UIButton *tbnSend = [[UIButton alloc] initWithFrame:CGRectMake(50.0f, 5.0f, tbnWidth, tbnHeight)];
			[tbnSend setTitle:@"Send" forState:UIControlStateNormal];
			[tbnSend setBackgroundImage:[Constants imageWithColor:kColorPink] forState:UIControlStateNormal];
			[tbnSend addTarget:self action:@selector(sendRequest:) forControlEvents:UIControlEventTouchUpInside];
			tbnSend.layer.cornerRadius = 10.0f;
			tbnSend.layer.masksToBounds = YES;
			
			[cell.contentView addSubview:tbnSend];
		}
			break;
		default:
			break;
	}
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	if(indexPath.row == 0){
		CountdownTimer *timer = [[CountdownTimer alloc] initWithFrame:self.view.frame];
		timer.delegate = self;
		[timer showTimer];
	}else{
		_addressBookController = [[ABPeoplePickerNavigationController alloc] init];
		[_addressBookController setPeoplePickerDelegate:self];
		
		[self presentViewController:_addressBookController animated:YES completion:nil];
	}
}

#pragma mark - GMSMapViewDelegate

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
	if(mapViewStatus == CallDriverFunction){
		if(!selectedSuggestionPlace){
			[UIViewController cancelPreviousPerformRequestsWithTarget:self selector:@selector(getLocation) object:nil];
			[self performSelector:@selector(getLocation) withObject:nil afterDelay:1.0];
		}else{
			[self showTableInfo];
			[self hideWindowMaker];
		}
	}else if(mapViewStatus == ShowingRouteFunction){
		if(!isShowingRoute){
			[UIViewController cancelPreviousPerformRequestsWithTarget:self selector:@selector(getLocation) object:nil];
			[self performSelector:@selector(getLocation) withObject:nil afterDelay:1.0];
		}
	}
}

- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture{
	
	if(gesture){
		selectedSuggestionPlace = NO;
	}
	
	[self.view endEditing:YES];
	if(mapViewStatus == CallDriverFunction){
		[self hideWindowMaker];
		if(gesture){
			[_txtFiController removeCacheAndFoundedPlace];
		}
		[self hideTableInfo];
	}else if(mapViewStatus == ShowingRouteFunction){
		if(!isShowingRoute){
			[_txtFiController removeCacheAndFoundedPlace];
		}else{
		}
	}
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
	[self.view endEditing:YES];
	
	[self hideWindowMaker];
	[_txtFiController hideSuggestionTable];
}


#pragma mark - UISegmentControl

- (IBAction)changeSegment:(id)sender{
	NSInteger segmentIndex = ((UISegmentedControl *)sender).selectedSegmentIndex;
	mapViewStatus = segmentIndex;
	_imviPinCenter.alpha = 1.0f;
	_endPointMarker.map = nil;
	_startPointMarker.map = nil;
	
	if(mapViewStatus == CallDriverFunction && _txtFiController.startPlaceFounded != nil){
		_tbvMain.alpha = 1.0f;
		_btnShowRoute.alpha = 0.0f;
		[_mapView clear];
	}else if(mapViewStatus == ShowingRouteFunction){
		_tbvMain.alpha = 0.0f;
		_viWindowMarker.alpha = 0.0f;
		
		if(isShowingRoute){
			_imviPinCenter.alpha = 0.0f;
			
			_endPointMarker.map = _mapView;
			_startPointMarker.map = _mapView;
			[self showRoute:_btnShowRoute];
		}else{
			_endPointMarker.map = _mapView;
			_startPointMarker.map = _mapView;
			
			_imviPinCenter.alpha = 1.0f;
		}
	}
	
	[_txtFiController mainVCChangeSegment:segmentIndex];
}

#pragma mark - IBAction

- (IBAction)sendRequest:(id)sender{
	/*
	// Not selected driver
	if([[NSUserDefaults standardUserDefaults] objectForKey:@"DriverNamePrevious"] == nil){
		[self dispatchAlertViewWithTitle:@"Can't call driver"
								 message:@"Please choose one driver"
					   cancelButtonTitle:nil
					 andOtherButtonTitle:@"Ok"];
		return;
	}
	
	// Not setup timer
	if(_timer == nil){
		[self dispatchAlertViewWithTitle:@"Can't call driver"
								 message:@"Please setup timer"
					   cancelButtonTitle:nil
					 andOtherButtonTitle:@"Ok"];
		return;
	}
	// Not pickup location yet
	if(_txtFiPickupLocation.text == nil){
		[self dispatchAlertViewWithTitle:@"Can't call driver"
								 message:@"Waiting for location"
					   cancelButtonTitle:nil
					 andOtherButtonTitle:@"Ok"];
		return;
	}
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	dateFormatter.dateFormat = @"hh:mm EEE dd/MM/YY";
	
	[[NSUserDefaults standardUserDefaults] setObject:@"Mr.X" forKey:@"UserPhone"];
	NSString *userPhone = [[NSUserDefaults standardUserDefaults] stringForKey:@"UserPhone"];
	NSString *body = [NSString stringWithFormat:@"userphone=%@&driverphone=%@&timereturn=%@&timego=%@&longtitude=%f&latitude=%f", userPhone, _driverPhoneNumber, @"0:20", [dateFormatter stringFromDate:[NSDate date]],_startCoordinate.longitude, _startCoordinate.latitude];
	body = [body stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://matiloca.esy.es/CallingDriverService.php?%@", body]];
	NSURLSessionDataTask *sendRequest = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
		if (!error) {
			NSError *error1;
			NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error1];
			
			if (!error1) {
				NSString *strResult = [result valueForKey:@"status"];
				
				if([strResult isEqualToString:@"1"]){
					[self dispatchAlertViewWithTitle:@"Successful"
											 message:@"Request have been sent"
								   cancelButtonTitle:nil
								 andOtherButtonTitle:@"Ok"];
				}else{
					// strResult == 0
					[self dispatchAlertViewWithTitle:@"Can't call driver"
											 message:@"Not found driver"
								   cancelButtonTitle:nil
								 andOtherButtonTitle:@"Ok"];
				}
			}else{
				// Serialization Fail
				NSLog(@"Serialization Error: %@",error1);
				[self dispatchAlertViewWithTitle:@"Can't call driver"
										 message:@"Something wrong with server"
							   cancelButtonTitle:nil
							 andOtherButtonTitle:@"Ok"];
			}
		}else{
			NSLog(@"Error : %@",error);
		}
	}];
	
	[sendRequest resume];
//	_favouritePlace = [[FavouritePlace alloc] init];
//	_favouritePlace.strPlaceName = _txtFiPickupLocation.text;
//	_favouritePlace.strPlaceTitle = @"Newest location";
//	_favouritePlace.location = [[CLLocation alloc] initWithLatitude:_startCoordinate.latitude longitude:_startCoordinate.longitude];
//	_favouritePlace.placeType = FavouritePlaceNewest;
//	[FavouritePlace addNewFavouritePlaces:_favouritePlace];
	
	ShowNetworkActivityIndicator();
	[self performSelector:@selector(checkStatus) withObject:nil afterDelay:10.0];
	 
	 */
}

- (IBAction)clickRightBarBtn:(id)sender{
	EditFavouritePlaceViewController *editFavouriteVC = [[EditFavouritePlaceViewController alloc] init];
	[self.navigationController pushViewController:editFavouriteVC animated:YES];
}

- (IBAction)showRoute:(id)sender{
	if(_txtFiController.startPlaceFounded != nil && _txtFiController.endPlaceFounded != nil){
		// Show direction
		[self getDirectionFromLocation:_txtFiController.startPlaceFounded.location.coordinate to:_txtFiController.endPlaceFounded.location.coordinate];
	}
}


#pragma mark - TextFieldsControllerDelegate

- (void)didSelectSuggestionPlace:(FavouritePlace *)suggestionPlace{
	selectedSuggestionPlace = YES;
	
	if(mapViewStatus == CallDriverFunction){
		[self showTableInfo];
		// Move Map camera
		GMSCameraUpdate *cameraUpdate = [GMSCameraUpdate setTarget:suggestionPlace.location.coordinate zoom:12];
		[_mapView moveCamera:cameraUpdate];
		
	}else if(mapViewStatus == ShowingRouteFunction){
		
		// Pin this location
//		if([_txtFiController selectedStartTxtFi]){
//			_startPointMarker = [GMSMarker markerWithPosition:suggestionPlace.location.coordinate];
//			_startPointMarker.icon = [UIImage imageNamed:@"startPin"];
//			_startPointMarker.map = _mapView;
//		}else{
//			_endPointMarker = [GMSMarker markerWithPosition:suggestionPlace.location.coordinate];
//			_endPointMarker.icon = [UIImage imageNamed:@"endPin"];
//			_endPointMarker.map = _mapView;
//		}
		[self createFlag];
		
		if(_txtFiController.startPlaceFounded != nil && _txtFiController.endPlaceFounded != nil){
			// Show direction
			[self getDirectionFromLocation:_txtFiController.startPlaceFounded.location.coordinate to:_txtFiController.endPlaceFounded.location.coordinate];
		}else{
			// Move Map camera
			GMSCameraUpdate *cameraUpdate = [GMSCameraUpdate setTarget:suggestionPlace.location.coordinate zoom:12];
			[_mapView moveCamera:cameraUpdate];
		}
	}
}

- (void)textFieldShouldBeginEditing{
	if(isShowingRoute){
		_imviPinCenter.alpha = 1.0f;
		isShowingRoute = !isShowingRoute;
	}
}


#pragma mark - Show alert

- (void)showAlertErrorConnection{
	[AlertLabel alertWithContent:@"Network connection error." inView:_txtFiController.view];
}

- (void)showAlertCantGetLocation{
	[AlertLabel alertWithContent:@"Can not locate your location. Please open Privacy in Setting" inView:_txtFiController.view];
}

- (void)showAlertCantGetDirection{
	[AlertLabel alertWithContent:@"Can not get direction." inView:_txtFiController.view];
}

#pragma mark - Get phonenumber from contacts

-(BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person{
	// IOS 7, deprecated in ios 8
	//	NSLog(@"IOS 7");
	
	// Initialize a mutable dictionary and give it initial values.
	NSMutableDictionary *contactInfoDict = [[NSMutableDictionary alloc]
											initWithObjects:@[@"", @"", @"", @"", @"", @"", @"", @"", @""]
											forKeys:@[@"firstName", @"lastName", @"mobileNumber", @"homeNumber", @"homeEmail", @"workEmail", @"address", @"zipCode", @"city"]];
	
	// Use a general Core Foundation object.
	CFTypeRef generalCFObject = ABRecordCopyValue(person, kABPersonFirstNameProperty);
	
	// Get the first name.
	if (generalCFObject) {
		[contactInfoDict setObject:(__bridge NSString *)generalCFObject forKey:@"firstName"];
		CFRelease(generalCFObject);
	}
	
	// Get the last name.
	generalCFObject = ABRecordCopyValue(person, kABPersonLastNameProperty);
	if (generalCFObject) {
		[contactInfoDict setObject:(__bridge NSString *)generalCFObject forKey:@"lastName"];
		CFRelease(generalCFObject);
	}
	
	// Get the phone numbers as a multi-value property.
	ABMultiValueRef phonesRef = ABRecordCopyValue(person, kABPersonPhoneProperty);
	for (int i=0; i<ABMultiValueGetCount(phonesRef); i++) {
		CFStringRef currentPhoneLabel = ABMultiValueCopyLabelAtIndex(phonesRef, i);
		CFStringRef currentPhoneValue = ABMultiValueCopyValueAtIndex(phonesRef, i);
		
		if(currentPhoneLabel){
			if (CFStringCompare(currentPhoneLabel, kABPersonPhoneMobileLabel, 0) == kCFCompareEqualTo) {
				[contactInfoDict setObject:(__bridge NSString *)currentPhoneValue forKey:@"mobileNumber"];
			}
			
			if (CFStringCompare(currentPhoneLabel, kABHomeLabel, 0) == kCFCompareEqualTo) {
				[contactInfoDict setObject:(__bridge NSString *)currentPhoneValue forKey:@"homeNumber"];
			}
		}
		
		CFRelease(currentPhoneLabel);
		CFRelease(currentPhoneValue);
	}
	CFRelease(phonesRef);
	
	NSString *phoneNumber = (NSString *)[contactInfoDict objectForKey:@"mobileNumber"];
	NSString *firstName = (NSString *)[contactInfoDict objectForKey:@"firstName"];
	NSString *lastName = (NSString *)[contactInfoDict objectForKey:@"lastName"];
	
	if([phoneNumber isEqualToString:@""]){
		UIAlertView *phoneNumberNotFound = [[UIAlertView alloc] initWithTitle:@"Mobile phone number not found" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
		[phoneNumberNotFound show];
		_driverPhoneNumber = nil;
		_driverName = nil;
	}else{
		_driverName = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
		_driverPhoneNumber = [NSString stringWithFormat:@"%@", phoneNumber];
		
		[[NSUserDefaults standardUserDefaults] setObject:_driverName forKey:@"DriverNamePrevious"];
		[[NSUserDefaults standardUserDefaults] setObject:_driverPhoneNumber	forKey:@"DriverPhoneNumberPrevious"];
		
		[_tbvMain reloadData];
	}
	
	
	// Dismiss the address book view controller.
	[_addressBookController dismissViewControllerAnimated:YES completion:nil];
	
	return NO;
}

-(BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier{
	// IOS 7, deprecated in ios 8
	return NO;
}

// This method called in IOS 8, and then it will call
- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
	NSLog(@"IOS 8");
	[self peoplePickerNavigationController:peoplePicker shouldContinueAfterSelectingPerson:person property:property identifier:identifier];
}

- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker
						 didSelectPerson:(ABRecordRef)person{
	NSLog(@"IOS 8");
	[self peoplePickerNavigationController:peoplePicker shouldContinueAfterSelectingPerson:person]; // IOS 7 method
}

-(void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker{
	[_addressBookController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - CountdownTimerDelegate

- (void)selectedTimer:(NSString *)timer{
	_timer = timer;
	[[NSUserDefaults standardUserDefaults] setObject:timer forKey:@"PreviousTime"];
	
	UITableViewCell *cell = [_tbvMain cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
	cell.detailTextLabel.text = timer;
}

#pragma mark - DestinationWindowMarkerDelegate

- (void)markNewPlace{
	[self hideWindowMaker];
	
	FavouritePlace *newPlace = _txtFiController.startPlaceFounded;
	newPlace.placeType = FavouritePlaceHome;
	// Show direction
	
	EditDetailPlaceVC *editVC = [[EditDetailPlaceVC alloc] init];
	editVC.place = newPlace;
	editVC.delegate = self;
	editVC.editingMode = CreateNewPlace;
	
	[self.navigationController pushViewController:editVC animated:YES];
}

#pragma mark - EditDetailPlaceProtocol

- (void)didSaveNewPlace{
	[self hideWindowMaker];
}

@end
