//
//  LoginVC.h
//  CallDriver
//
//  Created by Manh Tien on 4/6/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : int {
    AccessPermissionDriver = 0,
    AccessPermissionCEO
    
} kAccessPermission;

@interface LoginVC : UIViewController

@end
