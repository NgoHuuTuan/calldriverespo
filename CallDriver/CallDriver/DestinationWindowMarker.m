//
//  DestinationWindowMaker.m
//  CallDriver
//
//  Created by Ngo Huu Tuan on 3/30/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import "DestinationWindowMarker.h"
#import "Constants.h"


@interface DestinationWindowMarker()
@property (strong, nonatomic) UIButton *btnMarkHomePlace;
@property (strong, nonatomic) UIButton *btnMarkWorkPlace;
@property (strong, nonatomic) UIButton *btnMarkNewPlace;
@end

@implementation DestinationWindowMarker

- (instancetype)initWithFrame:(CGRect)frame{
	if (self = [super initWithFrame:frame]) {
		
		self.backgroundColor = kColorPink;
		self.layer.masksToBounds = YES;
		self.layer.cornerRadius = 20.0f;
		
//		UIImageView *imgviBackground = [[UIImageView alloc] init];
//		imgviBackground.translatesAutoresizingMaskIntoConstraints = NO;
//		imgviBackground.userInteractionEnabled = YES;
//		imgviBackground.contentMode = UIViewContentModeScaleToFill;
//		[self addSubview:imgviBackground];
//		
//		_btnMarkHomePlace = [[UIButton alloc] init];
//		_btnMarkHomePlace.translatesAutoresizingMaskIntoConstraints = NO;
//		[_btnMarkHomePlace setBackgroundImage:[UIImage imageNamed:@"home"] forState:UIControlStateNormal];
//		[_btnMarkHomePlace addTarget:self action:@selector(markToHomePlace:) forControlEvents:UIControlEventTouchUpInside];
//		
//		_btnMarkWorkPlace = [[UIButton alloc] init];
//		_btnMarkWorkPlace.translatesAutoresizingMaskIntoConstraints = NO;
//		[_btnMarkWorkPlace setBackgroundImage:[UIImage imageNamed:@"work"] forState:UIControlStateNormal];
//		[_btnMarkWorkPlace addTarget:self action:@selector(markToWorkPlace:) forControlEvents:UIControlEventTouchUpInside];
//		
//		UILabel *lblCenter = [[UILabel alloc] init];
//		lblCenter.text = @"Mark your destination";
//		lblCenter.translatesAutoresizingMaskIntoConstraints = NO;
//		lblCenter.textColor = [UIColor whiteColor];
//		lblCenter.font = [UIFont systemFontOfSize:16];
//		lblCenter.numberOfLines = 1;
//		lblCenter.minimumScaleFactor = 5;
//		lblCenter.adjustsFontSizeToFitWidth = YES;
//		
//		[imgviBackground addSubview:_btnMarkWorkPlace];
//		[imgviBackground addSubview:_btnMarkHomePlace];
//		[imgviBackground addSubview:lblCenter];
//        [imgviBackground setUserInteractionEnabled:YES];
		// AutoLayout
//		NSDictionary *views = @{
//								@"imgviBG":imgviBackground,
//								@"btnHome":_btnMarkHomePlace,
//								@"btnWork":_btnMarkWorkPlace,
//								@"lblCenter":lblCenter
//								};
//		
//		NSDictionary *metrics = @{
//								  @"btnSquare":@(40),
//								  };
		
//		NSArray *imgviBG_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[imgviBG]-0-|" options:0 metrics:nil views:views];
//		NSArray *imgviBG_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[imgviBG]-0-|" options:0 metrics:nil views:views];
//		[self addConstraints:imgviBG_H];
//		[self addConstraints:imgviBG_V];
//		
//		NSArray *btnHomeCons_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[btnHome(btnSquare)]" options:0 metrics:metrics views:views];
//		NSArray *btnHomeCons_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[btnHome]-0-|" options:0 metrics:metrics views:views];
//		[imgviBackground addConstraints:btnHomeCons_H];
//		[imgviBackground addConstraints:btnHomeCons_V];
//		
//		NSArray *btnWorkCons_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[btnWork(btnSquare)]-0-|" options:0 metrics:metrics views:views];
//		NSArray *btnWorkCons_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[btnWork]-0-|" options:0 metrics:metrics views:views];
//		[imgviBackground addConstraints:btnWorkCons_V];
//		[imgviBackground addConstraints:btnWorkCons_H];
//		
//		NSArray *lblCenterCons_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[btnHome]-0-[lblCenter]-0-[btnWork]" options:0 metrics:metrics views:views];
//		NSArray *lblCenterCons_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[lblCenter]-0-|" options:0 metrics:metrics views:views];
//		[imgviBackground addConstraints:lblCenterCons_V];
//		[imgviBackground addConstraints:lblCenterCons_H];
		
		_btnMarkNewPlace = [[UIButton alloc] init];
		_btnMarkNewPlace.translatesAutoresizingMaskIntoConstraints = NO;
		[_btnMarkNewPlace setTitle:@"Mark your destination" forState:UIControlStateNormal];
		[_btnMarkNewPlace setBackgroundImage:[Constants imageWithColor:kColorPink] forState:UIControlStateNormal];
		[_btnMarkNewPlace addTarget:self action:@selector(markToHomePlace:) forControlEvents:UIControlEventTouchUpInside];
		[self addSubview:_btnMarkNewPlace];
		
		NSArray *btnCons_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[_btnMarkNewPlace]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_btnMarkNewPlace)];
		NSArray *btnCons_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[_btnMarkNewPlace]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_btnMarkNewPlace)];
		[self addConstraints:btnCons_H];
		[self addConstraints:btnCons_V];
		
		[self updateConstraintsIfNeeded];
		[self layoutIfNeeded];
	}
	
	return self;
}

- (void)updateConstraints{
	[super updateConstraints];
}

- (IBAction)markToHomePlace:(id)sender{
	[self.delegate markNewPlace];
}


@end
