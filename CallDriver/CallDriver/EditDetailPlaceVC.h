//
//  EditDetailPlaceVC.h
//  CallDriver
//
//  Created by Manh Tien on 4/5/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FavouritePlace.h"

typedef enum{
	CreateNewPlace,
	EditingPreviousPlace
}EditDetailVCStatus;

@protocol EditDetailPlaceProtocol <NSObject>

- (void)didSaveNewPlace;

@end

@interface EditDetailPlaceVC : UIViewController

@property (strong,nonatomic) FavouritePlace *place;
@property (assign, nonatomic) NSInteger editingMode;
@property (weak, nonatomic) id <EditDetailPlaceProtocol> delegate;

@end
