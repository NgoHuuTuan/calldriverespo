//
//  CustomeTimePicker.m
//  CleanBasket
//
//  Created by Ngo Huu Tuan on 3/1/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import "DatePickerView.h"
#import "Constants.h"


@interface DatePickerView ()
@property (strong, nonatomic) UIDatePicker *datePicker;
@property (strong, nonatomic) UIButton *btnOk;
@end


@implementation DatePickerView

- (instancetype)initWithFrame:(CGRect)frame{
	self = [super initWithFrame:frame];
	
	if(self){
		
		self.backgroundColor = [UIColor colorWithRed:20/255 green:20/255 blue:20/255 alpha:0.7];
		
		UIView *anView = [[UIView alloc] init];
		anView.backgroundColor = [UIColor whiteColor];
		anView.layer.cornerRadius = kCornerRadius;
		anView.layer.shadowColor = [UIColor grayColor].CGColor;
		anView.layer.shadowOpacity = 0.5;
		anView.translatesAutoresizingMaskIntoConstraints = NO;
		[self addSubview:anView];
		
		
		_datePicker = [[UIDatePicker alloc] init];
		_datePicker.datePickerMode = UIDatePickerModeTime;
		_datePicker.translatesAutoresizingMaskIntoConstraints = NO;
		[self addSubview:_datePicker];
		
		
		_btnOk = [[UIButton alloc] init];
		[_btnOk addTarget:self action:@selector(chooseThatDate:) forControlEvents:UIControlEventTouchUpInside];
		[_btnOk setTitle:@"OK" forState:UIControlStateNormal];
		[_btnOk setBackgroundImage:[Constants imageWithColor:kColorPink] forState:UIControlStateNormal];
		_btnOk.layer.cornerRadius = 10.0f;
		_btnOk.layer.masksToBounds = YES;
		_btnOk.translatesAutoresizingMaskIntoConstraints = NO;
		[self addSubview:_btnOk];
		
		self.alpha = 0.0f;
		
		// Create timePicker constraints
		NSLayoutConstraint *timePickerCons_Leading = [NSLayoutConstraint constraintWithItem:_datePicker attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1 constant:40];
		NSLayoutConstraint *timePickerCons_Trailing = [NSLayoutConstraint constraintWithItem:_datePicker attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1 constant:-40];
		NSLayoutConstraint *timePickerCons_Height = [NSLayoutConstraint constraintWithItem:_datePicker attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:200];
		NSLayoutConstraint *timePickerCons_CenterY = [NSLayoutConstraint constraintWithItem:_datePicker attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
		[self addConstraint:timePickerCons_Leading];
		[self addConstraint:timePickerCons_Trailing];
		[self addConstraint:timePickerCons_Height];
		[self addConstraint:timePickerCons_CenterY];
		
		
		// Create _btnOk constraints
		NSLayoutConstraint *btnOkCons_Leading = [NSLayoutConstraint constraintWithItem:_btnOk attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1 constant:60];
		NSLayoutConstraint *btnOkCons_Trailing = [NSLayoutConstraint constraintWithItem:_btnOk attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1 constant:-60];
		NSLayoutConstraint *btnOkCons_Height = [NSLayoutConstraint constraintWithItem:_btnOk attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:50];
		NSLayoutConstraint *btnOkCons_Top = [NSLayoutConstraint constraintWithItem:_btnOk attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_datePicker attribute:NSLayoutAttributeBottom multiplier:1 constant:40];
		[self addConstraint:btnOkCons_Leading];
		[self addConstraint:btnOkCons_Trailing];
		[self addConstraint:btnOkCons_Height];
		[self addConstraint:btnOkCons_Top];
		
		
		// Create anview constraints
		NSLayoutConstraint *anViewCons_Leading = [NSLayoutConstraint constraintWithItem:anView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:_datePicker attribute:NSLayoutAttributeLeading multiplier:1 constant:-20];
		NSLayoutConstraint *anViewCons_Trailing = [NSLayoutConstraint constraintWithItem:anView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:_datePicker attribute:NSLayoutAttributeTrailing multiplier:1 constant:20];
		NSLayoutConstraint *anViewCons_Bottom = [NSLayoutConstraint constraintWithItem:anView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_btnOk attribute:NSLayoutAttributeBottom multiplier:1 constant:20];
		NSLayoutConstraint *anViewCons_Top = [NSLayoutConstraint constraintWithItem:anView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_datePicker attribute:NSLayoutAttributeTop multiplier:1 constant:-20];
		[self addConstraint:anViewCons_Leading];
		[self addConstraint:anViewCons_Trailing];
		[self addConstraint:anViewCons_Bottom];
		[self addConstraint:anViewCons_Top];
		
	}
		
	return self;
}

- (IBAction)chooseThatDate:(id)sender{
	[self.delegate selectedDate:_datePicker.date];
}

- (void)show{
	
	_datePicker.datePickerMode = UIDatePickerModeDate;

	[WINDOW addSubview:self];
	[UIView animateWithDuration:0.5f animations:^{
		self.alpha = 1.0f;
	}];
}

- (void)hide{
	
	[UIView animateWithDuration:0.4f animations:^{
		self.alpha = 0.0f;
	} completion:^(BOOL finished) {
		[self removeFromSuperview];
	}];
}

- (void)showDateTimePicker{
	
	_datePicker.datePickerMode = UIDatePickerModeDateAndTime;
	
	[WINDOW addSubview:self];
	[UIView animateWithDuration:0.5f animations:^{
		self.alpha = 1.0f;
	}];
}

- (void)showTimer{
	
	_datePicker.datePickerMode = UIDatePickerModeCountDownTimer;
//	_datePicker.countDownDuration = 300;
	_datePicker.minuteInterval = 5;
	
	[WINDOW addSubview:self];
	[UIView animateWithDuration:0.5f animations:^{
		self.alpha = 1.0f;
	}];
}

@end
