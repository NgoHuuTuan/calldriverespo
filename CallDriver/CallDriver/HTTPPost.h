//
//  HTTPPost.h
//  YellowPage2
//
//  Created by EuiHwa Park on 12. 6. 28..
//  Copyright (c) 2012년 pehsys@gmail.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTTPPost : NSObject

/* HTTP DELETE/POST/PUT 방식으로 처리하기 */
+ (NSString *)HTTPDeleteWithURL:(NSString *)url;
+ (NSString *)HTTPPutWithURL:(NSString *)url withBody:(NSString *)body;
+ (NSString *)HTTPPostWithURL:(NSString *)url withBody:(NSString *)body;
+ (NSString *)utf8HTTPPostWithURL:(NSString *)url withBody:(NSString *)body;

@end
