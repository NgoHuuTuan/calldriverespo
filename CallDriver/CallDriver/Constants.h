//
//  Constants.h
//  CleanBasket
//
//  Created by Manh Tien on 2/4/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Define.h"
#import <UIKit/UIKit.h>


#define kUserName @"username"
#define kUserAccessToken @"accesstoken"
#define kEmail @"email"
#define kPermission @"access_permission"

#define kCornerRadius 10.0f

#define kBackgroundColor RGB(251,252,252)
#define kColorBlue RGB(62, 151, 209)

#define kColorPink [UIColor colorWithRed:255/255.0f green:79/255.0f blue:113/255.0f alpha:1]

#define kColorBlueHighlighted RGB(20, 100, 200)
#define kColorYellow RGB(255, 162, 1)
#define kColorRed RGB(255, 79, 113)

#define kColorPlaceholder [UIColor colorWithWhite:1 alpha:0.58]
#define kColorText [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]

#define kColorGray  [UIColor colorWithRed:107.0f/255 green:107.0f/255 blue:107.0f/255 alpha:1.0f]


#define WINDOW [[[UIApplication sharedApplication] delegate] window]
#define ShowNetworkActivityindicator() [UIApplication shareApplication].networkActivityIndicatorVisible = YES
#define HideNetworkActivityindicator() [UIApplication shareApplication].networkActivityIndicatorVisible = NO

static NSString* const kAPIkey = @"AIzaSyBai-zLOUBu9tgVoZYKT-_8JS18FWyW7WE"; //@"AIzaSyCLgl6SzZxdGuNjBsNjSceT_DivXpcZGD8";
static NSString* const kGGMapServerKey = @"AIzaSyBsjMDPh_mw2q461_TQGpnOr2nrC2VB-8k"; // @"AIzaSyCOSQXhLTecAdW8N0-Le7EDjz-KNbIdjsc";
static NSString* const baseURL = @"https://maps.googleapis.com/maps/api/";

static NSString* const SERVER_URLSTRING = @"http://matiloca.esy.es/CallingDriverService.php";

static NSString *const PARSE_APP_ID = @"qNIOMuOprhHkKa27cjSJPbK1Zzy2jyEZIiR4OEb3";
static NSString *const PARSE_CLIENT_KEY = @"nKfHacqYEgrpXPHIkoQB1ru3lGvE2e6JRYYkoeNq";
static NSString *const PARSE_SERVER = @"https://parseapi.back4app.com/";

@interface Constants : NSObject

+ (UIImage *)imageWithColor:(UIColor *)color;
+ (void) saveString: (NSString *) value ForKey: (NSString *) key;
+(NSString *) getStringForKey: (NSString *) key;


@end

