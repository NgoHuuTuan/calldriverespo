//
//  Constants.m
//  CleanBasket
//
//  Created by Manh Tien on 2/12/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import "Constants.h"


@implementation Constants


+ (UIImage *)imageWithColor:(UIColor *)color {
	CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
	UIGraphicsBeginImageContext(rect.size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	CGContextSetFillColorWithColor(context, [color CGColor]);
	CGContextFillRect(context, rect);
	
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return image;
}


+ (void)saveString:(NSString *)value ForKey:(NSString *)key{
    [defaults setObject:value forKey:key];
    [defaults synchronize];
}

+ (NSString *)getStringForKey:(NSString *)key{
    return [defaults objectForKey:key];
}

@end
