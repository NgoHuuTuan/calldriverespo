//
//  TextFieldsController.h
//  CallDriver
//
//  Created by Ngo Huu Tuan on 7/5/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FavouritePlace.h"

@protocol TxtFiControllerDelegate <NSObject>

@required
//- (void)getLocationIDFormText:(NSString *)searchString;
- (void)didSelectSuggestionPlace:(FavouritePlace *)suggestionPlace;
- (void)didPressCancelTextFieldIndex:(NSInteger)textFiIndex;
- (void)textFieldShouldBeginEditing;

@end


@interface TextFieldsController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *txtStartLocation;
@property (weak, nonatomic) IBOutlet UITextField *txtEndLocation;
//@property (weak, nonatomic) IBOutlet UITableView *tbvSuggestion;

@property (strong, nonatomic)  UITableView *tbvSuggestion;

@property (strong, nonatomic) UITextField *txtFiSelected;

@property (strong, nonatomic) FavouritePlace *startPlaceFounded;					// Place founed by txtStartLocation
@property (strong, nonatomic) FavouritePlace *endPlaceFounded;						// Place founed by txtEndLocation

@property (weak, nonatomic) id <TxtFiControllerDelegate> delegate;

- (void)mainVCChangeSegment:(NSInteger)segmentIndex;
- (void)removeCacheAndFoundedPlace;
- (void)hideSuggestionTable;
- (void)setNewPlaceForSelectedTxtFi:(FavouritePlace *)newPlace;

- (BOOL)showingRoute;
- (BOOL)selectedStartTxtFi;
@end
