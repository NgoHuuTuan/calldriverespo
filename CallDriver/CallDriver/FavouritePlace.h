//
//  FavouritePlace.h
//  CallDriver
//
//  Created by Manh Tien on 3/29/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#define kFavouritePlaceEncodeListPlaces @"listplaces"
#define kFavouritePlaceEncodeName @"placename"
#define kFavouritePlaceEncodeTitle @"placetitle"
#define kFavouritePlaceEncodeLocationLong @"longtitude"
#define kFavouritePlaceEncodeLongcationLat @"latitude"
#define kFavouritePlaceEncodePlaceType @"placetype"

//#define kFavouritePlaceMaxPlace 3
#define kFavouritePlaceMaxHome 1
#define kFavouritePlaceMaxWork 2
#define kFavouritePlaceMaxOthers 3
//#define kFavouritePlaceMaxNewest 3

typedef enum{
    FavouritePlaceHome = 0,
    FavouritePlaceWork,
    FavouritePlaceOthers,
    FavouritePlaceNewest
}kFavouritePlaceType;

@interface FavouritePlace : NSObject


@property (strong,nonatomic) NSString *strPlaceName;
@property (strong,nonatomic) CLLocation *location;
@property (strong,nonatomic) NSString *strPlaceTitle;
@property (assign,nonatomic) kFavouritePlaceType placeType;
@property (strong,nonatomic) NSString *wrapPlaceType;


+ (void)saveListFavouritePlaces:(NSArray *)listPlaces;
+ (NSMutableArray *)loadListFavouritePlaces;
+ (NSArray *)filterListFavouritePlacesByType: (kFavouritePlaceType)placeType;
+ (void)addNewFavouritePlaces:(FavouritePlace *)place;
+ (void)removeFavouritePlaces:(FavouritePlace *)place;

- (instancetype)initWithPlaceName:(NSString *)placeName;
+ (NSArray *)loadListFavouritePlacesOfType:(kFavouritePlaceType)placeType;


/*
 @ Author Henry
 @
 */

+ (instancetype)newPlaceWithName:(NSString *)placeName andCoordinate:(CLLocationCoordinate2D)coor;

@end
