//
//  EditFavouritePlaceViewController.m
//  CallDriver
//
//  Created by Manh Tien on 4/2/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import "EditFavouritePlaceViewController.h"
#import "FavouritePlace.h"
#import "EditDetailPlaceVC.h"
#import "Constants.h"
#import "ViewController.h"
#import "LoginVC.h"
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import <Bolts/Bolts.h>


@interface EditFavouritePlaceViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (strong,nonatomic) UITableView *tableView;

@property (strong,nonatomic) NSMutableArray *arrListHomeFavorPlace;
@property (strong,nonatomic) NSMutableArray *arrListWorkFavorPlace;
@property (strong,nonatomic) NSMutableArray *arrListOthersFavorPlace;

@property (strong,nonatomic) FavouritePlace *selectedPlace;
@end

@implementation EditFavouritePlaceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	[self createInterface];
//	CGRect btnFrame = CGRectMake(0, 0, 30, 30);
//	UIButton *logoutBtn = [[UIButton alloc] initWithFrame:btnFrame];
//	[logoutBtn setImage:[UIImage imageNamed:@"SettingIcon"] forState:UIControlStateNormal];
//	[logoutBtn setTitle:@"Logout" forState:UIControlStateNormal];
//	[logoutBtn addTarget:self action:@selector(clickLogoutBtn:) forControlEvents:UIControlEventTouchUpInside];
//	UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithCustomView:logoutBtn];
	UIBarButtonItem *logouBar = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(loutOut:)];
	self.navigationItem.rightBarButtonItem = logouBar;
}

- (void)createInterface{
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    _tableView.translatesAutoresizingMaskIntoConstraints = NO;
	_tableView.delegate = self;
	_tableView.dataSource = self;
	[self.view addSubview:_tableView];
    
    // Autolayout
    NSDictionary *views = @{
                            @"tableView":_tableView,
							@"toplayoutGuide":self.topLayoutGuide
                            };
    NSArray *tbv_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[tableView]-0-|" options:0 metrics:nil views:views];
    NSArray *tbv_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[toplayoutGuide]-0-[tableView]-0-|" options:0 metrics:nil views:views];
    
    [self.view addConstraints:tbv_H];
    [self.view addConstraints:tbv_V];
    
	

//	_arrListHomeFavorPlace = [FavouritePlace loadListFavouritePlaces];
//	_arrListWorkFavorPlace = [FavouritePlace loadListFavouritePlaces];
//	_arrListOthersFavorPlace = [FavouritePlace loadListFavouritePlaces];
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	
	_arrListHomeFavorPlace = [NSMutableArray arrayWithArray:[FavouritePlace loadListFavouritePlacesOfType:FavouritePlaceHome]];
	_arrListWorkFavorPlace = [NSMutableArray arrayWithArray:[FavouritePlace loadListFavouritePlacesOfType:FavouritePlaceWork]];
	_arrListOthersFavorPlace = [NSMutableArray arrayWithArray:[FavouritePlace loadListFavouritePlacesOfType:FavouritePlaceOthers]];
	
	[_tableView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate and Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return (IS_IPHONE_4)?40.0f:50.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FavouritePlaceCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"FavouritePlaceCell"];
    }
    FavouritePlace *place = nil;
    switch (indexPath.section) {
        case 0:     // Home
        {
            cell.imageView.image = [UIImage imageNamed:@"homeicon"];
            place = [_arrListHomeFavorPlace objectAtIndex:indexPath.row];
            break;
        }
        case 1:{    // Work
            cell.imageView.image = [UIImage imageNamed:@"workicon"];
            place = [_arrListWorkFavorPlace objectAtIndex:indexPath.row];
			break;
        }
        case 2:{    // Others
            cell.imageView.image = [UIImage imageNamed:@"othersicon"];
            place = [_arrListOthersFavorPlace objectAtIndex:indexPath.row];
			break;
        }
        default:
            break;
    }

    cell.textLabel.text = place.strPlaceTitle;
    cell.detailTextLabel.text = place.strPlaceName;

    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0:     // Home
            return [_arrListHomeFavorPlace count];
        case 1:     // Work
            return [_arrListWorkFavorPlace count];
        case 2:     // Others
            return [_arrListOthersFavorPlace count];
        default:
            return 0;
    }
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return @"Home";
        case 1:
            return @"Work";
        case 2:
            return @"Others";
            
        default:
            return @"";
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
	return YES;
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
	if(editingStyle == UITableViewCellEditingStyleDelete){
		NSLog(@"Delete cell");
		if(indexPath.section == 0){
			_selectedPlace = _arrListHomeFavorPlace[indexPath.row];
			[_arrListHomeFavorPlace removeObject:_selectedPlace];
		}else if (indexPath.section == 1){
			_selectedPlace = _arrListWorkFavorPlace[indexPath.row];
			[_arrListWorkFavorPlace removeObject:_selectedPlace];
		}else if (indexPath.section == 2){
			_selectedPlace = _arrListOthersFavorPlace[indexPath.row];
			[_arrListOthersFavorPlace removeObject:_selectedPlace];
		}
		[FavouritePlace removeFavouritePlaces:_selectedPlace];
		[_tableView reloadData];
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//	FavouritePlace *selectedPlace;
	if(indexPath.section == 0){
		_selectedPlace = _arrListHomeFavorPlace[indexPath.row];
	}else if (indexPath.section == 1){
		_selectedPlace = _arrListWorkFavorPlace[indexPath.row];
	}else if (indexPath.section == 2){
		_selectedPlace = _arrListOthersFavorPlace[indexPath.row];
	}
	
	EditDetailPlaceVC *editDetailVC = [[EditDetailPlaceVC alloc] init];
	editDetailVC.place = _selectedPlace;
	editDetailVC.editingMode = EditingPreviousPlace;
	[self.navigationController pushViewController:editDetailVC animated:YES];
}


#pragma mark - UIButton action

- (IBAction)loutOut:(id)sender{
	ViewController *viewController = [[ViewController alloc] init];
	[PFUser logOutInBackgroundWithBlock:^(NSError * _Nullable error) {
        if (!error) {
            LoginVC *loginVC = [[LoginVC alloc] init];
            WINDOW.rootViewController = loginVC;
        }else{
            NSLog(@"SignOut error: %@",error.description);
        }
    }];
	
//	LoginVC *loginVC = [[LoginVC alloc] init];
//	WINDOW.rootViewController = loginVC;
}

@end
