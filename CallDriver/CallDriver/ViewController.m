//
//  ViewController.m
//  CallDriver
//
//  Created by Ngo Huu Tuan on 3/26/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import "ViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Constants.h"
#import <AddressBook/ABAddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "DriverViewController.h"
#import "CallingInformation.h"
#import "CountdownTimer.h"
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import "DestinationWindowMarker.h"
#import "FavouritePlace.h"
#import "AlertLabel.h"
#import "EditDetailPlaceVC.h"
#import "EditFavouritePlaceViewController.h"

#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import <Bolts/Bolts.h>
#import "MBProgressHUD.h"

typedef enum {
	CallDriveFunction = 0,
	RouteFunction,
	ShowingSuggestionPlace
}kMapViewStatus;

@interface ViewController () <GMSMapViewDelegate, CountdownTimerDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, ABPeoplePickerNavigationControllerDelegate, DestinationWindowMarkerDelegate,UIAlertViewDelegate,MFMessageComposeViewControllerDelegate>

@property (strong, nonatomic) GMSMapView *mapView;
@property (strong,nonnull) GMSPolyline *polyline;
@property (strong , nonatomic) GMSAutocompleteFilter *autocompleteFilterGMS;
@property (strong , nonatomic) GMSMarker *pickupMarker;
@property (strong , nonatomic) GMSMarker *destinationMarker;

@property (assign, nonatomic) CGFloat cellHeight;
@property (strong, nonatomic) UITableView *tbvMain;
@property (strong, nonatomic) UIImageView *imviCenterMarker;

@property (strong , nonatomic) UITableView *tbvShowingPlaces;
@property (strong , nonatomic) NSMutableArray *arrPlaces;
@property (strong , nonatomic) NSMutableArray *arrSuggestionPlaces;

@property (strong, nonatomic) UITextField *txtFiPickupLocation;
@property (strong, nonatomic) UITextField *txtFiDestination;
@property (strong, nonatomic) UITextField *txtFiSelected;

@property (assign, nonatomic) CLLocationCoordinate2D destinationCoor;
@property (assign, nonatomic) CLLocationCoordinate2D pickUpCoordinate;

@property (copy, nonatomic) NSString *driverPhoneNumber;
@property (copy, nonatomic) NSString *driverName;
@property (copy, nonatomic) NSString *timer;

@property (strong, nonatomic) DestinationWindowMarker *viWindowMarker;
@property (strong, nonatomic) FavouritePlace *favouritePlace;

@property (strong, nonatomic) UIButton *btnShowDirection;
@property (assign, nonatomic) kMapViewStatus mapViewStatus;

@property (assign, nonatomic) BOOL showingPreviousPlaces; // Showing previous places or suggestion places

@property (nonatomic, strong) ABPeoplePickerNavigationController *addressBookController;

@property (strong,nonatomic) PFUser *driver;
@property (strong,nonatomic) NSString *driverTargetAddress;
@end

@implementation ViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

	[self constructUIElements];
	
	// Segment for
	UISegmentedControl *featureSegment = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Call driver",@"Direction", nil]];
	[featureSegment addTarget:self action:@selector(changeSegment:) forControlEvents:UIControlEventValueChanged];
	featureSegment.selectedSegmentIndex = 0;
	self.navigationItem.titleView = featureSegment;

	CGRect btnFrame = CGRectMake(0, 0, 30, 30);
	UIButton *leftBarBtn = [[UIButton alloc] initWithFrame:btnFrame];
	[leftBarBtn setImage:[UIImage imageNamed:@"EditIcon"] forState:UIControlStateNormal];
	[leftBarBtn addTarget:self action:@selector(clickLeftBarBtn:) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftBarItem = [[UIBarButtonItem alloc] initWithCustomView:leftBarBtn];
	self.navigationItem.rightBarButtonItem =  leftBarItem;

	
//	_placeClientGSM = [[GMSPlacesClient alloc] init];
	
	_autocompleteFilterGMS = [[GMSAutocompleteFilter alloc] init];
	_autocompleteFilterGMS.type = kGMSPlacesAutocompleteTypeFilterNoFilter; // Filter results

    [self getDriverInfor];
}

- (void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	// Detect editviewcontroller have been poped off from navigation stack (we're already on the navigation stack)
	if (self.isMovingToParentViewController == NO){
		if(_tbvShowingPlaces.alpha == 1.0){
			[_tbvShowingPlaces reloadData];
		}
	}
    
    
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.

}

- (void)constructUIElements{
	
	_mapView = [[GMSMapView alloc] init];
	_tbvMain = [[UITableView alloc] init];
	_imviCenterMarker = [[UIImageView alloc] init];
	_txtFiPickupLocation = [[UITextField alloc] init];
	_txtFiDestination = [[UITextField alloc] init];
	_tbvShowingPlaces = [[UITableView alloc] init]; //WithFrame:CGRectMake(20, 120, ScreenWidth - 20 - 20, ScreenHeight - 140) style:UITableViewStyleGrouped];
	
	
	_mapView.translatesAutoresizingMaskIntoConstraints = NO;
	_mapView.settings.compassButton = YES;
	_mapView.settings.myLocationButton = YES;
	_mapView.myLocationEnabled = YES;
	_mapView.delegate = self;
    [_mapView addObserver:self forKeyPath:@"myLocation" options:0 context:nil];
    

	_tbvMain.translatesAutoresizingMaskIntoConstraints = NO;
	_tbvMain.delegate = self;
	_tbvMain.dataSource = self;
	_tbvMain.delaysContentTouches = NO;
	_tbvMain.alwaysBounceVertical = NO;
	_tbvMain.separatorStyle = UITableViewCellSeparatorStyleNone;
	_tbvMain.tag = 1;
	
	_imviCenterMarker.translatesAutoresizingMaskIntoConstraints = NO;
	_imviCenterMarker.image = [UIImage imageNamed:@"Pin"];
	_imviCenterMarker.contentMode = UIViewContentModeCenter;
	
	_txtFiPickupLocation.translatesAutoresizingMaskIntoConstraints = NO;
	_txtFiPickupLocation.borderStyle = UITextBorderStyleRoundedRect;
	_txtFiPickupLocation.backgroundColor = [UIColor whiteColor];
	_txtFiPickupLocation.textColor = [UIColor darkGrayColor];
	_txtFiPickupLocation.placeholder = @"Go To Pin";
	_txtFiPickupLocation.text = nil;
	_txtFiPickupLocation.delegate = self;
	

	_txtFiDestination.translatesAutoresizingMaskIntoConstraints = NO;
	_txtFiDestination.borderStyle = UITextBorderStyleRoundedRect;
	_txtFiDestination.backgroundColor = [UIColor whiteColor];
	_txtFiDestination.textColor = [UIColor darkGrayColor];
	_txtFiDestination.placeholder = @"Destination";
	_txtFiDestination.text = nil;
	_txtFiDestination.delegate = self;
	_txtFiDestination.clearButtonMode = UITextFieldViewModeAlways;
	_txtFiDestination.alpha = 0.0f;
	
	_viWindowMarker = [[DestinationWindowMarker alloc] initWithFrame:CGRectMake(ScreenWidth/2.0 - 120, ScreenHeight/2.0 - 40 - 40, 240, 40)];
	_viWindowMarker.delegate = self;
	_viWindowMarker.alpha = 0.0f;
	
	_tbvShowingPlaces.translatesAutoresizingMaskIntoConstraints = NO;
	_tbvShowingPlaces.delegate = self;
	_tbvShowingPlaces.dataSource = self;
	_tbvShowingPlaces.delaysContentTouches = NO;
	_tbvShowingPlaces.alpha = 0;
	_tbvShowingPlaces.tag = 2;
	_tbvShowingPlaces.backgroundColor = [UIColor clearColor];
	_tbvShowingPlaces.showsVerticalScrollIndicator = NO;
	_tbvShowingPlaces.alwaysBounceVertical = YES;
	_tbvShowingPlaces.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
	
	
	UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapTableHandle:)];
	[_tbvShowingPlaces addGestureRecognizer:tap];
	
	_btnShowDirection = [[UIButton alloc] init];
	[_btnShowDirection setBackgroundImage:[UIImage imageNamed:@"direction"] forState:UIControlStateNormal];
	[_btnShowDirection addTarget:self action:@selector(showDirection:) forControlEvents:UIControlEventTouchUpInside];
	_btnShowDirection.translatesAutoresizingMaskIntoConstraints = NO;
	_btnShowDirection.alpha = 0.0f;
	_btnShowDirection.backgroundColor = [UIColor whiteColor];
	_btnShowDirection.layer.masksToBounds = YES;
	_btnShowDirection.layer.cornerRadius = 5.0f;
	
	UIView *textFielfBackground = [[UIView alloc] init];
	textFielfBackground.backgroundColor = [UIColor whiteColor];
	
	_cellHeight = (IS_IPHONE_4)? 30.0f : 40.0f;
	
	[self.view addSubview:_mapView];
	[self.view addSubview:_tbvMain];
	[_mapView addSubview:_imviCenterMarker];
	[self.view addSubview:_viWindowMarker];
	[self.view addSubview:_txtFiDestination];
	[self.view addSubview:_txtFiPickupLocation];
	[self.view addSubview:_tbvShowingPlaces];
	[self.view addSubview:_btnShowDirection];
	
	
	// Create constraints
	NSDictionary *views = @{
						   @"txtFiPickupLocation":_txtFiPickupLocation,
						   @"txtFiDestination":_txtFiDestination,
						   @"mapView":_mapView,
						   @"imviCenterMarker":_imviCenterMarker,
						   @"tbvInfo":_tbvMain,
						   @"tbvShowingPlaces":_tbvShowingPlaces,
						   @"btnShowDirection":_btnShowDirection,
						   @"bottomLayoutGuide":self.bottomLayoutGuide,
						   @"topLayoutGuide":self.topLayoutGuide,
						   };
	
    float tbvHeight = _cellHeight * 4;
    
    UIEdgeInsets mapInsets = UIEdgeInsetsMake(tbvHeight, 0.0, tbvHeight, 0);
    _mapView.padding = mapInsets;
    
	NSDictionary *metrics = @{
							  @"txtFiHeight":@(_cellHeight),
							  @"tbvHeight":@(tbvHeight)
							  };
	
	NSArray *mapViewCons_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[mapView]-0-[bottomLayoutGuide]" options:0 metrics:metrics views:views];
	NSArray *tbvInfoCons_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[tbvInfo]-0-|" options:0 metrics:metrics views:views];
	[self.view addConstraints:mapViewCons_V];
	[self.view addConstraints:tbvInfoCons_H];
	
	
	NSArray *tbvMainCons_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[tbvInfo(tbvHeight)]-0-[bottomLayoutGuide]" options:0 metrics:metrics views:views];
	NSArray *mapViewCons_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[mapView]-0-|" options:0 metrics:metrics views:views];
	[self.view addConstraints:tbvMainCons_V];
	[self.view addConstraints:mapViewCons_H];

	NSArray *txtFiPickupLocationCons_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[topLayoutGuide]-10-[txtFiPickupLocation(txtFiHeight)]" options:0 metrics:metrics views:views];
	NSArray *txtFiPickupLocationCons_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[txtFiPickupLocation]-20-|" options:0 metrics:metrics views:views];
	[self.view addConstraints:txtFiPickupLocationCons_H];
	[self.view addConstraints:txtFiPickupLocationCons_V];
	
	
	NSArray *txtFiDestinationCons_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[txtFiPickupLocation]-10-[txtFiDestination(txtFiHeight)]" options:0 metrics:metrics views:views];
	NSArray *txtFiDestinationCons_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[txtFiDestination]-20-|" options:0 metrics:metrics views:views];
	[self.view addConstraints:txtFiDestinationCons_V];
	[self.view addConstraints:txtFiDestinationCons_H];

	
	NSArray *btnShowDirectionCons_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[txtFiDestination]-10-[btnShowDirection(40)]" options:0 metrics:metrics views:views];
	NSArray *btnShowDirectionCons_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[btnShowDirection(40)]-20-|" options:0 metrics:metrics views:views];
	[self.view addConstraints:btnShowDirectionCons_V];
	[self.view addConstraints:btnShowDirectionCons_H];

	
	NSLayoutConstraint *imviCenterMaker_CenterX = [NSLayoutConstraint constraintWithItem:_imviCenterMarker attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:_mapView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0];
	NSLayoutConstraint *imviCenterMaker_Y = [NSLayoutConstraint constraintWithItem:_imviCenterMarker attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:_mapView attribute:NSLayoutAttributeCenterY multiplier:1 constant:-25];
	NSLayoutConstraint *imviCenterMaker_Width = [NSLayoutConstraint constraintWithItem:_imviCenterMarker attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:40];
	NSLayoutConstraint *imviCenterMaker_Height = [NSLayoutConstraint constraintWithItem:_imviCenterMarker attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:50];
	[_mapView addConstraint:imviCenterMaker_CenterX];
	[_mapView addConstraint:imviCenterMaker_Y];
	[_mapView addConstraint:imviCenterMaker_Width];
	[_mapView addConstraint:imviCenterMaker_Height];
	
	// Moving _mapView's myLocation position
	UIButton* myLocationButton = (UIButton*)[[_mapView subviews] lastObject];
	myLocationButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
	CGRect frame = myLocationButton.frame;
	frame.origin.y = ScreenHeight - _cellHeight*4 - 50.0f;
	myLocationButton.frame = frame;
	
	self.automaticallyAdjustsScrollViewInsets = NO;
	
	// Enable click button in table view
	for (UIView *currentView in _tbvMain.subviews)
	{
		if([currentView isKindOfClass:[UIScrollView class]])
		{
			((UIScrollView *)currentView).delaysContentTouches = NO;
							break;
		}
	}
	self.view.backgroundColor = [UIColor whiteColor];
	
	_driverName = [[NSUserDefaults standardUserDefaults] stringForKey:@"DriverNamePrevious"];
	_driverPhoneNumber = [[NSUserDefaults standardUserDefaults] stringForKey:@"DriverPhoneNumberPrevious"];
	_timer = [[NSUserDefaults standardUserDefaults] stringForKey:@"PreviousTime"];

	_txtFiSelected = _txtFiPickupLocation;
	_favouritePlace = nil;
	
	_mapViewStatus = CallDriveFunction;
	
	_arrPlaces = [FavouritePlace loadListFavouritePlaces];
	_arrSuggestionPlaces = [[NSMutableArray alloc] init];
	_showingPreviousPlaces = YES;
}

- (void)dealloc {
	// If self still register KVO event, let remove it
	@try{
		[_mapView removeObserver:self forKeyPath:@"myLocation"];
	}@catch(id anException){
		//do nothing
		NSLog(@"Exception: %@", anException);
	}
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
	if([keyPath isEqualToString:@"myLocation"]){
		CLLocation *myLocation = [object myLocation];
		
		GMSCameraUpdate *cameraUpdate = [GMSCameraUpdate setTarget:myLocation.coordinate zoom:12];
		[_mapView moveCamera:cameraUpdate];
		[_mapView removeObserver:self forKeyPath:@"myLocation"];
	}
}


#pragma mark - UITableViewDelegate & UITableViewDatasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	if(tableView.tag == 1){
		return 1;
	}else if(tableView.tag == 2){
		return 1;
	}
	
	return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	
	if(tableView.tag == 1){
		return 4;
	}else if(tableView.tag == 2){
		if(_showingPreviousPlaces){
			return _arrPlaces.count;
		}else{
			return _arrSuggestionPlaces.count;
		}
		return _arrPlaces.count;
	}
	return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	if(tableView.tag == 1){
		if(indexPath.row == 3){
			// Cell for btnSendRequest
			return 60.0f;
		}
		return _cellHeight;
	}else if(tableView.tag == 2){
		return _cellHeight;
	}
		return 0;

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
	if(tableView.tag == 1){
		return 0;
	}else if(tableView.tag == 2){
		if(section == 0){
			return 0.0f;
		}else{
			return 5.0f;
		}
	}
	return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
	
	return 0.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

	if(tableView.tag == 1){
		UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellIdent"];
				if (!cell) {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"CellIdent"];
		}
		
		switch (indexPath.row) {
			case 0:
			{
				cell.textLabel.text = @"Timer";
				cell.detailTextLabel.text = _timer;
			}
				break;
				
			case 1:
			{
				cell.textLabel.text = @"Driver";
				cell.detailTextLabel.text = _driverName;
			}
				break;
			case 2:
			{
				cell.textLabel.text = @"Phone number";
				cell.detailTextLabel.text = _driverPhoneNumber;
			}
				break;
			case 3:
			{
				CGFloat tbnWidth = self.view.frame.size.width - 2*50.0f;
				CGFloat tbnHeight = _cellHeight - 2*5.0f;
				
				UIButton *tbnSend = [[UIButton alloc] initWithFrame:CGRectMake(50.0f, 5.0f, tbnWidth, tbnHeight)];
				[tbnSend setTitle:@"Send" forState:UIControlStateNormal];
				[tbnSend setBackgroundImage:[Constants imageWithColor:kColorPink] forState:UIControlStateNormal];
				[tbnSend addTarget:self action:@selector(sendRequest:) forControlEvents:UIControlEventTouchUpInside];
				tbnSend.layer.cornerRadius = 10.0f;
				tbnSend.layer.masksToBounds = YES;
				
				[cell.contentView addSubview:tbnSend];
			}
				break;
			default:
				break;
		}
		return cell;
		
	}else if(tableView.tag == 2){
		
		UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"PlaceCell"];
		FavouritePlace *place;

		if(_showingPreviousPlaces == YES){
			place = _arrPlaces[indexPath.row];

			cell.textLabel.text = place.strPlaceTitle;
			cell.detailTextLabel.text = place.strPlaceName;
			NSString *iconName;
			if(place.placeType == FavouritePlaceHome){
				iconName = @"homeicon";
			}else if(place.placeType == FavouritePlaceWork){
				iconName = @"workicon";
			}else if(place.placeType == FavouritePlaceOthers){
				iconName = @"othersicon";
			}else{
				iconName = @"newesticon";
			}
			
			cell.imageView.image = [UIImage imageNamed:iconName];
		}else{
			place = _arrSuggestionPlaces[indexPath.row];
			NSRange rangeOfFirstComma = [place.strPlaceName rangeOfString:@","];
			// If comma was found, substring by comma
			if(rangeOfFirstComma.length > 0){
				cell.textLabel.text = [place.strPlaceName substringToIndex:rangeOfFirstComma.location];
				cell.detailTextLabel.text = [place.strPlaceName substringFromIndex:rangeOfFirstComma.location + 2];
			}else{
				cell.textLabel.text = place.strPlaceName;
			}
		}
		return cell;
	}
	return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
	
	UIView *header = [[UIView alloc] init];
	header.backgroundColor = [UIColor clearColor];
	
	return header;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	if(tableView.tag == 1){
		switch (indexPath.row) {
			case 0:
			{
				CountdownTimer *timer = [[CountdownTimer alloc] initWithFrame:self.view.frame];
				timer.delegate = self;
				[timer showTimer];
			}
				break;
			case 1:
			{
				_addressBookController = [[ABPeoplePickerNavigationController alloc] init];
				[_addressBookController setPeoplePickerDelegate:self];
				[self presentViewController:_addressBookController animated:YES completion:nil];
			}
				break;
			case 2:
			{
				_addressBookController = [[ABPeoplePickerNavigationController alloc] init];
				[_addressBookController setPeoplePickerDelegate:self];
				
				[self presentViewController:_addressBookController animated:YES completion:nil];
			}
				break;
			default:
				break;
		}
		
	}else if(tableView.tag == 2){
		[self hidePlacesTable];
//		FavouritePlace *favouritePlace;
		NSMutableArray *selectedArray;
		if(_showingPreviousPlaces){
			selectedArray = _arrPlaces;
		}else{
			selectedArray = _arrSuggestionPlaces;
		}
		_favouritePlace = (FavouritePlace *)selectedArray[indexPath.row];
		
		_txtFiDestination.text = _favouritePlace.strPlaceName;
		_destinationCoor = _favouritePlace.location.coordinate;
		_viWindowMarker.alpha = 0.0f;
	
		[self showDirection:_btnShowDirection]; // call _btnShowDirection action
	}
	
}


#pragma mark - CountdownTimerDelegate

- (void)selectedTimer:(NSString *)timer{
	_timer = timer;
	[[NSUserDefaults standardUserDefaults] setObject:timer forKey:@"PreviousTime"];

	UITableViewCell *cell = [_tbvMain cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
	cell.detailTextLabel.text = timer;
}

#pragma mark - GMSMapViewDelegate

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
	if(_mapViewStatus != ShowingSuggestionPlace){
		[ViewController cancelPreviousPerformRequestsWithTarget:self selector:@selector(getLocation) object:nil];
		[self performSelector:@selector(getLocation) withObject:nil afterDelay:1.0];
	}
}

- (void)getLocation{
	
	CLLocationCoordinate2D newCoordinate;
	
	newCoordinate = [_mapView.projection coordinateForPoint:CGPointMake(_imviCenterMarker.frame.size.width/2.0f + _imviCenterMarker.frame.origin.x, _imviCenterMarker.frame.size.height + _imviCenterMarker.frame.origin.y)];
	if([_txtFiSelected isEqual:_txtFiPickupLocation]){
		_pickUpCoordinate = newCoordinate;
	}else{
		_destinationCoor = newCoordinate;
	}
	NSString *urlString = [NSString stringWithFormat:@"%@geocode/json?latlng=%f,%f&location_type=GEOMETRIC_CENTER&language=vi&key=%@",baseURL, newCoordinate.latitude, newCoordinate.longitude, kGGMapServerKey];
	NSURL *url = [NSURL URLWithString:urlString];
	NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
		if (!error) {
			
			NSError *error1;
			NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error1];
			NSString *status = [result objectForKey:@"status"];
			if (!error1) {
				
				// Print adress to Pin
				if([status isEqualToString:@"OK"]){
					// get string result from json
					NSArray *resultArr = [result valueForKey:@"results"];
					NSDictionary *resultDic = [resultArr firstObject];
					
					NSString *formatted_address = [resultDic objectForKey:@"formatted_address"];
					NSArray *addressArr = [formatted_address componentsSeparatedByString:@","];
					dispatch_async(dispatch_get_main_queue(), ^{
						
						// Get address from string array
						if(addressArr.count > 2){
							_txtFiSelected.text = [NSString stringWithFormat:@"%@, %@, %@", (NSString *)addressArr[0], (NSString *)addressArr[1], (NSString *)addressArr[2]];
						}else if(addressArr.count == 2){
							_txtFiSelected.text = [NSString stringWithFormat:@"%@, %@", (NSString *)addressArr[0], (NSString *)addressArr[1]];
						}else if(addressArr.count == 1){
							_txtFiSelected.text = [NSString stringWithFormat:@"%@@", (NSString *)addressArr[0]];
						}else{
							_txtFiSelected.text = [NSString stringWithFormat:@"Not found address"];
						}
						
						if(_mapViewStatus == CallDriveFunction){
							[self showTableInfo];
							
						}else if (_mapViewStatus == RouteFunction){
							_favouritePlace = [[FavouritePlace alloc] init];
							_favouritePlace.strPlaceName = _txtFiSelected.text;
							_favouritePlace.location = [[CLLocation alloc] initWithLatitude:newCoordinate.latitude longitude:newCoordinate.longitude];
							HideNetworkActivityIndicator();
							// Just show window marker when txtFiDestination is selected
							if([_txtFiSelected isEqual:_txtFiDestination]){
								[UIView animateWithDuration:0.5f animations:^{
									_viWindowMarker.alpha = 1.0f;
								}];
							}
							if((![_txtFiDestination.text isEqualToString:@""]) && (![_txtFiPickupLocation.text isEqualToString:@""])){
								[UIView animateWithDuration:0.5f animations:^{
									_btnShowDirection.alpha = 1.0f;
								}];
							}
						}
					});
					
				}else{
					dispatch_async(dispatch_get_main_queue(), ^{
						UIAlertView *notFoundAlert = [[UIAlertView alloc]
													  initWithTitle:@"Not Found"
													  message:@"Location is not available"
													  delegate:nil
													  cancelButtonTitle:nil
													  otherButtonTitles:@"Ok", nil];
						[notFoundAlert show];
					});
				}
			}
		}else{
			NSLog(@"Error : %@",error);
		}
	}];
	
	[task resume];
}

- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture{

	[UIView animateWithDuration:0.5f animations:^{
		_viWindowMarker.alpha = 0.0f;
		_btnShowDirection.alpha = 0.0f;
		_tbvShowingPlaces.alpha = 0.0f;
	}];
	
	if(_mapViewStatus != ShowingSuggestionPlace	&& gesture == YES){
		_txtFiSelected.text = @"";
		
		// Play activity indicator
	}
	[self.view endEditing:YES];
	if(gesture){ // Just perform this method when user move map, not move camera
		[self hideTableInfo];
	}
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
	[self.view endEditing:YES];
	
	// When user tap to map view, just hide _viWindowMarker if _tbvShowingPlaces is hidden
	if(_tbvShowingPlaces.alpha == 0.0f){
		[UIView animateWithDuration:0.5f animations:^{
			_viWindowMarker.alpha = 0.0f;
		}];
	}else{
		// Hide window marker when tap on map
		if(_viWindowMarker.alpha == 1.0f){
			_imviCenterMarker.alpha = 0.0f;
			_viWindowMarker.alpha = 0.0f;
		}
		[self hidePlacesTable];
	}
}


#pragma mark - Show and hide table

- (void)showTableInfo{
	[UIView animateWithDuration:1.0f animations:^{
		_tbvMain.alpha = 1.0f;
	}];
}

- (void)hideTableInfo{
	[UIView animateWithDuration:0.5f animations:^{
		_tbvMain.alpha = 0.0f;
	}];
}

- (void)hidePlacesTable{
	[UIView animateWithDuration:0.5f animations:^{
		_tbvShowingPlaces.alpha = 0.0f;
	} completion:^(BOOL finished) {
		[_tbvShowingPlaces removeFromSuperview];
	}];
}

- (void)showPlacesTable{
	// Show place table when it had any element
	NSMutableArray *selectedArray;
	if(_showingPreviousPlaces){
		selectedArray = _arrPlaces;
	}else{
		selectedArray = _arrSuggestionPlaces;
	}
	if(selectedArray.count == 0){
		[_tbvShowingPlaces removeFromSuperview];
	}else{
		CGFloat newHeight = _cellHeight*selectedArray.count;
		CGFloat newWidth = ScreenWidth - 40;
		_tbvShowingPlaces.frame = CGRectMake(20, _txtFiDestination.frame.origin.y + 50, newWidth, newHeight);
		[self.view addSubview:_tbvShowingPlaces];
		
		[UIView animateWithDuration:0.5f animations:^{
			_tbvShowingPlaces.alpha = 1.0f;
		} completion:^(BOOL finished) {
			[_tbvShowingPlaces reloadData];
		}];
		_viWindowMarker.alpha = 0.0f;
	}
}


#pragma mark - Show and hide textfield and window marker

- (void)showWindowMarkerAndDirectionButton{
	
	[UIView animateWithDuration:0.5f animations:^{
		_btnShowDirection.alpha = 1.0f;
		_viWindowMarker.alpha = 1.0f;
	}];
}

- (void)hideWindowMarkerAndDirectionButton{
	
	[UIView animateWithDuration:0.5f animations:^{
		_btnShowDirection.alpha = 0.0f;
		_viWindowMarker.alpha = 0.0f;
	}];
}

- (void)hideTxtFiDestination{
	[UIView animateWithDuration:0.5f animations:^{
		_txtFiDestination.alpha = 0.0f;
	}];
}

- (void)showTxtFiDestination{
	[UIView animateWithDuration:0.5f animations:^{
		_txtFiDestination.alpha = 1.0f;
	}];
}

- (void)showCenterMarker{
	[UIView animateWithDuration:0.5f animations:^{
		_imviCenterMarker.alpha = 1.0f;
	}];
}

- (void)hideCenterMarker{
	[UIView animateWithDuration:0.5f animations:^{
		_imviCenterMarker.alpha = 0.0f;
	}];
}


#pragma mark - DestinationWindowMarkerDelegate

- (void)markNewPlace{
	
	_favouritePlace.placeType = FavouritePlaceHome;
	// Show direction
	[UIView animateWithDuration:0.5f animations:^{
		_viWindowMarker.alpha = 0.0f;
	}];

	EditDetailPlaceVC *editVC = [[EditDetailPlaceVC alloc] init];
	editVC.place = _favouritePlace;
	editVC.editingMode = CreateNewPlace;
	
	[self.navigationController pushViewController:editVC animated:YES];
}

#pragma mark - UIButton action

- (IBAction)clickRightBarBtn:(id)sender{
	NSLog(@"How setting view");
}

- (IBAction)clickLeftBarBtn:(id)sender{
	EditFavouritePlaceViewController *editFavouriteVC = [[EditFavouritePlaceViewController alloc] init];
	[self.navigationController pushViewController:editFavouriteVC animated:YES];
}

- (IBAction)btnBackPressed:(id)sender{
	[self.view endEditing:YES];
	
	_mapViewStatus = RouteFunction;
	_txtFiDestination.leftView = nil;
	_destinationMarker.map = nil;
	_polyline.map = nil;
	_txtFiDestination.text = @"";
	_viWindowMarker.alpha = 0.0f;
	_btnShowDirection.alpha = 0.0f;
	
	[self hidePlacesTable];
	[self showCenterMarker];
	
}

- (IBAction)showDirection:(id)sender{
	
	if ([_txtFiPickupLocation.text isEqualToString:@""] == NO) {
		_mapViewStatus = ShowingSuggestionPlace;
		[_mapView clear];
		
		_pickupMarker = [GMSMarker markerWithPosition:_pickUpCoordinate];
		_pickupMarker.icon = [UIImage imageNamed:@"startPin"];
		_pickupMarker.map = _mapView;
		
		_destinationMarker = [GMSMarker markerWithPosition:_destinationCoor];
		_destinationMarker.icon = [UIImage imageNamed:@"endPin"];
		_destinationMarker.map = _mapView;
		
		_imviCenterMarker.alpha = 0.0f;
		
		[self getDirectionFromLocation:_pickUpCoordinate to:_destinationCoor];
	}else{
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Can find the direction"
															message:@"Please select pickup location first"
														   delegate:nil
												  cancelButtonTitle:nil
												  otherButtonTitles:@"OK", nil];
		[alertView show];
	}
}

- (IBAction)sendRequest:(id)sender{
	
	// Not selected driver
	if([[NSUserDefaults standardUserDefaults] objectForKey:@"DriverNamePrevious"] == nil){
		[self dispatchAlertViewWithTitle:@"Can't call driver"
								 message:@"Please choose one driver"
					   cancelButtonTitle:nil
					 andOtherButtonTitle:@"Ok"];
		return;
	}
	
	// Not setup timer
	if(_timer == nil){
		[self dispatchAlertViewWithTitle:@"Can't call driver"
								 message:@"Please setup timer"
					   cancelButtonTitle:nil
					 andOtherButtonTitle:@"Ok"];
		return;
	}
	// Not pickup location yet 
	if(_txtFiPickupLocation.text == nil){
		[self dispatchAlertViewWithTitle:@"Can't call driver"
								 message:@"Waiting for location"
					   cancelButtonTitle:nil
					 andOtherButtonTitle:@"Ok"];
		return;
	}
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	dateFormatter.dateFormat = @"hh:mm EEE dd/MM/YY";
//
//	[[NSUserDefaults standardUserDefaults] setObject:@"Mr.X" forKey:@"UserPhone"];
//	NSString *userPhone = [[NSUserDefaults standardUserDefaults] stringForKey:@"UserPhone"];
//	NSString *body = [NSString stringWithFormat:@"userphone=%@&driverphone=%@&timereturn=%@&timego=%@&longtitude=%f&latitude=%f", userPhone, _driverPhoneNumber, @"0:20", [dateFormatter stringFromDate:[NSDate date]],_pickUpCoordinate.longitude, _pickUpCoordinate.latitude];
//	body = [body stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://matiloca.esy.es/CallingDriverService.php?%@", body]];
//	NSURLSessionDataTask *sendRequest = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//		if (!error) {
//			NSError *error1;
//			NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error1];
//			
//			if (!error1) {
//				NSString *strResult = [result valueForKey:@"status"];
//				
//				if([strResult isEqualToString:@"1"]){
//					[self dispatchAlertViewWithTitle:@"Successful"
//											 message:@"Request have been sent"
//								   cancelButtonTitle:nil
//								 andOtherButtonTitle:@"Ok"];
//				}else{
//					// strResult == 0
//					[self dispatchAlertViewWithTitle:@"Can't call driver"
//											 message:@"Not found driver"
//								   cancelButtonTitle:nil
//								 andOtherButtonTitle:@"Ok"];
//				}
//			}else{
//				// Serialization Fail
//				NSLog(@"Serialization Error: %@",error1);
//				[self dispatchAlertViewWithTitle:@"Can't call driver"
//										 message:@"Something wrong with server"
//							   cancelButtonTitle:nil
//							 andOtherButtonTitle:@"Ok"];
//			}
//		}else{
//			NSLog(@"Error : %@",error);
//		}
//	}];
//	
//	[sendRequest resume];
//	_favouritePlace = [[FavouritePlace alloc] init];
//	_favouritePlace.strPlaceName = _txtFiPickupLocation.text;
//	_favouritePlace.strPlaceTitle = @"Newest location";
//	_favouritePlace.location = [[CLLocation alloc] initWithLatitude:_pickUpCoordinate.latitude longitude:_pickUpCoordinate.longitude];
//	_favouritePlace.placeType = FavouritePlaceNewest;
//	[FavouritePlace addNewFavouritePlaces:_favouritePlace];
    
    MBProgressHUD * HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.label.text = @"Loading...";
    
    __weak ViewController *weakSelf = self;
    PFObject *callingInformation = [PFObject objectWithClassName:@"callingdriver"];
    
    [callingInformation setObject:self.timer forKey:@"timerequest"];
    [callingInformation setObject:[dateFormatter stringFromDate:[NSDate date]] forKey:@"timecalling"];
    NSString *strLongtitude = [NSString stringWithFormat:@"%f",_pickUpCoordinate.longitude];
    [callingInformation setObject:strLongtitude forKey:@"longtitude"];
    NSString *strLatitude = [NSString stringWithFormat:@"%f",_pickUpCoordinate.latitude];
    [callingInformation setObject:strLatitude forKey:@"latitude"];
    [callingInformation setObject:@"0" forKey:@"status"];
    
    [callingInformation setObject:self.driver forKey:@"driver"];
    [callingInformation setObject:[PFUser currentUser] forKey:@"ceo"];
    
    [callingInformation saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
        });
        if (!error) {
            if (succeeded) {
                
                [weakSelf sendPushNotificationToDriver];
            }else{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Message Send Fail" delegate:nil cancelButtonTitle:@"Oke" otherButtonTitles: nil];
                [alertView show];
            }
        }else{
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Connection Fail!" delegate:nil cancelButtonTitle:@"Oke" otherButtonTitles: nil];
            [alertView show];
        }
    }];
    
	ShowNetworkActivityIndicator();
	//[self performSelector:@selector(checkStatus) withObject:nil afterDelay:10.0];
}

- (void)sendPushNotificationToDriver{
    MBProgressHUD * HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.label.text = @"Loading...";
    
    __weak ViewController *weakSelf = self;
    
    NSString *message = [NSString stringWithFormat:@"\"message\":\"New request from CEO : Pick me up at %@.\",\"data\":{\"mode\":\"0\",\"documentID\":\"0\",\"receiverID\":\"01026144688_1410495618967\",\"senderID\":\"(null)\"",self.txtFiPickupLocation.text];

    NSString *pushURL = [NSString stringWithFormat:@"http://cleanbasketap.azurewebsites.net/v1/cb/User/snsPushSend?key=6c8ecba5a4621419df0f58598c4b90f7&target_arn=%@&message={%@}}",self.driverTargetAddress,message];
    NSURL *url = [NSURL URLWithString:[pushURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
        });
        if (!error) {
            NSError *error1;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error1];
            if (!error1) {
                NSString *strResult = [result valueForKey:@"result"];
                if ([strResult isEqualToString:@"true"]) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Message Send Success!" delegate:nil cancelButtonTitle:@"Oke" otherButtonTitles: nil];
                        [alertView show];
                    });
                }else{
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Fail" message:@"Message Send Fail!" delegate:nil cancelButtonTitle:@"Oke" otherButtonTitles: nil];
                    [alertView show];
                }

            }
        }else{
            NSLog(@"Error : %@",error.description);
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Oke" otherButtonTitles: nil];
            [alertView show];
        }
    }] resume];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
	_txtFiSelected = textField;
	[self.view endEditing:YES];
	
	// When routine is show, if user tap in any text field _mapViewStatus will change to Route Fuction
	if(_mapViewStatus == ShowingSuggestionPlace){
		_mapViewStatus = RouteFunction;
	}
	
	// Hide map poline
	_polyline.map = nil;
	_imviCenterMarker.alpha = 1.0f;
	if([textField isEqual:_txtFiPickupLocation]){
		[self hidePlacesTable];
		[self hideWindowMarkerAndDirectionButton];
		[self showPickupTextFieldBorderColor];
		
		_pickupMarker.map = nil;
		if(![_txtFiDestination.text isEqualToString:@""]){
			_destinationMarker.map = _mapView;
		}
		
		// User not allowed enter pickup location
		return NO;
		
	}else if([textField isEqual:_txtFiDestination]){
		
		if([[textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]){
			_showingPreviousPlaces = YES;
			_arrPlaces = [FavouritePlace loadListFavouritePlaces];
			[_tbvShowingPlaces reloadData];
		}else{
			_showingPreviousPlaces = NO;
		}
		
		[self showTxtFiDestination];
		[self showDestinationTextFieldBorderColor];
		[self showPlacesTable];
		
		_destinationMarker.map = nil;
		if(![_txtFiPickupLocation.text isEqualToString:@""]){
			_pickupMarker.map = _mapView;
		}
		
		// Show back button in textfield
		UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
		[btnBack addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
		btnBack.frame = CGRectMake(0, 0, textField.frame.size.height, textField.frame.size.height);
		[btnBack setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
		textField.leftViewMode = UITextFieldViewModeAlways;
		textField.leftView = btnBack;
		return YES;
	}
	return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
	//	NSLog(@"%@", textField.text);
	
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
	[self.view endEditing:YES];
	_showingPreviousPlaces = YES;
	_arrPlaces = [FavouritePlace loadListFavouritePlaces];
	[_tbvShowingPlaces reloadData];
	// Hide tbvShowingPlace, select destination by moving map view
	[UIView animateWithDuration:0.5f animations:^{
		_imviCenterMarker.alpha = 1.0f;
	}];

//	[self hideTableInfo];
	_polyline.map = nil;
	_mapViewStatus = RouteFunction;
	return YES;
}

//
// 	Get list of predicted place names or address
//
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(nonnull NSString *)string{
	_mapViewStatus = ShowingSuggestionPlace;
	NSString *newText;
	if (string.length == 0) {
		// handle Delete (also handles the Cut menu)
		NSString *txtFiText = [textField.text stringByTrimmingCharactersInSet:[NSMutableCharacterSet whitespaceAndNewlineCharacterSet]];
		newText = [txtFiText substringToIndex:txtFiText.length - 1];
	} else {
		// some other key or text is being pasted.
		newText = [textField.text stringByAppendingString:string];
		newText = [newText stringByTrimmingCharactersInSet:[NSMutableCharacterSet whitespaceAndNewlineCharacterSet]];
	}
	if([newText isEqualToString:@""]){
		_showingPreviousPlaces = YES;
		_arrPlaces = [FavouritePlace loadListFavouritePlaces];
		[_tbvShowingPlaces reloadData];
		[ViewController cancelPreviousPerformRequestsWithTarget:self selector:@selector(getLocationIDFormText:) object:textField.text];
		return YES;
	}else{
		_showingPreviousPlaces = NO;
	}
	[ViewController cancelPreviousPerformRequestsWithTarget:self selector:@selector(getLocationIDFormText:) object:textField.text];
	[self performSelector:@selector(getLocationIDFormText:) withObject:newText afterDelay:0.8];
	
	return YES;
}


#pragma mark - Get suggestion address with GMS API

// Get location ID from text input
- (void)getLocationIDFormText:(NSString *)textInput{
	
	// Testing
//	dispatch_async(dispatch_get_main_queue(), ^{
//		[_arrPlaces removeAllObjects];
//		for(int i = 0; i < (arc4random_uniform(4) + 1); i++){
//			FavouritePlace *newPlace = [[FavouritePlace alloc] init];
//			newPlace.strPlaceName = textInput;
//			newPlace.location = [[CLLocation alloc] initWithLatitude:0.0f longitude:0.0f];
//			[_arrPlaces addObject:newPlace];
//		}
//		// Repostion tbv base on its cell number
//		CGFloat newHeight = _cellHeight*_arrPlaces.count;
//		CGFloat newWidth = ScreenWidth - 40;
//		_tbvShowingPlaces.frame = CGRectMake(20, _txtFiDestination.frame.origin.y + 50, newWidth, newHeight);
//		[_tbvShowingPlaces reloadData];
//	});
	
	[[GMSPlacesClient sharedClient] autocompleteQuery:textInput
										   bounds:nil
										   filter:_autocompleteFilterGMS
										 callback:^(NSArray<GMSAutocompletePrediction *> * _Nullable results, NSError * _Nullable error) {
											 if(error != nil){
											 }else{
												 [self getPlacesFromAutocompletePrediction:results];
											 }
										 }];
	ShowNetworkActivityIndicator();
}

// Get address from location ID
- (void)getPlacesFromAutocompletePrediction:(NSArray *)predictionArr{
	
	__block NSMutableArray *arrNewPlaces = [[NSMutableArray alloc] init];
	//  Get address by using place ID
	for (GMSAutocompletePrediction* prediction in predictionArr) {
		
		[[GMSPlacesClient sharedClient] lookUpPlaceID:prediction.placeID callback:^(GMSPlace *place, NSError *error) {
			if (error != nil) {
				// Don't show alert here
			}else if(place != nil){
				FavouritePlace *newPlace = [[FavouritePlace alloc] init];
				newPlace.strPlaceName = place.formattedAddress;
				newPlace.location = [[CLLocation alloc] initWithLatitude:place.coordinate.latitude longitude:place.coordinate.longitude];
				
				if(newPlace.strPlaceName != nil){
					[arrNewPlaces addObject:newPlace];
				}
				// Reload suggestion table with the last Predication
				if([prediction isEqual:[predictionArr lastObject]]){
					dispatch_async(dispatch_get_main_queue(), ^{
						_arrSuggestionPlaces = arrNewPlaces;
						CGFloat newHeight = _cellHeight*_arrSuggestionPlaces.count;
						CGFloat newWidth = ScreenWidth - 40;
						_tbvShowingPlaces.frame = CGRectMake(20, _txtFiDestination.frame.origin.y + 50, newWidth, newHeight);
						_tbvShowingPlaces.alpha = 1.0f;
						[self.view addSubview:_tbvShowingPlaces];
						[_tbvShowingPlaces reloadData];
						HideNetworkActivityIndicator();
					});
				}
			}
		}]; // GMSPlacesClient method
	} // for(;;) statement
}

- (void)dispatchAlertViewWithTitle:(NSString *)title
						   message:(NSString *)message
				 cancelButtonTitle:(NSString *)cancelTitle
			   andOtherButtonTitle:(NSString *)ortherTitle{
		dispatch_async(dispatch_get_main_queue(), ^{
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
															message:message
														   delegate:nil
												  cancelButtonTitle:cancelTitle
												  otherButtonTitles:ortherTitle , nil];
		[alertView show];
	});
}


- (void)showPickupTextFieldBorderColor{
	_txtFiPickupLocation.layer.borderColor = [UIColor blueColor].CGColor;
	_txtFiPickupLocation.layer.borderWidth = 2.0f;
	
	// Hide txtFiDestination border
	_txtFiDestination.layer.borderColor = [UIColor clearColor].CGColor;
}

- (void)showDestinationTextFieldBorderColor{
	_txtFiDestination.layer.borderColor = [UIColor blueColor].CGColor;
	_txtFiDestination.layer.borderWidth = 2.0f;
	
	// Hide txtFiDestination border
	_txtFiPickupLocation.layer.borderColor = [UIColor clearColor].CGColor;
}

#pragma mark - Handle tap table previous places

- (void)tapTableHandle:(UITapGestureRecognizer *)recognizer{
	
	CGPoint tapLocation = [recognizer locationInView:_tbvShowingPlaces];
	NSIndexPath *indexPath = [_tbvShowingPlaces indexPathForRowAtPoint:tapLocation];
	
	if(indexPath == nil){
		recognizer.cancelsTouchesInView = NO;
	}else{
		// Selected row
		[self tableView:_tbvShowingPlaces didSelectRowAtIndexPath:indexPath];
	}
	[self.view endEditing:YES];
	[self hidePlacesTable];
}


#pragma mark - Test API Get check STATUS

- (void)checkStatus{

}

- (void)actionBaseOnStatus: (int)status{
    if (status == 0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Notice" message:@"The driver can not receive the message. Please choose others action." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Message",@"Calling", nil];
        [alertView show];
    }else{
        
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) { // Message
        [self sendMessageToDriver];
    }else if(buttonIndex == 2){ // Calling
        [self callCeoPhone];
    }
}

#pragma mark - Call to driver

- (void)callCeoPhone{
        NSURL *phoneURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",_driverPhoneNumber]];
        if ([[UIApplication sharedApplication] canOpenURL:phoneURL]) {
            [[UIApplication sharedApplication] openURL:phoneURL];
        }else
        {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
}

- (void)sendMessageToDriver{
    if (![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesnt support SMS" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSArray *recipents = @[_driverPhoneNumber];
    NSString *message = [NSString stringWithFormat:@"My position now is in : %@ . Please pick up me in %@ minutes",_txtFiPickupLocation.text,_timer];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:recipents];
    [messageController setBody:message];
    
    //
    [self presentViewController:messageController animated:YES completion:^{
        
    }];
}

#pragma mark - MessageCompose Delegate

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    switch (result) {
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Get direction
- (void)getDirectionFromLocation:(CLLocationCoordinate2D)startLocation to:(CLLocationCoordinate2D)stopLocation{
	NSString *strRequest = [NSString stringWithFormat:@"%@directions/json?key=%@&language=vi&origin=%f,%f&destination=%f,%f",baseURL,kGGMapServerKey,startLocation.latitude,startLocation.longitude,stopLocation.latitude,stopLocation.longitude];
	NSURLSession *session = [NSURLSession sharedSession];
	
	__weak ViewController *weakSelf = self;
	[[session dataTaskWithURL:[NSURL URLWithString:strRequest] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
		if (!error) {
			NSError *parseError;
			NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
			if (!parseError) {
				NSDictionary *routes = [[result objectForKey:@"routes"] firstObject];
				NSDictionary *route = [routes objectForKey:@"overview_polyline"];
				if (route == nil) {
					
					dispatch_async(dispatch_get_main_queue(), ^{
						[weakSelf showAlertCantGetDirection];
					});
					return;
				}
				
				dispatch_async(dispatch_get_main_queue(), ^{
					NSString *overview_route = [route objectForKey:@"points"];
					GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
					GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
					[weakSelf drawPolylineOnMap:polyline];
				});
			}
		}else{
			[weakSelf showAlertErrorConnection];
		}
	}] resume];
}

#pragma mark - Draw polyline on Map
- (void)drawPolylineOnMap: (GMSPolyline *)newPolyline{
	if (newPolyline != nil) {
		// Delete old polyline
		self.polyline.map = nil;
		// Update region
		GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:[newPolyline.path coordinateAtIndex:0]  coordinate:[newPolyline.path coordinateAtIndex:newPolyline.path.count-1]];
		GMSCameraUpdate *boundCamera = [GMSCameraUpdate fitBounds:bounds withPadding:50.0];
		[self.mapView animateWithCameraUpdate:boundCamera];
		// Draw new direction
		self.polyline = newPolyline;
		self.polyline.map = self.mapView;
		self.polyline.strokeWidth = 3;
	}else{
		[self showAlertErrorConnection];
	}
}

#pragma mark - Show alert

- (void)showAlert: (NSString *)text{
	AlertLabel *alert = [AlertLabel initAlertWithContent:text];
	[alert setTranslatesAutoresizingMaskIntoConstraints:NO];
	[self.view addSubview:alert];
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:alert
														  attribute:NSLayoutAttributeCenterX
														  relatedBy:NSLayoutRelationEqual
															 toItem:self.mapView
														  attribute:NSLayoutAttributeCenterX
														 multiplier:1.0
														   constant:0.0]];
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.mapView
														  attribute:NSLayoutAttributeBottom
														  relatedBy:NSLayoutRelationEqual
															 toItem:alert
														  attribute:NSLayoutAttributeBottom
														 multiplier:1.0
														   constant:40.0]];
	[alert addConstraint:[NSLayoutConstraint constraintWithItem:alert
													  attribute:NSLayoutAttributeHeight
													  relatedBy:NSLayoutRelationEqual
														 toItem:nil
													  attribute:NSLayoutAttributeNotAnAttribute
													 multiplier:1.0
													   constant:21]];
	[alert dismissAlertLabel];
}

- (void)showAlertErrorConnection{
	[self showAlert:@"Network connection error."];
}

- (void)showAlertCantGetLocation{
	[self showAlert:@"Can not locate your location. Please open Privacy in Setting"];
}

- (void)showAlertCantGetDirection{
	[self showAlert:@"Can not get direction."];
}

#pragma mark - ABPeoplePickerNavigationControllerDelegate

-(BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person{
	// IOS 7, deprecated in ios 8
	//	NSLog(@"IOS 7");
	
	// Initialize a mutable dictionary and give it initial values.
	NSMutableDictionary *contactInfoDict = [[NSMutableDictionary alloc]
											initWithObjects:@[@"", @"", @"", @"", @"", @"", @"", @"", @""]
											forKeys:@[@"firstName", @"lastName", @"mobileNumber", @"homeNumber", @"homeEmail", @"workEmail", @"address", @"zipCode", @"city"]];
	
	// Use a general Core Foundation object.
	CFTypeRef generalCFObject = ABRecordCopyValue(person, kABPersonFirstNameProperty);
	
	// Get the first name.
	if (generalCFObject) {
		[contactInfoDict setObject:(__bridge NSString *)generalCFObject forKey:@"firstName"];
		CFRelease(generalCFObject);
	}
	
	// Get the last name.
	generalCFObject = ABRecordCopyValue(person, kABPersonLastNameProperty);
	if (generalCFObject) {
		[contactInfoDict setObject:(__bridge NSString *)generalCFObject forKey:@"lastName"];
		CFRelease(generalCFObject);
	}
	
	// Get the phone numbers as a multi-value property.
	ABMultiValueRef phonesRef = ABRecordCopyValue(person, kABPersonPhoneProperty);
	for (int i=0; i<ABMultiValueGetCount(phonesRef); i++) {
		CFStringRef currentPhoneLabel = ABMultiValueCopyLabelAtIndex(phonesRef, i);
		CFStringRef currentPhoneValue = ABMultiValueCopyValueAtIndex(phonesRef, i);
		
		if(currentPhoneLabel){
			if (CFStringCompare(currentPhoneLabel, kABPersonPhoneMobileLabel, 0) == kCFCompareEqualTo) {
				[contactInfoDict setObject:(__bridge NSString *)currentPhoneValue forKey:@"mobileNumber"];
			}
			
			if (CFStringCompare(currentPhoneLabel, kABHomeLabel, 0) == kCFCompareEqualTo) {
				[contactInfoDict setObject:(__bridge NSString *)currentPhoneValue forKey:@"homeNumber"];
			}
		}
		
		CFRelease(currentPhoneLabel);
		CFRelease(currentPhoneValue);
	}
	CFRelease(phonesRef);
	
	NSString *phoneNumber = (NSString *)[contactInfoDict objectForKey:@"mobileNumber"];
	NSString *firstName = (NSString *)[contactInfoDict objectForKey:@"firstName"];
	NSString *lastName = (NSString *)[contactInfoDict objectForKey:@"lastName"];
	
	if([phoneNumber isEqualToString:@""]){
		UIAlertView *phoneNumberNotFound = [[UIAlertView alloc] initWithTitle:@"Mobile phone number not found" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
		[phoneNumberNotFound show];
		
		_driverPhoneNumber = nil;
		_driverName = nil;
	}else{
		_driverName = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
		_driverPhoneNumber = [NSString stringWithFormat:@"%@", phoneNumber];
		
		[[NSUserDefaults standardUserDefaults] setObject:_driverName forKey:@"DriverNamePrevious"];
		[[NSUserDefaults standardUserDefaults] setObject:_driverPhoneNumber	forKey:@"DriverPhoneNumberPrevious"];
		
		[_tbvMain reloadData];
	}
	
	
	// Dismiss the address book view controller.
	[_addressBookController dismissViewControllerAnimated:YES completion:nil];
	
	return NO;
}

-(BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier{
	// IOS 7, deprecated in ios 8
	//	NSLog(@"IOS 7");
	return NO;
}

// This method called in IOS 8, and then it will call
- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
	NSLog(@"IOS 8");
	[self peoplePickerNavigationController:peoplePicker shouldContinueAfterSelectingPerson:person property:property identifier:identifier];
}

- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker
						 didSelectPerson:(ABRecordRef)person{
	NSLog(@"IOS 8");
	[self peoplePickerNavigationController:peoplePicker shouldContinueAfterSelectingPerson:person]; // IOS 7 method
	
}

-(void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker{
	[_addressBookController dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - UISegmentControl

- (IBAction)changeSegment:(id)sender{
	
	if(((UISegmentedControl *)sender).selectedSegmentIndex == 0){ // Call driver function
		// Select call driver feature
		_mapViewStatus = CallDriveFunction;
		_txtFiSelected = _txtFiPickupLocation;
		[self showPickupTextFieldBorderColor];
		[UIView animateWithDuration:0.5 animations:^{
			_txtFiDestination.alpha = 0.0f;
			_tbvShowingPlaces.alpha	= 0.0f;
			_viWindowMarker.alpha = 0.0f;
			_btnShowDirection.alpha = 0.0f;
			
			_tbvMain.alpha = 1.0f;
			_imviCenterMarker.alpha = 1.0f;
		}];
		[_mapView clear];
	}else{ // Show direction function
		
        [self textFieldShouldBeginEditing:_txtFiDestination];
        [_txtFiDestination becomeFirstResponder];
        
		if((![_txtFiDestination.text isEqualToString:@""]) && (![_txtFiPickupLocation.text isEqualToString:@""])){
			[UIView animateWithDuration:0.5f animations:^{
				_btnShowDirection.alpha = 1.0f;
			}];
		}
		// Select show route feature
		_mapViewStatus = RouteFunction;
		[UIView animateWithDuration:0.5 animations:^{
			_txtFiDestination.alpha = 1.0f;
			_tbvMain.alpha = 0.0f;
		}];
	}

}

#pragma mark - Get Driver Infor
- (void)getDriverInfor{
    
    
    __weak ViewController *weakSelf = self;
    MBProgressHUD * HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.label.text = @"Loading...";
    // First get offline
//    PFQuery *offlineQuery = [PFUser query];
//    [offlineQuery fromLocalDatastore];
//    [offlineQuery findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
//       
//    }];
    
    
    PFQuery *query = [PFQuery queryWithClassName:@"relation"];
    [query includeKey:[PFUser currentUser].objectId];
    [query selectKeys:@[@"driver"]];
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if (!error) {
            PFUser *driver = objects.firstObject[@"driver"];
            
            // Query Driver Information
            PFQuery *queryDriver = [PFUser query];
            [queryDriver whereKey:@"objectId" equalTo:driver.objectId];
            [queryDriver findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
                });

                if (!error) {
                    if (objects.count > 0) {
                        weakSelf.driver = [objects firstObject];
                        [weakSelf.driver pinInBackground];
                        [weakSelf getDriverTarget];
                    }else{
                        NSLog(@"Driver Not Found");
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Driver not found. Please contact with our service staff.Thank you" delegate:nil cancelButtonTitle:@"Oke" otherButtonTitles: nil];
                        [alertView show];
                    }
                }else{
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Oke" otherButtonTitles: nil];
                    [alertView show];

                }
                
            }];
        }else{
            NSLog(@"Error");
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
            });
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Oke" otherButtonTitles: nil];
            [alertView show];
        }
        
    }];
    
    

}

- (void)getDriverTarget{
    MBProgressHUD * HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.label.text = @"Loading...";
    __weak ViewController *weakSelf = self;
    NSString *getDriverTarget = [NSString stringWithFormat:@"http://cleanbasketap.azurewebsites.net/v1/cb/User/snsPushRegister?key=6c8ecba5a4621419df0f58598c4b90f7&token=%@&platform=2&user_id=nfbs2000",[self.driver valueForKey:@"token"] ];
    [[[NSURLSession sharedSession] dataTaskWithURL:[NSURL URLWithString:getDriverTarget]
                                completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//                                    dispatch_async(dispatch_queue_t queue, <#^(void)block#>)
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
                                    });
                                    if (!error) {
                                        
                                        NSError *error1;
                                        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error1];
                                        if (!error) {
                                            NSString *strResult = [result valueForKey:@"result"];
                                            if ([strResult isEqualToString:@"true"]) {
                                                weakSelf.driverTargetAddress = [result valueForKey:@"endpoint_arn"];
                                            }
                                        }
                                    }else{
                                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Oke" otherButtonTitles: nil];
                                        [alertView show];
                                    }
                                }] resume];
}


#pragma mark - Fetch Data
- (void)fetchNewDataWitCompletionHandler: (void (^)(UIBackgroundFetchResult))completionHandler andUserInfor:(NSDictionary *)userInfo{
    
}


#pragma mark - Check if Driver is received message
@end







