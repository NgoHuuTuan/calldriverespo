//
//  TextFieldsController.m
//  CallDriver
//
//  Created by Ngo Huu Tuan on 7/5/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import "TextFieldsController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Define.h"



typedef NS_ENUM(NSInteger, SuggestionTableViewStatus) {
	ShowingPreviousPlaces,
	ShowingSearchingPlaces
};

static SuggestionTableViewStatus suggestionTbvStatus;
static CGFloat cellHeight;

@interface TextFieldsController () <UITextFieldDelegate, UIGestureRecognizerDelegate, UITableViewDelegate, UITableViewDataSource>

@property (strong , nonatomic) GMSAutocompleteFilter *autocompleteFilterGMS;

@property (strong , nonatomic) NSMutableArray *arrFavouritePlace;
@property (strong , nonatomic) NSMutableArray *arrSearchingPlaces;
@property (strong , nonatomic) NSMutableArray *arrCacheSearching_1;
@property (strong , nonatomic) NSMutableArray *arrCacheSearching_2;

@end


@implementation TextFieldsController

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	
	_txtStartLocation.text = nil;
	_txtStartLocation.layer.borderWidth = 2.0f;
	_txtStartLocation.layer.cornerRadius = 5.0f;
	_txtStartLocation.layer.masksToBounds = YES;
	
	_txtEndLocation.text = nil;
	_txtEndLocation.layer.borderWidth = 2.0f;
	_txtEndLocation.layer.cornerRadius = 5.0f;
	_txtEndLocation.layer.masksToBounds = YES;
	
	_txtEndLocation.alpha = 0.0f;

	
	_txtFiSelected = _txtStartLocation;
	[self showTxtFiSelectedBorder];
	
	suggestionTbvStatus = ShowingPreviousPlaces;
	
	_arrFavouritePlace = [FavouritePlace loadListFavouritePlaces];
	_arrSearchingPlaces = [[NSMutableArray alloc] init];
	_arrCacheSearching_1 = [[NSMutableArray alloc] init];
	_arrCacheSearching_2 = [[NSMutableArray alloc] init];
	
	_autocompleteFilterGMS = [[GMSAutocompleteFilter alloc] init];
	_autocompleteFilterGMS.type = kGMSPlacesAutocompleteTypeFilterNoFilter; // Filter results
	_autocompleteFilterGMS.country = @"VN";
	
	
	if(IS_IPHONE_4 || IS_IPHONE_5){
		cellHeight = 30.0f;
	}else{
		cellHeight = 40.0f;
	}
	
	CGFloat newHeight = cellHeight*_arrFavouritePlace.count;
	CGFloat newWidth = ScreenWidth - 40;
	_tbvSuggestion = [[UITableView alloc] initWithFrame: CGRectMake(20, _txtEndLocation.frame.origin.y + 50, newWidth, newHeight) style:UITableViewStylePlain];
	_tbvSuggestion.alpha = 0.0f;
	_tbvSuggestion.dataSource = self;
	_tbvSuggestion.delegate = self;
	[self.view addSubview:_tbvSuggestion];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)mainVCChangeSegment:(NSInteger)segmentIndex{

	if(segmentIndex == 0){
		// Call driver function
		_txtFiSelected = _txtStartLocation;
		[UIView animateWithDuration:0.25f animations:^{
			_txtEndLocation.alpha = 0.0f;
		}];
		
	}else{
		// Show route function
		[UIView animateWithDuration:0.25f animations:^{
			_txtEndLocation.alpha = 1.0f;
		}];
	}
	[self showTxtFiSelectedBorder];
}

- (void)removeCacheAndFoundedPlace{
	[self.view endEditing:YES];
	_txtFiSelected.text = nil;
	
	if(_txtFiSelected == _txtStartLocation){
		_arrCacheSearching_1 = nil;
		_startPlaceFounded = nil;
	}else{
		_arrCacheSearching_2 = nil;
		_endPlaceFounded = nil;
	}
	[self hideSuggestionTable];
}


- (void)hideSuggestionTable{
	[self.view endEditing:YES];
	
	[UIView animateWithDuration:0.25f animations:^{
		_tbvSuggestion.alpha = 0.0f;
	}];
}

- (BOOL)showingRoute{
	if(_startPlaceFounded == nil || _endPlaceFounded == nil){
		return NO;
	}
	return YES;
}

- (BOOL)selectedStartTxtFi{
	if(_txtFiSelected == _txtStartLocation){
		return YES;
	}
	return NO;
}

- (void)showTxtFiSelectedBorder{
	if(_txtFiSelected == _txtEndLocation){
		_txtStartLocation.layer.borderColor = [UIColor clearColor].CGColor;
	}else if(_txtFiSelected == _txtStartLocation){
		_txtEndLocation.layer.borderColor = [UIColor clearColor].CGColor;
	}
	_txtFiSelected.layer.borderColor = [UIColor blueColor].CGColor;
}

// Get location ID from text input
- (void)getLocationIDFormText:(NSString *)textInput{
	ShowNetworkActivityIndicator();
	// Testing
	//	dispatch_async(dispatch_get_main_queue(), ^{
	//		[_arrPlaces removeAllObjects];
	//		for(int i = 0; i < (arc4random_uniform(4) + 1); i++){
	//			FavouritePlace *newPlace = [[FavouritePlace alloc] init];
	//			newPlace.strPlaceName = textInput;
	//			newPlace.location = [[CLLocation alloc] initWithLatitude:0.0f longitude:0.0f];
	//			[_arrPlaces addObject:newPlace];
	//		}
	//		// Repostion tbv base on its cell number
	//		CGFloat newHeight = _cellHeight*_arrPlaces.count;
	//		CGFloat newWidth = ScreenWidth - 40;
	//		_tbvShowingPlaces.frame = CGRectMake(20, _txtFiDestination.frame.origin.y + 50, newWidth, newHeight);
	//		[_tbvShowingPlaces reloadData];
	//	});
	
	[[GMSPlacesClient sharedClient] autocompleteQuery:textInput
											   bounds:nil
											   filter:_autocompleteFilterGMS
											 callback:^(NSArray<GMSAutocompletePrediction *> * _Nullable results, NSError * _Nullable error) {
												 HideNetworkActivityIndicator();
												 NSLog(@"%s error: %@", __PRETTY_FUNCTION__, error.localizedDescription);
												
												 if(results.count > 0 && error == nil){
													 [self getPlacesFromAutocompletePrediction:results];
												 }else{
													 dispatch_async(dispatch_get_main_queue(), ^{
														 [self reloadTableWithArray:nil andMode:ShowingSearchingPlaces];
													 });
												 }
											 }];
}

// Get address from location ID
- (void)getPlacesFromAutocompletePrediction:(NSArray *)predictionArr{
	
	__block NSMutableArray *arrNewPlaces = [[NSMutableArray alloc] init];
	//  Get address by using place ID
	
	for (GMSAutocompletePrediction* prediction in predictionArr) {
		[[GMSPlacesClient sharedClient] lookUpPlaceID:prediction.placeID callback:^(GMSPlace *place, NSError *error) {
			NSLog(@"%s error: %@", __PRETTY_FUNCTION__, error.localizedDescription);
			HideNetworkActivityIndicator();

			if (error != nil) {
				// Don't show alert here
			}else if(place != nil){
				FavouritePlace *newPlace = [[FavouritePlace alloc] init];
				newPlace.strPlaceName = place.formattedAddress;
				newPlace.location = [[CLLocation alloc] initWithLatitude:place.coordinate.latitude longitude:place.coordinate.longitude];
				
				if(newPlace.strPlaceName != nil){
					[arrNewPlaces addObject:newPlace];
				}
				// Reload suggestion table with the last Predication
				if([prediction isEqual:[predictionArr lastObject]]){
					dispatch_async(dispatch_get_main_queue(), ^{
						if(_txtFiSelected ==  _txtStartLocation){
							_arrCacheSearching_1 = arrNewPlaces;
						}else{
							_arrCacheSearching_2 = arrNewPlaces;
						}
						[self reloadTableWithArray:arrNewPlaces andMode:ShowingSearchingPlaces];
					});
				}
			}
		}]; // GMSPlacesClient method
	} // for(;;) statement
}

- (void)reloadTableWithArray:(NSMutableArray *)arrPlace andMode:(SuggestionTableViewStatus)tableStatus{
	suggestionTbvStatus = tableStatus;
	if(suggestionTbvStatus == ShowingPreviousPlaces){
		_arrFavouritePlace = arrPlace;
	}else if(suggestionTbvStatus == ShowingSearchingPlaces){
		_arrSearchingPlaces = arrPlace;
	}
	
	if(arrPlace.count > 0){
		CGFloat newHeight = cellHeight*arrPlace.count;
		CGFloat newWidth = ScreenWidth - 40;
		_tbvSuggestion.frame = CGRectMake(20, _txtEndLocation.frame.origin.y + 50, newWidth, newHeight);
		_tbvSuggestion.alpha = 1.0f;
		[self.view addSubview:_tbvSuggestion];
		[_tbvSuggestion reloadData];

	}else{
		_tbvSuggestion.alpha = 0.0f;
	}
}

- (void)setNewPlaceForSelectedTxtFi:(FavouritePlace *)newPlace{
	_txtFiSelected.text = newPlace.strPlaceName;
	if(_txtFiSelected == _txtStartLocation){
		_startPlaceFounded = newPlace;
	}else{
		_endPlaceFounded = newPlace;
	}
}

#pragma mark - UITableViewDelegate & UITableViewDataSourve

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	if(suggestionTbvStatus == ShowingPreviousPlaces){
		return _arrFavouritePlace.count;
	}else if(suggestionTbvStatus == ShowingSearchingPlaces){
		return _arrSearchingPlaces.count;
	}
	return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return cellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
	
	return 0.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
	
	return 0.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

	UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"CellIdent"];
	
	FavouritePlace *place;
	
	if(suggestionTbvStatus == ShowingPreviousPlaces){
		place = _arrFavouritePlace[indexPath.row];
		
		cell.textLabel.text = place.strPlaceTitle;
		cell.detailTextLabel.text = place.strPlaceName;
		
		NSString *iconName;
		if(place.placeType == FavouritePlaceHome){
			iconName = @"homeicon";
		}else if(place.placeType == FavouritePlaceWork){
			iconName = @"workicon";
		}else if(place.placeType == FavouritePlaceOthers){
			iconName = @"othersicon";
		}else{
			iconName = @"newesticon";
		}
		
		cell.imageView.image = [UIImage imageNamed:iconName];
		
	}else if(suggestionTbvStatus == ShowingSearchingPlaces){
		place = _arrSearchingPlaces[indexPath.row];
		NSRange rangeOfFirstComma = [place.strPlaceName rangeOfString:@","];
		
		// If comma was found, substring by comma
		if(rangeOfFirstComma.length > 0){
			cell.textLabel.text = [place.strPlaceName substringToIndex:rangeOfFirstComma.location];
			cell.detailTextLabel.text = [place.strPlaceName substringFromIndex:rangeOfFirstComma.location + 2];
		}else{
			cell.textLabel.text = place.strPlaceName;
		}
	}
	
	return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	FavouritePlace *selectedPlace;
	if(suggestionTbvStatus == ShowingPreviousPlaces){
		 selectedPlace = ((FavouritePlace *)_arrFavouritePlace[indexPath.row]);
	}else if(suggestionTbvStatus == ShowingSearchingPlaces){
		selectedPlace = ((FavouritePlace *)_arrSearchingPlaces[indexPath.row]);
	}
	
	if(_txtFiSelected == _txtStartLocation){
		_startPlaceFounded = selectedPlace;
	}else{
		_endPlaceFounded = selectedPlace;
	}
	_txtFiSelected.text = selectedPlace.strPlaceName;
	
	[self.delegate didSelectSuggestionPlace:selectedPlace];
	[self hideSuggestionTable];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
	
	_txtFiSelected = textField;
	[self showTxtFiSelectedBorder];
	[self.delegate textFieldShouldBeginEditing];
	
	if([_txtFiSelected isEqual:_txtStartLocation]){
		
	}else if([_txtFiSelected isEqual:_txtEndLocation]){
		
	}
	
	if([[textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]){
		[self reloadTableWithArray:[FavouritePlace loadListFavouritePlaces] andMode:ShowingPreviousPlaces];
	}else if([_txtFiSelected isEqual:_txtStartLocation]){
		[self reloadTableWithArray:_arrCacheSearching_1 andMode:ShowingSearchingPlaces];
	}else if([_txtFiSelected isEqual:_txtEndLocation]){
		[self reloadTableWithArray:_arrCacheSearching_2 andMode:ShowingSearchingPlaces];
	}
	
	return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
	
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
	[self.view endEditing:YES];
	
	return YES;
}

//
// 	Get list of predicted place names or address
//
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(nonnull NSString *)string{
//	_mapViewStatus = ShowingSuggestionPlace;
	NSString *stringAfterReplace;
	if (string.length == 0) {
		// handle Delete (also handles the Cut menu)
		NSString *txtFiText = [textField.text stringByTrimmingCharactersInSet:[NSMutableCharacterSet whitespaceAndNewlineCharacterSet]];
		stringAfterReplace = [txtFiText substringToIndex:txtFiText.length - 1];
	} else {
		// some other key or text is being pasted.
		stringAfterReplace = [textField.text stringByAppendingString:string];
		stringAfterReplace = [stringAfterReplace stringByTrimmingCharactersInSet:[NSMutableCharacterSet whitespaceAndNewlineCharacterSet]];
	}
	if([stringAfterReplace isEqualToString:@""]){
		suggestionTbvStatus = ShowingPreviousPlaces;
		_arrFavouritePlace = [FavouritePlace loadListFavouritePlaces];
		[_tbvSuggestion reloadData];
		[UIViewController cancelPreviousPerformRequestsWithTarget:self selector:@selector(getLocationIDFormText:) object:textField.text];
		
		return YES;
	}else{
		suggestionTbvStatus = ShowingSearchingPlaces;
	}
	[UIViewController cancelPreviousPerformRequestsWithTarget:self selector:@selector(getLocationIDFormText:) object:textField.text];
	[self performSelector:@selector(getLocationIDFormText:) withObject:stringAfterReplace afterDelay:0.5];
	return YES;
}


#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
	
	if([touch.view isKindOfClass:[TextFieldsController class]]){
		return NO;
	}
	
	return YES;
}




@end
