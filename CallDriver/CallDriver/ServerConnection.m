//
//  ServerConnection.m
//  CT
//
//  Created by EuiHwa Park on 13. 11. 19..
//  Copyright (c) 2013년 EuiHwa Park. All rights reserved.
//

#import "ServerConnection.h"
#import "JSONParser.h"
#import "HTTPPost.h"
#import "Define.h"

@implementation ServerConnection

+ (NSString *)connectionResultStringToURL:(NSString *)url connectionName:(NSString *)connectionName {
    
    NSLog(@"%@ URL : %@", connectionName, url);
    
    NSString *escapedUrl = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:escapedUrl] encoding:NSUTF8StringEncoding error:nil];
    
    if (SERVER_CONNECTION_RESULT_ON) {
        NSLog(@"\n----- result -----\n%@\n--------------------", result);
    }
    
    return result;
    
}

+ (NSString *)connectionStringPostURL:(NSString *)url body:(NSString *)body connectionName:(NSString *)connectionName utf8:(BOOL)utf8 {
    
    NSLog(@"%@ URL : %@?%@", connectionName, url, body);
    
    NSString *returnString;
    if (utf8 == NO) {
        returnString = [HTTPPost HTTPPostWithURL:url withBody:body];
    } else {
        returnString = [HTTPPost utf8HTTPPostWithURL:url withBody:body];
    }
    
    if (SERVER_CONNECTION_RESULT_ON) {
        NSLog(@"\n----- %@ result -----\n%@\n--------------------", connectionName, returnString);
    }
    
    return returnString;
}

+ (NSDictionary *)connectionDictionaryGetURL:(NSString *)url connectionName:(NSString *)connectionName {
    
    NSLog(@"%@ URL : %@", connectionName, url);
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    
	JSONParser *json = [[JSONParser alloc] initWithJSON];
    
    [dic setDictionary:[json dictionaryWithURL:url]];
    
    if (SERVER_CONNECTION_RESULT_ON) {
        NSLog(@"\n----- %@ result -----\n%@\n--------------------", connectionName, dic);
    }
        
    return dic;
}

+ (NSArray *)connectionArrayGetURL:(NSString *)url connectionName:(NSString *)connectionName {
    
    NSLog(@"%@ URL : %@", connectionName, url);
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
	JSONParser *json = [[JSONParser alloc] initWithJSON];
    
    [array setArray:[json arrayWithURL:url]];
    
    if (SERVER_CONNECTION_RESULT_ON) {
        NSLog(@"\n----- %@ result -----\n%@\n--------------------", connectionName, array);
    }
        
    return array;
}

+ (NSDictionary *)connectionDictionaryPostURL:(NSString *)url body:(NSString *)body connectionName:(NSString *)connectionName utf8:(BOOL)utf8 {
    
    NSLog(@"%@ URL : %@?%@", connectionName, url, body);
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    
	JSONParser *json = [[JSONParser alloc] initWithJSON];
    
    NSString *returnString;
    if (utf8 == NO) {
        returnString = [HTTPPost HTTPPostWithURL:url withBody:body];
    } else {
        returnString = [HTTPPost utf8HTTPPostWithURL:url withBody:body];
    }
    
    if (SERVER_CONNECTION_RESULT_ON) {
        NSLog(@"\n----- %@ result -----\n%@\n--------------------", connectionName, returnString);
    }
    
    [dic setDictionary:[json dictionaryWithJsonString:returnString]];
    
    return dic;
}

+ (NSArray *)connectionArrayPostURL:(NSString *)url body:(NSString *)body connectionName:(NSString *)connectionName {
    
    NSLog(@"%@ URL : %@?%@", connectionName, url, body);
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
	JSONParser *json = [[JSONParser alloc] initWithJSON];
    
    NSString *returnString = [HTTPPost HTTPPostWithURL:url withBody:body];
    
    if (SERVER_CONNECTION_RESULT_ON) {
        NSLog(@"\n----- %@ result -----\n%@\n--------------------", connectionName, returnString);
    }
    
    [array setArray:[json arrayWithJsonString:returnString]];
    
    return array;
}

+ (NSDictionary *)connectionDictionaryPutURL:(NSString *)url body:(NSString *)body connectionName:(NSString *)connectionName {
    
    NSLog(@"%@ URL : %@?%@", connectionName, url, body);
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    
	JSONParser *json = [[JSONParser alloc] initWithJSON];
    
    NSString *returnString = [HTTPPost HTTPPutWithURL:url withBody:body];
    
    if (SERVER_CONNECTION_RESULT_ON) {
        NSLog(@"\n----- %@ result -----\n%@\n--------------------", connectionName, returnString);
    }
    
    [dic setDictionary:[json dictionaryWithJsonString:returnString]];
    
    return dic;
}

+ (NSArray *)connectionArrayPutURL:(NSString *)url body:(NSString *)body connectionName:(NSString *)connectionName {
    
    NSLog(@"%@ URL : %@?%@", connectionName, url, body);
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
	JSONParser *json = [[JSONParser alloc] initWithJSON];
    
    NSString *returnString = [HTTPPost HTTPPutWithURL:url withBody:body];
    
    if (SERVER_CONNECTION_RESULT_ON) {
        NSLog(@"\n----- %@ result -----\n%@\n--------------------", connectionName, returnString);
    }
    
    [array setArray:[json arrayWithJsonString:returnString]];
    
    return array;
}

@end
