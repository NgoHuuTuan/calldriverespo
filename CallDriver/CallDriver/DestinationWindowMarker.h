//
//  DestinationWindowMaker.h
//  CallDriver
//
//  Created by Ngo Huu Tuan on 3/30/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DestinationWindowMarkerDelegate
- (void)markNewPlace;
@end

@interface DestinationWindowMarker : UIView
@property (weak, nonatomic) id <DestinationWindowMarkerDelegate> delegate;
@end
