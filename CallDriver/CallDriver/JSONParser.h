//
//  JSONParser.h
//  DefaultJSON
//
//  Created by 박의화 on 11. 5. 30..
//  Copyright 2011 sunchon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBJson.h"

@interface JSONParser : NSObject {
	SBJsonParser *jsonParser;
	SBJsonWriter *jsonWriter;
}

- (id)initWithJSON;
- (NSMutableDictionary *)dictionaryWithURL:(NSString *)urlString;
- (NSMutableArray *)arrayWithURL:(NSString *)urlString;
- (NSMutableDictionary *)dictionaryWithJsonString:(NSString *)jsonString;
- (NSMutableArray *)arrayWithJsonString:(NSString *)jsonString;

@end
