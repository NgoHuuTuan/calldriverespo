//
//  TransparentView.m
//  CallDriver
//
//  Created by Ngo Huu Tuan on 7/5/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import "TransparentView.h"

@implementation TransparentView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.


- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event{
	for(UIView *subView in self.subviews){
		if([subView hitTest:[self convertPoint:point toView:subView] withEvent:event] != nil){
			return YES;
		}
	}
	return NO;
}

@end
