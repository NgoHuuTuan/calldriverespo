//
//  DatePickerView.h
//  CleanBasket
//
//  Created by Ngo Huu Tuan on 3/1/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DatePickerViewDelegate <NSObject>

@required
- (void)selectedDate:(NSDate *)date;
@end


@interface DatePickerView : UIView
@property (weak, nonatomic) id <DatePickerViewDelegate> delegate;

- (void)show; // Show date picker
- (void)hide;
- (void)showDateTimePicker; // Show date time picker
- (void)showTimer; // Show timer

@end
