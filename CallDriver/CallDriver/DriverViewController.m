//
//  DriverViewController.m
//  CleanBasket
//
//  Created by Manh Tien on 3/26/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import "DriverViewController.h"
#import "AlertLabel.h"
#import "FavouritePlace.h"
#import "LoginVC.h"
#import "MBProgressHUD.h"

#define kDriverVCCellHeight ((IS_IPHONE_4) ? 40 : 50)

@interface DriverViewController ()<UITableViewDelegate,UITableViewDataSource,GMSMapViewDelegate,UIAlertViewDelegate>
@property (strong,nonatomic) GMSMapView *mapView;
@property (strong,nonatomic) UITableView *tbvInformation;
@property (strong,nonnull) GMSPolyline *polyline;

@property (strong,nonatomic) PFObject *request;
@end

@implementation DriverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createInterface];
    [self setUpMapView];
    
    [self loadRequestFromServer];

}

- (void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	
	
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createInterface{
    self.title = @"CEO Mr.X";
    [self addRightBarDirectionButton];
	[self addLogoutButton];
    _mapView = [[GMSMapView alloc] init];
    _mapView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_mapView];
    
    _tbvInformation = [[UITableView alloc] init];
    _tbvInformation.translatesAutoresizingMaskIntoConstraints = NO;
    _tbvInformation.alwaysBounceVertical = NO;
    _tbvInformation.delegate = self;
    _tbvInformation.dataSource = self;
	_tbvInformation.separatorStyle = UITableViewCellSeparatorStyleNone;
	
    [self.view addSubview:_tbvInformation];
    
    // Autolayout
    NSDictionary *views = @{
                            @"mapview":_mapView,
                            @"tbv":_tbvInformation,
							@"bottomLayoutGuide":self.bottomLayoutGuide,
                            @"topLayoutGuide":self.topLayoutGuide
                            };

	CGFloat tbvHeight;
    tbvHeight = kDriverVCCellHeight*3;
	
//    int mapview_h = (IS_IPHONE_4) ? 300 : 500;
    NSDictionary *metrics = @{
                              @"tbvHeight":@(tbvHeight)
                              };
    
    NSArray *vertical_Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[topLayoutGuide]-0-[mapview]-[tbv(tbvHeight)]-10-[bottomLayoutGuide]" options:0 metrics:metrics views:views];
    NSArray *mapview_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[mapview]-0-|" options:0 metrics:metrics views:views];
    NSArray *tbvInformation_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[tbv]-0-|" options:0 metrics:metrics views:views];
    
    [self.view addConstraints:vertical_Constraint];
    [self.view addConstraints:mapview_H];
    [self.view addConstraints:tbvInformation_H];
	
	
	self.view.backgroundColor = [UIColor whiteColor];
}

- (void)setUpMapView{
    _mapView.settings.myLocationButton = YES;
    _mapView.myLocationEnabled = YES;
    _mapView.delegate = self;
    
    [_mapView addObserver:self forKeyPath:@"myLocation" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)dealloc{
    @try{
        [_mapView removeObserver:self forKeyPath:@"myLocation"];
    }@catch(id anException){
        //do nothing
    }
}

- (void)addRightBarDirectionButton{
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Direct" style:UIBarButtonItemStyleDone target:self action:@selector(directionBarButtonClicked:)];
    self.navigationItem.rightBarButtonItem = barButtonItem;
}

- (void)addLogoutButton{
	UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStyleDone target:self action:@selector(clickLogout:)];
	self.navigationItem.leftBarButtonItem = barButtonItem;
}

#pragma mark - Observer my location
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"myLocation"]) {
        if (self.call == nil) {
            CLLocation *newLocation = [change objectForKey:@"new"];
            
            GMSCameraUpdate *cameraUpdate = [GMSCameraUpdate setTarget:newLocation.coordinate zoom:12];
            [_mapView moveCamera:cameraUpdate];
            [_mapView removeObserver:self forKeyPath:@"myLocation"];
        }
    }
}

#pragma mark - TableView delegate and datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InforCell"];
	
	if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"InforCell"];
    }
	
    switch (indexPath.row) {
        case 0:
        {
            cell.textLabel.text = @"Time CEO call :";
            NSString *txt = (self.call == nil) ? @"" : self.call.strDateCalling;
            cell.detailTextLabel.text = txt;
            cell.detailTextLabel.textColor = [UIColor darkGrayColor];
        }
            break;
            
        case 1:{
            cell.textLabel.text = @"Time pick up CEO :";
            NSString *txt = (self.call == nil) ? @"" : self.call.strDatePickup;
            cell.detailTextLabel.text = txt;
            cell.detailTextLabel.textColor = [UIColor darkGrayColor];
            break;
        }
            
        case 2:{
//            cell.textLabel.text = @"CEO phone :";
//            NSString *txt = (self.call == nil) ? @"" : self.call.strUserPhone;
//            cell.detailTextLabel.text = txt;
//            cell.detailTextLabel.textColor = [UIColor redColor];
//            break;
            
            CGFloat tbnWidth = self.view.frame.size.width - 2*kDriverVCCellHeight;
            CGFloat tbnHeight = kDriverVCCellHeight - 2*5.0f;
            
            UIButton *tbnSend = [[UIButton alloc] initWithFrame:CGRectMake(50.0f, 5.0f, tbnWidth, tbnHeight)];
            [tbnSend setTitle:@"CALL TO CEO" forState:UIControlStateNormal];
            [tbnSend setBackgroundImage:[Constants imageWithColor:kColorPink] forState:UIControlStateNormal];
            [tbnSend addTarget:self action:@selector(btnCallClicked:) forControlEvents:UIControlEventTouchUpInside];
            tbnSend.layer.cornerRadius = 10.0f;
            tbnSend.layer.masksToBounds = YES;
            
            [cell.contentView addSubview:tbnSend];
        }
        
        default:
            break;
    }
	
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.textLabel.textColor = [UIColor blackColor];
    
    
	
    return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kDriverVCCellHeight;
}

#pragma mark - Show Calling

- (void)showCallingWithCallingInformation{
	dispatch_async(dispatch_get_main_queue(), ^{
        [_mapView clear];
        
        GMSCameraUpdate *customerLocation = [GMSCameraUpdate setTarget:_call.pickupLocation.coordinate zoom:12.0f];
        [_mapView moveCamera:customerLocation];
        
        GMSMarker *marker = [GMSMarker markerWithPosition:_call.pickupLocation.coordinate];
        marker.icon = [UIImage imageNamed:@"Pin_TimCookSmall"];
        marker.map = _mapView;
        marker.appearAnimation = kGMSMarkerAnimationPop;
        
        [_tbvInformation reloadData];
    });
}

#pragma mark - Calling CEO phone
- (void)callCeoPhone{
    if (self.call != nil) {
        NSString *ceoPhone = self.call.strUserPhone;
        NSURL *phoneURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",ceoPhone]];
        if ([[UIApplication sharedApplication] canOpenURL:phoneURL]) {
            [[UIApplication sharedApplication] openURL:phoneURL];
        }else
        {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
}

#pragma mark - Get direction 
- (void)getDirectionFromLocation:(CLLocationCoordinate2D)startLocation to:(CLLocationCoordinate2D)stopLocation{
    NSString *strRequest = [NSString stringWithFormat:@"%@directions/json?key=%@&language=vi&origin=%f,%f&destination=%f,%f",baseURL,kGGMapServerKey,startLocation.latitude,startLocation.longitude,stopLocation.latitude,stopLocation.longitude];
    NSURLSession *session = [NSURLSession sharedSession];
    
    __weak DriverViewController *weakSelf = self;
    [[session dataTaskWithURL:[NSURL URLWithString:strRequest] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (!error) {
            NSError *parseError;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
            if (!parseError) {
                NSDictionary *routes = [[result objectForKey:@"routes"] firstObject];
                NSDictionary *route = [routes objectForKey:@"overview_polyline"];
                if (route == nil) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakSelf showAlertCantGetDirection];
                    });
                    return;
                }
                NSString *overview_route = [route objectForKey:@"points"];
                GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
                GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
                
                [weakSelf drawPolylineOnMap:polyline];
            }
        }else{
            [weakSelf showAlertErrorConnection];
        }
    }] resume];
}

#pragma mark - Draw polyline on Map
- (void)drawPolylineOnMap: (GMSPolyline *)newPolyline{
    if (newPolyline != nil) {
        // Delete old polyline
        self.polyline.map = nil;
        // Update region
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:[newPolyline.path coordinateAtIndex:0]  coordinate:[newPolyline.path coordinateAtIndex:newPolyline.path.count-1]];
        GMSCameraUpdate *boundCamera = [GMSCameraUpdate fitBounds:bounds withPadding:50.0];
        [self.mapView animateWithCameraUpdate:boundCamera];
        // Draw new direction
        self.polyline = newPolyline;
        self.polyline.map = self.mapView;
    }else{
        [self showAlertErrorConnection];
    }
}

#pragma mark - Show alert

- (void)showAlert: (NSString *)text{
    AlertLabel *alert = [AlertLabel initAlertWithContent:text];
    [alert setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.view addSubview:alert];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:alert
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.mapView
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0
                                                           constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.mapView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:alert
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0
                                                           constant:40.0]];
    [alert addConstraint:[NSLayoutConstraint constraintWithItem:alert attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:21]];
    [alert dismissAlertLabel];
}

- (void)showAlertErrorConnection{
    [self showAlert:@"Network connection error."];
}

- (void)showAlertCantGetLocation{
    [self showAlert:@"Can not locate your location. Please open Privacy in Setting"];
}

- (void)showAlertCantGetDirection{
    [self showAlert:@"Can not get direction."];
}

#pragma mark - Direcion and logout button clicked

- (IBAction)directionBarButtonClicked:(id)sender{
    if (self.call != nil) {
        if (self.mapView.myLocation != nil) {
            [self getDirectionFromLocation:self.mapView.myLocation.coordinate to:self.call.pickupLocation.coordinate];
        }
    }
}

- (IBAction)clickLogout:(id)sender{
	[PFUser logOutInBackgroundWithBlock:^(NSError * _Nullable error) {
        if (!error)  {
            LoginVC *loginVC = [[LoginVC alloc] init];
            WINDOW.rootViewController = loginVC;
        }else{
        NSLog(@"SignOut error: %@",error.description);
        }

    }];
}

#pragma mark - Load data from server

- (void)loadRequestFromServer{
//    NSString *strRequest = [NSString stringWithFormat:@"%@?get=""",SERVER_URLSTRING];
//    NSURLSession *session = [NSURLSession sharedSession];
//    
//    __weak DriverViewController *weakSelf = self;
//    [[session dataTaskWithURL:[NSURL URLWithString:strRequest] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//        if (!error) {
//            NSError *parseError;
//            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
//            if (!parseError) {
//                NSDictionary *newestCalling = [[result objectForKey:@"result"] firstObject];
//                CallingInformation *callingInformation = [CallingInformation initWithDictionaryData:newestCalling];
//                weakSelf.call = callingInformation;
//                if (weakSelf.call.callingStatus == 0) {
//                    [weakSelf confirmRequest];
//                }
//            }
//        }
//    }] resume ];
    // Get CEO
    
    
    MBProgressHUD * HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.label.text = @"Loading...";
    
    __weak DriverViewController *weakSelf = self;
    PFQuery *query = [PFQuery queryWithClassName:@"relation"];

    NSLog(@"ID :%@",[PFUser currentUser].objectId);
    

    [query includeKey:[PFUser currentUser].objectId];
    [query selectKeys:@[@"ceo"]];
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        PFUser *ceo = objects.firstObject[@"ceo"];
        
        // Query Ceo Information
        PFQuery *queryCeo = [PFUser query];
        [queryCeo whereKey:@"objectId" equalTo:ceo.objectId];
        [queryCeo findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
            if (!error) {
                if (objects.count > 0) {
                    PFUser *ceoInfor = [objects firstObject];
                    PFQuery *queryInformation = [PFQuery queryWithClassName:@"callingdriver"];
                    [queryInformation whereKey:@"ceo" equalTo:ceoInfor];
                    [queryInformation whereKey:@"status" equalTo:@"0"];
                    [queryInformation orderByAscending:@"createdAt"];
                    queryInformation.limit = 1;
                    
                    [queryInformation findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
                        });
                        if (!error) {
                            
                            if (objects.count>0) {
                            
                                PFObject *infor = objects.firstObject;
                                
                                //
                                
                                weakSelf.call = [[CallingInformation alloc] init];
                                weakSelf.call.strDateCalling = [infor valueForKey:@"timecalling"];
                                weakSelf.call.strDatePickup = [infor valueForKey:@"timerequest"];
                                weakSelf.call.callingStatus = [[infor valueForKey:@"status"] intValue];
                                
                                NSString *longtitude = [infor valueForKey:@"longtitude"];
                                NSString *latitude = [infor valueForKey:@"latitude"];
                                weakSelf.call.pickupLocation = [[CLLocation alloc] initWithLatitude:[latitude doubleValue] longitude:[longtitude doubleValue]];
                                
                                [weakSelf showCallingWithCallingInformation];
                                
                                //
                                [infor setValue:@"1" forKey:@"status"];
                                [infor saveInBackground];
                                
                            }else{
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No new request." delegate:nil cancelButtonTitle:@"Oke" otherButtonTitles: nil];
                                    [alertView show];
                                });

                               
                            }
                        }else{
                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:error.description delegate:nil cancelButtonTitle:@"Oke" otherButtonTitles: nil];
                            [alertView show];
                        }
                    }];
                    
                }else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
                    });
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Can not find CEO information. Please contact with company." delegate:nil cancelButtonTitle:@"Oke" otherButtonTitles: nil];
                    [alertView show];
                }
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
                });
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Oke" otherButtonTitles: nil];
                [alertView show];
            }
        }];
        
        
        
    }];
    


    
}

#pragma mark - Get,Set method
- (void)setCall:(CallingInformation *)call{
    _call  = call;
    [self showCallingWithCallingInformation];
}

#pragma mark - UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 1000) {
        [self callCeoPhone];
    }else if(alertView.tag == 1001){
        [self loadRequestFromServer];
    }
}



#pragma mark - ButtonCall clicked 
- (IBAction)btnCallClicked:(id)sender{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Calling to CEO" message:@"Do you want to call to the CEO ?" delegate:self cancelButtonTitle:@"Call" otherButtonTitles:@"Cancel", nil];
    alertView.tag = 1000;
    [alertView show];
}


#pragma mark - Fetch Data with Completion Handler
- (void)fetchNewDataWitCompletionHandler: (void (^)(UIBackgroundFetchResult))completionHandler andUserInfor:(NSDictionary *)userInfo{
    NSDictionary *apsDic = [userInfo valueForKey:@"aps"];
    if (apsDic != nil) {
        NSString *strAlert = [apsDic valueForKey:@"alert"];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"New Message" message:strAlert delegate:self cancelButtonTitle:@"Oke" otherButtonTitles: nil];
        alertView.tag = 1001;
        [alertView show];
    }
}




@end
