//
//  FavouritePlace.m
//  CallDriver
//
//  Created by Manh Tien on 3/29/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import "FavouritePlace.h"

@interface FavouritePlace()<NSCoding>

@end

@implementation FavouritePlace





#pragma mark - NSCodeing protocol
-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.strPlaceName forKey:kFavouritePlaceEncodeName];
    [aCoder encodeDouble:self.location.coordinate.longitude forKey:kFavouritePlaceEncodeLocationLong];
    [aCoder encodeDouble:self.location.coordinate.latitude forKey:kFavouritePlaceEncodeLongcationLat];
    [aCoder encodeInt:self.placeType forKey:kFavouritePlaceEncodePlaceType];
    [aCoder encodeObject:self.strPlaceTitle forKey:kFavouritePlaceEncodeTitle];
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (self) {
        self.strPlaceName = [aDecoder decodeObjectForKey:kFavouritePlaceEncodeName];
        self.strPlaceTitle = [aDecoder decodeObjectForKey:kFavouritePlaceEncodeTitle];
        float longtitude = [aDecoder decodeDoubleForKey:kFavouritePlaceEncodeLocationLong];
        float latitude = [aDecoder decodeDoubleForKey:kFavouritePlaceEncodeLongcationLat];
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:latitude longitude:longtitude];
        self.location = location;
        
        self.placeType = [aDecoder decodeIntForKey:kFavouritePlaceEncodePlaceType];
        
    }
    return self;
}

- (void)setPlaceType:(kFavouritePlaceType)placeType{
    _placeType = placeType;
    _wrapPlaceType = [NSString stringWithFormat:@"%d",_placeType];
}

#pragma mark - Save,load data 

static NSMutableArray *ListFavouritePlaces;

//static NSMutableArray *ListFavouriteHomePlaces;
//static NSMutableArray *ListFavouriteWorkPlaces;
//static NSMutableArray *ListFavouriteOthersPlaces;
//static NSMutableArray *ListFavouriteNewestPlaces;

+ (void)saveListFavouritePlaces:(NSArray *)listPlaces{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	NSMutableArray *data = [[NSMutableArray alloc] init];
	for (FavouritePlace *place in listPlaces) {
		NSData *placeData = [NSKeyedArchiver archivedDataWithRootObject:place];
		[data addObject:placeData];
	}
	
	
	[userDefaults setObject:[NSKeyedArchiver archivedDataWithRootObject:data] forKey:kFavouritePlaceEncodeListPlaces];
	[userDefaults synchronize];


    ListFavouritePlaces = [NSMutableArray arrayWithArray:listPlaces];
    ListFavouritePlaces = [self arrageListFavouritePlace:ListFavouritePlaces];
}

+ (NSMutableArray *)loadListFavouritePlaces{
    if (ListFavouritePlaces == nil) {
        ListFavouritePlaces = [[NSMutableArray alloc] init];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSData *data = [userDefaults objectForKey:kFavouritePlaceEncodeListPlaces];
        if (data) {
			
            NSMutableArray *placesData = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            for (NSData *placeData in placesData) {
                FavouritePlace *place = [NSKeyedUnarchiver unarchiveObjectWithData:placeData];
                [ListFavouritePlaces addObject:place];
            }
			
        }
        [userDefaults synchronize];
        
        ListFavouritePlaces = [self arrageListFavouritePlace:ListFavouritePlaces];
    }
	
    return ListFavouritePlaces;
}


+ (NSMutableArray *)arrageListFavouritePlace:(NSMutableArray *)listPlaces{

    NSArray *sortedArray = [listPlaces sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        NSString *first = [(FavouritePlace *)obj1 wrapPlaceType];
        NSString *second = [(FavouritePlace *)obj2 wrapPlaceType];
        return [first compare:second];
    }];
    
    return [NSMutableArray arrayWithArray:sortedArray];
}

+ (void)addNewFavouritePlaces:(FavouritePlace *)place{
    NSMutableArray *listFavouritePlaces = [FavouritePlace loadListFavouritePlaces];
	NSLog(@"%@ %@ %@ %d", place.strPlaceName, place.strPlaceTitle, place.wrapPlaceType, place.placeType);
	
    NSArray *listPlaceOfSameType = [self filterListFavouritePlacesByType:place.placeType];
    
    int maximumPlace = (place.placeType == FavouritePlaceHome) ? kFavouritePlaceMaxHome : ( (place.placeType == FavouritePlaceWork) ? kFavouritePlaceMaxWork : kFavouritePlaceMaxOthers );
    
    if (listPlaceOfSameType.count > maximumPlace) {
        FavouritePlace *oldPlace = [listPlaceOfSameType lastObject];
        [listFavouritePlaces removeObject:oldPlace];
    }
    [listFavouritePlaces insertObject:place atIndex:0];
    
    //
    [self saveListFavouritePlaces:listFavouritePlaces];
    
    
}

+ (NSArray *)filterListFavouritePlacesByType: (kFavouritePlaceType)placeType{
	
    [FavouritePlace loadListFavouritePlaces];
    NSString *strPredicate = [NSString stringWithFormat:@"wrapPlaceType == '%@'",[NSString stringWithFormat:@"%d",placeType]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:strPredicate];
    NSArray *arr = [ListFavouritePlaces filteredArrayUsingPredicate:predicate];
    return [ListFavouritePlaces filteredArrayUsingPredicate:predicate];
}

+ (void)removeFavouritePlaces:(FavouritePlace *)place{
    NSArray *listFavouritePlaces = [FavouritePlace loadListFavouritePlaces];
    NSMutableArray *listNewPlaces = [NSMutableArray arrayWithArray:listFavouritePlaces];
    [listNewPlaces removeObject:place];
    // Save new favouriteplace
    [FavouritePlace saveListFavouritePlaces:listNewPlaces];
}

+ (NSArray *)loadListFavouritePlacesOfType: (kFavouritePlaceType)placeType{
    return [self filterListFavouritePlacesByType:placeType];
}

- (instancetype)initWithPlaceName:(NSString *)placeName{
	self = [super init];
	
	if(self){
		self.strPlaceName = placeName;
	}
	
	return self;
}

+ (instancetype)newPlaceWithName:(NSString *)placeName andCoordinate:(CLLocationCoordinate2D)coor{
	FavouritePlace *newObject = [[FavouritePlace alloc] init];
	
	if(self){
		newObject.strPlaceName = placeName;
		newObject.location = [[CLLocation alloc] initWithLatitude:coor.latitude longitude:coor.longitude];
		newObject.placeType = kFavouritePlaceMaxOthers;
	}
	
	return newObject;
}

@end
