//
//  NSDictionary+RemoveNull.m
//  CT
//
//  Created by 박의화 on 2015. 3. 22..
//  Copyright (c) 2015년 의화. All rights reserved.
//

#import "NSDictionary+RemoveNull.h"

@implementation NSDictionary (RemoveNull)

- (NSDictionary *)removeNull {
    NSMutableDictionary *removedDic = [NSMutableDictionary dictionary];
    for (NSString * key in [self allKeys])
    {
        if (![[self objectForKey:key] isKindOfClass:[NSNull class]])
            [removedDic setObject:[self objectForKey:key] forKey:key];
    }
    return removedDic;
}

@end
