//
//  DriverViewController.h
//  CleanBasket
//
//  Created by Manh Tien on 3/26/16.
//  Copyright © 2016 heyKorean. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CallingInformation.h"
#import "Define.h"
#import "Constants.h"
#import "FavouritePlace.h"

@import GoogleMaps;

@interface DriverViewController : UIViewController
@property (strong,nonatomic) CallingInformation *call;


- (void)showCallingWithCallingInformation;
- (void)fetchNewDataWitCompletionHandler: (void (^)(UIBackgroundFetchResult))completionHandler andUserInfor:(NSDictionary *)userInfo;

@end
